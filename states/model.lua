ModelState = {}
ModelStateMT = { __index = ModelState }

setmetatable(ModelState, { __index = EditorState })

function ModelState:Create(owner)
  local self = EditorState:Create(owner, ModelStateMT)

  --owner:set_style(true, true, false)
  return self
end

function ModelState:Destroy()
  EditorState.Destroy(self)
end

function ModelState:Select()
  self:ShowLibrary()
  local library = self.library
  local mode = self.mode
  if mode == nil then
    -- select first available item
    for i, v in ipairs(library.list.items) do
      mode = v
      break
    end
  end
  self:SetMode(mode)
end

function ModelState:Deselect(wx, wy)
  self:HideParams()
  self:HideLibrary()
end

function ModelState:Redraw()
  self:RedrawJoints()
end

function ModelState:KeyPress(key, ctr, shift)
  if key == KEY_DOT then
  elseif key == KEY_COMMA then
  end
end

function ModelState:SetMode(mode)
  self.mode = mode
  self.owner:set_tool(AddModelTool, mode)
  local library = self.library
  if library then
    for i, v in ipairs(library.list.items) do
      if library.list.items[i] == mode then
        library.list:set_index(i)
        break
      end
    end
  end
  self:HideParams()
  self:ShowParams()
end

function ModelState:ShowLibrary()
  local pad = 5
  local w = 200

  local y = pad
  local list = gui.List(pad, y, w - pad*2, 8)
  
  -- order models alphabetically by name
  local amodels = {}
  for i, v in pairs(models) do
    table.insert(amodels, i)
  end
  table.sort(amodels)
  
  for i, v in pairs(amodels) do
    list:add_items(v)
  end
  list.on_change = function(list, index)
    self:SetMode(list.items[index])
    --self.owner:set_focus(nil)
  end
  list.node.depth = -1
  y = y + list.height + pad

  local owner = self.owner
  local ow, oh = owner:get_size()
  local h = y + pad
  local win = gui.Window(ow - w, 0, w, h, "Models library")
  local tw, th = win:get_title_size()
  win:set_position(ow - w, th)
  win.node.depth = -1
  owner:add_child(win)
  
  win.list = list
  win:add_child(list)
  
  self.library = win
end

function ModelState:HideLibrary()
  if self.library then
    self.owner:remove_child(self.library)
    self.library = nil
  end
end

function ModelState:ShowParams()
  local pad = 5
  local y = pad
  local w = 200
  local hw = w/2 - pad*2
  local owner = self.owner
  
  local labels, inputs = {}, {}
  local params = owner.tool.model.params
  gui.set_style('align', 'right')
  for i, v in pairs(params) do
    local label = gui.Label(pad, y, i, hw)
    local input = gui.Input(w/2, y, hw, v)
    table.insert(labels, label)
    table.insert(inputs, input)
    input.on_change = function(self, v)
      local nv = tonumber(v)
      if nv then
        params[i] = nv
      end
    end
    y = y + input.height + pad
  end

  local ow, oh = owner:get_size()
  local nx, ny = self.library:get_position()
  local nw, nh = self.library:get_size()
  local yo = ny + nh + 1
  local win = gui.Window(ow - w, yo, w, y + pad, self.mode)
  local tw, th = win:get_title_size()
  win:set_position(nx, yo + th)
  win.node.depth = -1
  owner:add_child(win)
  for i, v in ipairs(labels) do
    win:add_child(v)
    win:add_child(inputs[i])
  end
  self.params = win
end

function ModelState:HideParams()
  if self.params then
    self.owner:remove_child(self.params)
    self.params:destroy()
    self.params = nil
  end
end