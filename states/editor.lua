EditorState = {}
EditorStateMT = { __index = EditorState }

setmetatable(EditorState, { __index = GameState })

function EditorState:Create(owner, mt)
  local self = GameState:Create(owner, mt or EditorStateMT)

  --self.level = level
  self.selection = {}
  self.selection2 = {}

  --self:ShowMenu()
  return self
end

function EditorState:Destroy()
  --self:HideMenu()
  
  --self.level = nil
  self.selection = nil
  self.selection2 = nil
  GameState.Destroy(self)
end

-- Selects a list of items
function EditorState:AddToSelection(s, s2)
  s2 = s2 or s
  for i, v in ipairs(s) do
    if table.find(self.selection, v) == nil then
      table.insert(self.selection, v)
      table.insert(self.selection2, s2[i])
    end
  end
end

-- Deselects a list of items
function EditorState:RemoveFromSelection(s)
  for i, v in ipairs(s) do
    local i2 = table.find(self.selection, v)
    if i2 then
      table.remove(self.selection, i2)
      table.remove(self.selection2, i2)
    end
  end
end

-- Deselects all items
function EditorState:ClearSelection()
  for i = #self.selection, 1, -1 do
    self.selection[i] = nil
    self.selection2[i] = nil
  end
end

-- Swaps two selected element
function EditorState:ShiftSelection(i, i2)
  local s1 = self.selection
  local s2 = self.selection2
  s1[i], s1[i2] = s1[i2], s1[i]
  s2[i], s2[i2] = s2[i2], s2[i]
end

function EditorState:RedrawGridRes(step, color, alpha)
  local scene = self.owner:get_scene()
  local camera = self.owner:get_camera()
  local istep = 1/step

  local ow, oh = self.owner:get_size()
  local hw, hh = ow/2, oh/2
  local l, t = camera:get_world_point(-hw, -hh)
  local r, b = camera:get_world_point(hw, hh)
  l = math.floor(l*istep)/istep
  t = math.floor(t*istep)/istep
  r = math.ceil(r*istep)/istep
  b = math.ceil(b*istep)/istep

  local aabb = level.world.aabb
  local lower = aabb.lowerBound
  local upper = aabb.upperBound
  -- vertical lines
  for x = l, r, step do
    if x >= lower.x and x <= upper.x then
      scene:DrawLine(x, lower.y, x, upper.y, 1, color, alpha)
    end
  end
  -- horizontal lines
  for y = t, b, step do
    if y >= lower.y and y <= upper.y then
      scene:DrawLine(lower.x, y, upper.x, y, 1, color, alpha)
    end
  end
end
--[[
function EditorState:RedrawGrid()
  if self.grid == false or self.gridres == nil then
    return
  end
  -- clamp grid resolution depending on the camera zoom
  local camera = self.owner:get_camera()
  local step = camera.scalex*30
  step = 10^math.floor(math.log10(step) + 0.5)
  step = math.max(step, self.gridres)
  self:RedrawGridRes(step, WHITE, 0.1, false)
  self:RedrawGridRes(step*10, WHITE, 0.2)
end

-- Changes grid resolution
function EditorState:ToggleGrid()
  if self.gridres == nil then
    self.gridres = 0.001
  end
  -- increment grid resolution
  self.gridres = self.gridres*10
  if self.gridres >= 10 then
    self.gridres = nil
  end
end
]]

-- Snaps world point to grid
function EditorState:ToGrid(x, y)
  local scene = self.owner:get_scene()
  return scene:ToGrid(x, y)
end


--[[
function EditorState:RedrawBody(body)
  local world = self.owner:get_world()
  world:RedrawBody(body, self.outlines, self.vertices)
end

function EditorState:RedrawBodies(bodies)
  local world = self.owner:get_world()
  world:RedrawBodies(body, self.outlines, self.vertices)
end
]]

function EditorState:RedrawJoint(joint, highlight)
  local color, alpha = WHITE, 1
  local draw = "DrawImage"
  if highlight then
    color = ORANGE
    draw = "DrawImageS"
  end
  local t = joint:GetType()
  local owner = self.owner
  local world = owner:get_world()
  local scene = owner:get_scene()
  local z = scene:GetZoom()

  local a1x, a1y = joint:GetAnchor1()
  local a2x, a2y = joint:GetAnchor2()
  if t == 'revolute' then
    -- rotation limits
    local e = joint:IsLimitEnabled()
    local l = joint:GetLowerLimit()
    local u = joint:GetUpperLimit()
    if e == true then
      local a = joint.def.body1:GetAngle() + math.pi/2
      l, u = l + a, u + a
      local x, y = math.cos(l)*z, math.sin(l)*z
      local x2, y2 = math.cos(u)*z, math.sin(u)*z
      --scene:DrawLine(a1x + x*4, a1y + y*4, a1x + x*12, a1y + y*12, 1, color, alpha)
      --scene:DrawLine(a1x + x2*4, a1y + y2*4, a1x + x2*12, a1y + y2*12, 1, color, alpha)
      --scene:DrawArcW(a1x, a1y, l, u, z*4, 1, color, alpha)
      local ld, ud = math.deg(l), math.deg(u)
      scene:DrawArc(a1x, a1y, 0, 360, 8, 1, RED, 1)
      scene:DrawArc(a1x, a1y, ld, ud, 8, 1, color, alpha)
      
      local x, y = math.cos(a)*z, math.sin(a)*z
      scene:DrawLine(a1x + x*8, a1y + y*8, a1x + x*12, a1y + y*12, 1, color, alpha)
    end
    -- overstreched revolute joint
    if a1x ~= a2x or a1y ~= a2y then
      scene:DrawLine(a1x, a1y, a2x, a2y, 1, RED, alpha)
      scene[draw](scene, "revoluteR", a2x, a2y)
    end
    scene[draw](scene, "revolute", a1x, a1y)
  elseif t == "weld" then
    -- overstreched weld joint
    if a1x ~= a2x or a1y ~= a2y then
      scene:DrawLine(a1x, a1y, a2x, a2y, 1, RED, alpha)
      scene[draw](scene, "rope", a2x, a2y)
    end
    scene[draw](scene, "rope", a1x, a1y)
  elseif t == 'distance' or t == 'rope' then
    scene:DrawLine(a1x, a1y, a2x, a2y, 1, color, alpha)
    scene[draw](scene, t, a1x, a1y)
    scene[draw](scene, t, a2x, a2y)
    -- overstreched distance joint?
    local dx, dy = a1x - a2x, a1y - a2y
    local d = math.sqrt(dx*dx + dy*dy)
    local l = joint.def.length
    if t == 'rope' then
      l = joint.def.maxLength
    end
    if d > l or d < l then
      local l = l - d
      local lx, ly = dx/d*l, dy/d*l
      scene:DrawLine(a1x, a1y, a1x + lx, a1y + ly, 1, RED, 1)
    end
  elseif t == 'prismatic' then
    scene[draw](scene, "prismatic", a1x, a1y)
    scene[draw](scene, "prismatic", a2x, a2y)

    local e = joint:IsLimitEnabled()
    local l = joint:GetLowerLimit()
    local u = joint:GetUpperLimit()
    local ax, ay = joint:GetWorldAxis()
    assert(ax ~= 0 or ay ~= 0)
    local sx, sy = a1x, a1y
    local ex, ey = a1x, a1y
    if e == true then
      sx, sy = sx + ax*l, sy + ay*l
      ex, ey = ex + ax*u, ey + ay*u
      -- overstreched prismatic joint
      local dx, dy = a1x - a2x, a1y - a2y
      local d = math.sqrt(dx*dx + dy*dy)
      if d > math.abs(u - l) or d < math.abs(l) then
        scene:DrawLine(ex, ey, a2x, a2y, 1, RED, 1)
      end
    else
      local w, h = screen:get_size()
      local diag = math.sqrt(w*w + h*h)*z
      sx, sy = sx + ax*-diag, sy + ay*-diag
      ex, ey = ex + ax*diag, ey + ay*diag
    end
    scene:DrawLine(sx, sy, ex, ey, 1, color, alpha)
    scene:DrawLine(a1x, a1y, a1x + ax*z*20, a1y + ay*z*20, 1, BLUE, 1)
  elseif t == 'pulley' then
    local a2x, a2y = joint:GetAnchor2()
    scene[draw](scene, "rope", a1x, a1y)
    scene[draw](scene, "rope", a2x, a2y)
    local g1x, g1y = joint:GetGroundAnchor1()
    local g2x, g2y = joint:GetGroundAnchor2()
    scene[draw](scene, "rope", g1x, g1y)
    scene[draw](scene, "rope", g2x, g2y)
    scene:DrawLine(a1x, a1y, g1x, g1y, 1, color, alpha*0.5)
    scene:DrawLine(g1x, g1y, g2x, g2y, 1, color, alpha*0.5)
    scene:DrawLine(g2x, g2y, a2x, a2y, 1, color, alpha*0.5)
  elseif t == 'gear' then
    local j1ax, j1ay = joint.def.joint1:GetAnchor1()
    local j2ax, j2ay = joint.def.joint2:GetAnchor2()
    a1x, a1y = (j1ax + j2ax)/2, (j1ay + j2ay)/2
    scene:DrawLine(a1x, a1y, j1ax, j1ay, 1, color, alpha*0.5)
    scene:DrawLine(a1x, a1y, j2ax, j2ay, 1, color, alpha*0.5)
    scene[draw](scene, "gear", a1x, a1y)
  end
end

function EditorState:RedrawJoints(joints)
  local world = self.owner:get_world()
  joints = joints or world.joints
  for i, v in pairs(joints) do
    self:RedrawJoint(v, color, alpha)
  end
end


local bimages =
{
  body = 0, box = 1, distance = 2, grid = 3,
  concave = 4, prismatic = 5, play = 6, nosnap = 7,
  group = 8, addvertex = 9, joint = 10, circle = 11,
  revolute = 12, selection = 13, snap = 14, ungroup = 15,
  pulley = 16, model = 17, triangle = 18, vertex = 19,
  chain = 20, group = 21, gravity = 22, loop = 23,
  rope = 24, weld = 25
}

local img = Image()
if img:load("boxy/icons/buttons.png") == false then
  img = nil
end

function EditorState:ShowMenu()
  local buttons =
  {
    { BodyState, SelectTool, "body", nil, "Edit bodies" },
    { VertexState, VSelectTool, "vertex", nil, "Edit vertices" },
    { VertexState, AddVertexTool, "addvertex", nil, "Insert vertex" },
    { VertexState, PolygonTool, "triangle", "concave", "New polygon" },
    { VertexState, PolygonTool, "concave", "chain", "New chain" },
    { VertexState, PolygonTool, "loop", "loop", "New loop" },
    { VertexState, CircleTool, "circle", nil, "New circle" },
    { VertexState, BoxTool, "box", nil, "New box" },
    --[[
    { VertexState, nil, "group" },
    { VertexState, nil, "ungroup" },]]
    { JointState, JSelectTool, "joint", nil, "Edit joints" },
    { JointState, AddJointTool, "revolute", "revolute", "New revolute joint" },
    { JointState, AddJointTool, "distance", "distance", "New distance joint" },
    { JointState, AddJointTool, "rope", "rope", "New rope joint" },
    { JointState, AddJointTool, "prismatic", "prismatic", "New prismatic joint" },
    { JointState, AddJointTool, "weld", "weld", "New weld joint" },
    { ModelState, AddModelTool, "model", nil, "New model" }
    --[[
    { VertexState, nil, "revolute" },
    { VertexState, nil, "pulley" }
    { PlayState, InteractTool, "play" }
    ]]
  }

  local owner = self.owner
  local pad = 2
  local bs = 40
  local w = #buttons*(pad + bs) + pad
  local h = bs + pad*2

  local win = gui.Window(0, 0, w, h, "Tools")
  local tw, th = win:get_title_size()
  win:set_position(0, th)
  win.node.depth = -1

  for i, v in ipairs(buttons) do
    local x, y = (pad + bs)*(i - 1), pad
    local b = gui.Image(x, y, nil, bs, bs)
    b.node.depth = -1
    b.tooltip = v[5]
    win:add_child(b)
    b.on_click = function(b, button)
      owner:set_state(v[1])
      owner:set_tool(v[2], v[4])
      owner:set_focus(nil)
    end
    --local img = display.gfx.load("boxy/icons/buttons.png")
    --local img = Image()
    --img:load("boxy/icons/buttons.png")
    local frame = bimages[v[3]]
    if img and frame then
      
      -- frame dimensions from cols and rows
      local sw = img.width/8
      local sh = img.height/8
      -- position in image
      local sx = frame%8*sw
      local sy = math.floor(frame/8)*sh
      
      --local sx, sy, sw, sh = img:get_subimage(frame, 8, 8)
      b:set_image(img, sx, sy, sw, sh)
    end
  end

  owner:add_child(win)
  self.menu = win
end

function EditorState:HideMenu()
  if self.menu then
    self.owner:remove_child(self.menu)
    self.menu = nil
  end
end



function EditorState:TranslateBodies(bodies, wdx, wdy)
  local j = {}
  for i, v in ipairs(bodies) do
    v:ChangePosition(wdx, wdy)

    local joints = v.world:GetAssociatedJoints(v)
    for i, joint in ipairs(joints) do
      j[joint] = joint
    end
  end

  -- translate associated joints (ugly)
  for i, joint in pairs(j) do
    local s1 = table.find(bodies, joint.def.body1)
    local s2 = table.find(bodies, joint.def.body2)
    if joint:GetType() == "pulley" then
      if s1 then
        local ga1 = joint.def.groundAnchor1
        ga1.x = ga1.x + wdx
        ga1.y = ga1.y + wdy
      end
      if s2 then
        local ga2 = joint.def.groundAnchor2
        ga2.x = ga2.x + wdx
        ga2.y = ga2.y + wdy
      end
    end
    -- translate if only one body was moved
    if s1 == nil or s2 == nil then
      self:ResetJoint(joint)
      --joint:Release()
      --joint:Init()
    end
  end
end

function EditorState:RotateBodies(bodies, wpx, wpy, da)
  local cosd = math.cos(da)
  local sind = math.sin(da)
  local j = {}
  for i, v in ipairs(bodies) do
    local vp = v.def.position
    local ox, oy = wpx - vp.x, wpy - vp.y
    local tx = cosd*ox - sind*oy
    local ty = cosd*oy + sind*ox
    local dx, dy = ox - tx, oy - ty
    v:ChangePosition(dx, dy)
    v:ChangeAngle(da)
    
    local joints = v.world:GetAssociatedJoints(v)
    for i, joint in ipairs(joints) do
      j[joint] = joint
    end
  end

  -- rotate associated joints (ugly)
  for i, joint in pairs(j) do
    local s1 = table.find(bodies, joint.def.body1)
    local s2 = table.find(bodies, joint.def.body2)
    --[[
    if joint:GetType() == "pulley" then
      if s1 then
        local g1 = joint.def.groundAnchor1
        local dx, dy = wpx - g1.x, wpy - g1.y
        
      end
      if s2 then
      
      end
    end
    ]]
    -- rotate if only one body was moved
    if s1 == nil or s2 == nil then
      self:ResetJoint(joint)
      --joint:Release()
      --joint:Init()
    end
  end
end

function EditorState:ScaleBodies(bodies, wpx, wpy, s)
  local j = {}
  for i, v in ipairs(bodies) do
    local vp = v.def.position
    local wvpx, wvpy = vp.x - wpx, vp.y - wpy
    wvpx, wvpy = wvpx*s + wpx, wvpy*s + wpy
    v:SetPosition(wvpx, wvpy)

    local os = v:GetScale()
    v:SetScale(os*s)
    --v.object:UpdateSprite(0)
    -- scaling bodies affects their mass

    local joints = v.world:GetAssociatedJoints(v)
    for i, joint in ipairs(joints) do
      j[joint] = joint
    end
  end
  
  -- scale associated joints (ugly)
  for i, joint in pairs(j) do
    local s1 = table.find(bodies, joint.def.body1)
    local s2 = table.find(bodies, joint.def.body2)

    local t = joint:GetType()
    if s1 and s2 then
      -- both bodies were scaled
      if t == 'distance' then
        -- adjust equilibrium length
        joint.def.length = joint.def.length*s
      elseif t == 'rope' then
        -- adjust equilibrium length
        joint.def.maxLength = joint.def.maxLength*s
      elseif t == 'prismatic' then
        -- adjust translation limits
        local l = joint.def.lowerTranslation
        local u = joint.def.upperTranslation
        local nl, nu = l*s, l*s + (u - l)*s
        if nu < nl then
          nl, nu = nu, nl
        end
        joint.def.lowerTranslation = nl
        joint.def.upperTranslation = nu
      elseif t == 'pulley' then
        -- todo: adjust limits
        joint.def.maxLengthA = joint.def.maxLengthA*s
        joint.def.maxLengthB = joint.def.maxLengthB*s
      end
    end
    if s1 then
      local a1 = joint.def.localAnchor1
      if a1 then
        a1.x = a1.x*s
        a1.y = a1.y*s
      end
      local ga1 = joint.def.groundAnchor1
      if ga1 then
        local dx, dy = ga1.x - wpx, ga1.y - wpy
        dx, dy = dx*s, dy*s
        ga1.x = wpx + dx
        ga1.y = wpy + dy
      end
    end
    if s2 then
      local a2 = joint.def.localAnchor2
      if a2 then
        a2.x = a2.x*s
        a2.y = a2.y*s
      end
      local ga2 = joint.def.groundAnchor2
      if ga2 then
        local dx, dy = ga2.x - wpx, ga2.y - wpy
        dx, dy = dx*s, dy*s
        ga2.x = wpx + dx
        ga2.y = wpy + dy
      end
    end
    self:ResetJoint(joint)
    --joint:Release()
    --joint:Init()
  end
end

function EditorState:TranslateVertices(vertices, shapes, wdx, wdy)
  local s = {}
  for i, v in ipairs(vertices) do
    local shape = shapes[i]
    local ldx, ldy = shape.body:GetLocalVector(wdx, wdy)
    v.x = v.x + ldx
    v.y = v.y + ldy
    s[shape] = shape
    shape:Sync()
  end
end

function EditorState:TranslateAnchors(anchors, joints, wdx, wdy)
  local j = {}
  for i, v in ipairs(anchors) do
    local joint = joints[i]
    j[joint] = joint
    local b1, b2 = joint.def.body1, joint.def.body2
    local a1, a2 = joint.def.localAnchor1, joint.def.localAnchor2
    if v == a1 or v == a2 then
      if v == a2 then
        b1, b2 = b2, b1
        a1, a2 = a2, a1
      end
      local ldx, ldy = b1:GetLocalVector(wdx, wdy)
      v.x = v.x + ldx
      v.y = v.y + ldy
    else
      v.x = v.x + wdx
      v.y = v.y + wdy
    end

    local t = joint:GetType()
    if t == 'revolute' or t == 'weld' then
      local wa1x, wa1y = b1:GetWorldPoint(a1.x, a1.y)
      local lx, ly = b2:GetLocalPoint(wa1x, wa1y)
      joint.def.localAnchor2.x = lx
      joint.def.localAnchor2.y = ly
    end
  end
  -- reinit joints
  for i, v in pairs(j) do
    self:ResetJoint(v)
    --v:Release()
    --v:Init()
  end
end

function EditorState:ResetJoint(v)
  local t = v:GetType()
  if t == 'prismatic' then
    -- reinit joint axis
    local b1 = v.def.body1
    local b2 = v.def.body2
    local a1 = v.def.localAnchor1
    local a2 = v.def.localAnchor2
    local wa1x, wa1y = b1:GetWorldPoint(a1.x, a1.y)
    local wa2x, wa2y = b2:GetWorldPoint(a2.x, a2.y)
    local dx, dy = wa1x - wa2x, wa1y - wa2y
    local d = math.sqrt(dx*dx + dy*dy)
    if d > 0 then
      local ax, ay = v.def.body1:GetLocalVector(dx/d, dy/d)
      -- todo:?
      if v.def.upperTranslation > 0 then
        ax, ay = -ax, -ay
      end
      v.def.localAxis1.x = ax
      v.def.localAxis1.y = ay
      v.def.referenceAngle = b2.def.angle - b1.def.angle
    end
  elseif t == 'pulley' then
    local a1x, a1y = v:GetAnchor1()
    local a2x, a2y = v:GetAnchor2()
    local g1x, g1y = v:GetGroundAnchor1()
    local g2x, g2y = v:GetGroundAnchor2()
    local d1x, d1y = a1x - g1x, a1y - g1y
    local d2x, d2y = a2x - g2x, a2y - g2y
    v.def.length1 = math.sqrt(d1x*d1x + d1y*d1y)
    v.def.length2 = math.sqrt(d2x*d2x + d2y*d2y)
  end
end

-- Deletes bodies
function EditorState:DeleteBodies(selection)
  local scene = self.owner:get_scene()
  local world = self.owner:get_world()
  for i = #selection, 1, -1 do
    local body = selection[i]
    world:DestroyBody(body)
    scene:RemoveBody(body)
  end
end

-- Group bodies
function EditorState:GroupBodies(selection)
  local scene = self.owner:get_scene()
  local world = self.owner:get_world()
  local f = selection[1]
  for i = #selection, 2, -1 do
    local body = selection[i]
    for _, s in ipairs(body.shapes) do
      local t = s:GetType()
      local p2 = s.def.density or 0
      local p3 = s.def.friction or 0
      local p4 = s.def.restitution or 0
      local p5 = s.def.isSensor or false
      if t == "circle" then
        local p1 = s.def.radius
        local lc = s.def.localPosition
        local lx, ly = body:GetWorldPoint(lc.x, lc.y)
        local p6, p7 = f:GetLocalPoint(lx, ly)
        f:CreateShapeEx(t, p1, p2, p3, p4, p5, p6, p7)
      elseif t == "polygon" or t == "concave" or t == "chain" then
        local vertices = {}
        for _, v in ipairs(s.def.vertices) do
          local lx, ly = body:GetWorldPoint(v.x, v.y)
          local x, y = f:GetLocalPoint(lx, ly)
          table.insert(vertices, { x = x, y = y })
        end
        local p1 = vertices
        f:CreateShapeEx(t, p1, p2, p3, p4, p5)
      end
    end

    local joints = world:GetAssociatedJoints(body)
    for _, j in ipairs(joints) do
      local def = j.def
      local bsz = "body1"
      local b1, b2 = def.body1, def.body2
      local a1, a2 = def.localAnchor1, def.localAnchor2
      if body == b2 then
        bsz = "body2"
        b1, b2 = b2, b1
        a2, a1 = a2, a1
      end
      if a1 then
        local w1x, w1y = b1:GetWorldPoint(a1.x, a1.y)
        local l1x, l1y = f:GetLocalPoint(w1x, w1y)
        a1.x = l1x
        a1.y = l1y
      end
      def[bsz] = f
      --[[
      if body == b2 then
        b1, b2 = b1, b2
      end
      local t = j:GetType()
      local a1 = j:GetAnchor2()
      if t == "revolute" then
      elseif t == "prismatic" then
      elseif t == "rope" then
      elseif t == "pulley" then
      elseif t == "gear" then
      elseif t == "distance" then
      end
      ]]
    end

    world:DestroyBody(body)
    scene:RemoveBody(body)
  end
  self:ClearSelection()
  self:AddToSelection({ f })
  scene:RedrawBody(f)
end

-- Deletes vertices
function EditorState:DeleteVertices(selection, selection2)
--[[
  -- hack that enables deleting multiple vertices in the same polygon shape
  for i, v in ipairs(selection2) do
    local t = v:GetType()
    if t == 'polygon' or t == 'box' then
      selection[i] = b2Vec2(selection[i])
    end
  end
  ]]
  local bodies = {}
  local shapes = {}
  for i, v in ipairs(selection) do
    local shape = selection2[i]
    local body = shape.body
    local t = shape:GetType()
    if t == 'polygon' or t == 'concave' or t == 'chain' or t == 'loop' then
      local vi = shape:FindVertex(v)
      shape:RemoveVertex(vi)
      shapes[shape] = shape
    elseif t == 'circle' then
      shapes[shape] = shape
    end
    bodies[body.id] = body
  end
  -- reinitialize shapes
  for i, shape in pairs(shapes) do
    local t = shape:GetType()
    local body = shape.body
    if t == 'polygon' or t == 'concave' or t == 'chain' or t == 'loop' then
      local vc = #shape.def.vertices
      if vc == 0 then
        body:DestroyShape(shape)
      end
    elseif t == 'circle' then
      body:DestroyShape(shape)
    end
  end
  local scene = self.owner:get_scene()
  -- destroy bodies with no shapes
  for i, v in pairs(bodies) do
    if #v.shapes == 0 then
      v.world:DestroyBody(v)
      scene:RemoveBody(v)
    else
      scene:RedrawBody(v)
    end
  end
  --self.Level:ClearSelection()
end

-- Deletes joints
function EditorState:DeleteJoints(selection, selection2)
  local joints = {}
  for i, v in ipairs(selection2) do
    joints[v] = v
  end
  for i, v in pairs(joints) do
    v.world:DestroyJoint(v)
  end
end


function EditorState:CopySelection()
  local selection = self.selection
  self.copy = {}
  for i, v in ipairs(selection) do
    local x = v:GetXML()
    local xsz = xml.str(x)
    table.insert(self.copy, xsz)
  end
  -- todo copy associated joints
  local world = self.owner:get_world()
  local joints = {}
  for i, j in pairs(world.joints) do
    local b1 = table.find(selection, j.def.body1)
    local b2 = table.find(selection, j.def.body2)
    if b1 and b2 then
      joints[j] = j
    end
  end
  for i, v in pairs(joints) do
    local x = v:GetXML()
    local xsz = xml.str(x)
    table.insert(self.copy, xsz)
  end
end

function EditorState:Paste()
  local owner = self.owner
  local bodies = {}
  local world = owner:get_world()
  local scene = owner:get_scene()
  local idlookup = {}
  for i, v in ipairs(self.copy) do
    v = xml.eval(v)
    local t = xml.tag(v)
    if t == "Body" then
      local id = world:GetAvailableID()
      idlookup[v.id] = id
      v.id = id
      local b = world:CreateBodyXML(v)
      table.insert(bodies, b)
    elseif t == "RevoluteJoint" or t == "PrismaticJoint" or t == "DistanceJoint" or t == "RopeJoint" then
      v.id = world:GetAvailableJID()
      v.body1 = idlookup[v.body1] or v.body1
      v.body2 = idlookup[v.body2] or v.body2
      world:CreateJointXML(v)
    end
  end
  --local mx, my = mouse.xaxis, mouse.yaxis
  --local wx, wy = scene.camera:get_world_point(mx, my)
  for i, v in ipairs(bodies) do
    --v:UpdateSprite(0)
    scene:AddBody(v)
    scene:RedrawBody(v)
  end
  return bodies
end




-- returns a list of bodies based on a query aabb
function EditorState:QueryBodies(wsx, wsy, wex, wey, sq)
  --sq = self:GetZoom()*sq
  if wsx == wex then
    wsx = wsx - b2.linearSlop/2
    wex = wex + b2.linearSlop/2
  end
  if wsy == wey then
    wsy = wsy - b2.linearSlop/2
    wey = wey + b2.linearSlop/2
  end
  -- ordered aabb
  local l = math.min(wsx, wex)
  local r = math.max(wsx, wex)
  local t = math.min(wsy, wey)
  local b = math.max(wsy, wey)

  local q = {}

  -- query shapes
  local q2 = {}
  local world = self.owner:get_world()
  world:QueryAABB(l, t, r, b, q2)
  for i, v in ipairs(q2) do
    local b = v:GetBody()
    if table.find(q, v) == nil then
      table.insert(q, b)
    end
  end

  -- query body centers
  for i, v in pairs(world.bodies) do
    if table.find(q, v) == nil then
      local px, py = v:GetPosition()
      local tx = px + sq < l or px - sq > r
      local ty = py + sq < t or py - sq > b
      if tx == false and ty == false then
        table.insert(q, v)
      end
    end
  end
  return q
end

function EditorState:TestVertex(vertex, body, l, t, r, b, sqw)
  local wvx, wvy = body:GetWorldPoint(vertex.x, vertex.y)
  local tx = wvx + sqw < l or wvx - sqw > r
  local ty = wvy + sqw < t or wvy - sqw > b
  return tx == false and ty == false
end

-- returns a list of vertices and shapes based on a query aabb
function EditorState:QueryVertices(wsx, wsy, wex, wey, sq)
  --local sqw = self:GetZoom()*sq
  -- ordered aabb
  local l = math.min(wsx, wex)
  local r = math.max(wsx, wex)
  local t = math.min(wsy, wey)
  local b = math.max(wsy, wey)
  local q, s = {}, {}
  local world = self.owner:get_world()
  for i, body in pairs(world.bodies) do
    for i, v in ipairs(body.shapes) do
      local type = v:GetType()
      if type == 'circle' then
        local vx = v.def.localPosition
        if self:TestVertex(vx, body, l, t, r, b, sq) then
          table.insert(q, vx)
          table.insert(s, v)
        end
      elseif type == 'polygon' or type == 'concave' or type == 'chain' or type == 'loop' then
        for i, vx in ipairs(v.def.vertices) do
          if self:TestVertex(vx, body, l, t, r, b, sq) then
            table.insert(q, vx)
            table.insert(s, v)
          end
        end
      end
    end
  end
  return q, s
end

-- returns a list of joints and shapes based on a query aabb
function EditorState:QueryJoints(wsx, wsy, wex, wey, sq)
  --local sqw = self:GetZoom()*sq
  local world = self.owner:get_world()
  local l = math.min(wsx, wex)
  local r = math.max(wsx, wex)
  local t = math.min(wsy, wey)
  local b = math.max(wsy, wey)
  local q, j = {}, {}
  for i, joint in pairs(world.joints) do
    local jt = joint:GetType()
    local a1x, a1y = joint:GetAnchor1()
    if jt == 'revolute' or jt == 'weld' or jt == 'prismatic' or jt == 'distance' or jt == 'rope' or jt == 'pulley' then
      local tx = a1x + sq < l or a1x - sq > r
      local ty = a1y + sq < t or a1y - sq > b
      if tx == false and ty == false then
        table.insert(q, joint.def.localAnchor1)
        table.insert(j, joint)
      end
    end
    if jt == 'distance' or jt == 'prismatic' or jt == 'rope' or jt == 'pulley' then
      -- distance/prismatic joints have a second anchor
      local a2x, a2y = joint:GetAnchor2()
      local tx = a2x + sq < l or a2x - sq > r
      local ty = a2y + sq < t or a2y - sq > b
      if tx == false and ty == false then
        table.insert(q, joint.def.localAnchor2)
        table.insert(j, joint)
      end
    end
    if jt == 'pulley' then
      local ga1x, ga1y = joint:GetGroundAnchor1()
      local tx = ga1x + sq < l or ga1x - sq > r
      local ty = ga1y + sq < t or ga1y - sq > b
      if tx == false and ty == false then
        table.insert(q, joint.def.groundAnchor1)
        table.insert(j, joint)
      end
      local ga2x, ga2y = joint:GetGroundAnchor2()
      local tx = ga2x + sq < l or ga2x - sq > r
      local ty = ga2y + sq < t or ga2y - sq > b
      if tx == false and ty == false then
        table.insert(q, joint.def.groundAnchor2)
        table.insert(j, joint)
      end
    end
  end
  return q, j
end

-- returns a list of anchors and bodies based on a query point
function EditorState:QueryAnchors(joints, wx, wy, sq)
  --local sqw = self:GetZoom()*sq
  local j, b = {}, {}
  for i, joint in ipairs(joints) do
    local b1, b2 = joint.def.body1, joint.def.body2
    local cm1x, cm1y = b1:GetWorldCenter()
    local cm2x, cm2y = b2:GetWorldCenter()
    cm1x, cm1y = cm1x - sq*2, cm1y - sq*2
    cm2x, cm2y = cm2x + sq*2, cm2y - sq*2
    if wx > cm1x - sq and wx < cm1x + sq then
      if wy > cm1y - sq and wy < cm1y + sq then
        table.insert(j, joint)
        table.insert(b, b1)
      end
    end
    if wx > cm2x - sq and wx < cm2x + sq then
      if wy > cm2y - sq and wy < cm2y + sq then
        table.insert(j, joint)
        table.insert(b, b2)
      end
    end
  end
  return j, b
end