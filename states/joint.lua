--
-- Hovers over joints
--
JointState = {}
JointStateMT = { __index = JointState }

setmetatable(JointState, { __index = EditorState })

local props =
{
  revolute =
  {
    { "enableLimit", "boolean", "Enables the joint limit" },
    { "lowerAngle", "number", "Minimum angle of the joint in radians" },
    { "upperAngle", "number", "Maximum angle of the joint in radians" },
    { "enableMotor", "boolean", "Enables the joint motor" },
    { "motorSpeed", "number", "Motor speed in radians/sec" },
    { "maxMotorTorque", "number", "Maximum joint force used by the motor" },
    { "collideConnected", "boolean", "Determines if the two joined bodies should collide" },
    { "id", "number", "Joint identifier" }
  },
  weld =
  {
    { "frequencyHz", "number", "Frequency in Hz" },
    { "dampingRatio", "number", "Damping ratio" },
    { "collideConnected", "boolean", "Determines if the two joined bodies should collide" },
    { "id", "number", "Joint identifier" }
  },
  prismatic =
  {
    { "enableLimit", "boolean", "Enables the joint limit" },
    { "lowerTranslation", "number", "Minimum translation of the joint in meters" },
    { "upperTranslation", "number", "Maximum translation of the joint in meters" },
    { "enableMotor", "boolean", "Enables the joint motor" },
    { "motorSpeed", "number", "Motor speed in meters/sec" },
    { "maxMotorForce", "number", "Maximum joint force used by the motor" },
    { "collideConnected", "boolean", "Determines if the two joined bodies should collide" },
    { "id", "number", "Joint identifier" }
  },
  distance =
  {
    { "length", "number", "Default length of the joint" },
    { "frequencyHz", "number", "Frequency in Hz" },
    { "dampingRatio", "number", "Damping ratio" },
    { "collideConnected", "boolean", "Determines if the two joined bodies should collide" },
    { "id", "number", "Joint identifier" }
  },
  rope = 
  {
    { "maxLength", "number", "Maximum length of the joint" },
    { "collideConnected", "boolean", "Determines if the two joined bodies should collide" },
    { "id", "number", "Joint identifier" }
  },
  pulley = 
  {
    { "length1", "number", "Length of the joint segment A" },
    { "length2", "number", "Length of the joint segment B" },
    { "maxLength1", "number", "Maximum length of the joint segment A" },
    { "maxLength2", "number", "Maximum length of the joint segment B" },
    { "ratio", "number", "Ratio between the segments" },
    { "collideConnected", "boolean", "Determines if the two joined bodies should collide" },
    { "id", "number", "Joint identifier" }
  }
}

function JointState:Create(owner)
  local self = EditorState:Create(owner, JointStateMT)

  return self
end

function JointState:Destroy()
  self:ClearSelection()
  self:RedrawSelectedJoints()
  
  self:HideProperties()
  self:DestroyProperties()
  EditorState.Destroy(self)
end

function JointState:Select()
  local scene = self.owner:get_scene()
  scene:SetStyle(true, false)
end

function JointState:Deselect()
  self:ClearSelection()
  self:RedrawSelectedJoints()
  self:HideProperties()
  self:DestroyProperties()
end

function JointState:MousePress(wx, wy, alt, shift)

end

function JointState:Redraw()
  self:RedrawJoints()
  self:RedrawSelectedJoints()

  if self.properties == nil or self.properties.is_focused ~= true then
    self:UpdateProperties()
  end
end

function JointState:KeyPress(key)
  if key == KEY_DEL or key == KEY_NUMDEL then
    self:DeleteJoints(self.selection, self.selection2)
    self:ClearSelection()
    self.owner:set_tool(JSelectTool)
  end
end

function JointState:RedrawSelectedJoints()
  -- un-highlight bodies
  local scene = self.owner:get_scene()
  scene:ClearBodyHighlights()

  -- selected anchors
  for i, anchor in ipairs(self.selection) do
    local joint = self.selection2[i]
    local b1, b2 = joint.def.body1, joint.def.body2
    if anchor == joint.def.localAnchor2 then
      b1, b2 = b2, b1
    end
    assert(b1 and b2, "Missing body in joint: " .. joint.id)
    -- highlight bodies
    scene:HighlightBody(b1)
    scene:HighlightBody(b2)
   --[[ 
    self:RedrawJoint(joint, ORANGE, 1)
]]
    -- highlight anchor
    local wvx, wvy = b1:GetWorldPoint(anchor.x, anchor.y)
    local t = joint:GetType()
    scene:DrawImageS(t, wvx, wvy)

  end
end


function JointState:GetProperties(joint)
  local def = joint.def
  local t = joint:GetType()
  for i, v in ipairs(props[t]) do
    for _, p in pairs(self.props) do
      local l = v[1]
      if l == p[1] then
        if p[2] == "boolean" then
          p[4].is_checked = def[l]
        elseif p[2] == "number" then
          local v = def[l]
          if l == "id" then
            v = joint.id
          end
          p[4].text = v
          p[4]:clear_selection()
        end
      end
    end
  end
end

function JointState:ChangeProperty(l, i, value)
  if #self.selection == 0 then
    return
  end
  if l == "id" then
    self.selection2[1].id = value
  end
  for _, v in ipairs(self.selection2) do
    if l ~= "id" then
      v.def[l] = value
    end
  end
end

function JointState:UpdateProperties()
  self:ClearProperties()
  if #self.selection == 0 then
    self:HideProperties()
    self:DestroyProperties()
    return
  end
  local t = self.selection2[1]:GetType()
  for i, v in ipairs(self.selection2) do
    if v:GetType() ~= t then
      self:HideProperties()
      self:DestroyProperties()
      return
    end
  end
  if self.properties == nil then
    self:CreateProperties(t .. " joint properties", props[t])
  end
  self:ShowProperties()
  
  self:GetProperties(self.selection2[1])
  for i = #self.selection2, 2, -1 do
    local joint = self.selection2[i]
    local def = joint.def
    
    for _, p in pairs(self.props) do
      if p[2] == "boolean" then
        if p[4].is_checked ~= def[ p[1] ] then
          p[4].is_checked = nil
        end
      elseif p[2] == "number" then
        if p[4].text ~= def[ p[1] ] then
          p[4].text = ""
        end
      end
    end
  end
end