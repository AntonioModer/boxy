LoadingState = {}
LoadingStateMT = { __index = LoadingState }

setmetatable(LoadingState, { __index = GameState })

function LoadingState:Create(owner, mt)
  local self = GameState:Create(owner, mt or LoadingStateMT)
  assert(false)
  return self
end

function LoadingState:Destroy()
  GameState.Destroy(self)
end

