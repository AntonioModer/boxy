--
-- Hovers over bodies
--
BodyState = {}
BodyStateMT = { __index = BodyState }

setmetatable(BodyState, { __index = EditorState })

function BodyState:Create(owner)
  local self = EditorState:Create(owner, BodyStateMT)

  local p =
  {
    { "type", { "staticBody", "dynamicBody", "kinematicBody" }, "Determines how the body responds to collisions" },
    { "x", "number", "X-position in meters" },
    { "y", "number", "Y-position in meters" },
    { "angle", "number", "Angle in radians" },
    { "scale", "number", "Scales shapes attached to the body" },
    { "linearDamping", "number", "Decreases linear velocity over time" },
    { "angularDamping", "number", "Decreases angular velocity over time" },
    { "fixedRotation", "boolean", "Disables rotation of the body" },
    { "isBullet", "boolean", "Forces continuous collision detection for the body" },
    { "allowSleep", "boolean", "Allows the body to enter a \"sleep\" state" },
    { "isSleeping", "boolean", "Does not simulate the body when idle" },
    { "active", "boolean", "Does not simulate the body until activated" },
    { "gravityScale", "number", "Gravity scalar for the body" },
    { "linearVel-X", "number", "Linear velocity X-value in meters/sec" },
    { "linearVel-Y", "number", "Linear velocity Y-value in meters/sec" },
    { "angularVel", "number", "Angular velocity in radians/sec" },
    { "id", "number", "Body identifier" }
  }
  self:CreateProperties("Body properties", p)
  return self
end

function BodyState:Destroy()
  self:DestroyProperties()
  EditorState.Destroy(self)
end

function BodyState:Select()
  local scene = self.owner:get_scene()
  scene:SetStyle(true, false)
  
  self:ShowProperties()
end

function BodyState:Deselect()
  self:HideProperties()
  self:ClearSelection()
  self:ClearAABB()
  self:RedrawSelectedBodies()
end

function BodyState:MousePress(wx, wy, alt, shift, ctrl)

end

function BodyState:MouseMove(wx, wy, owx, owy)

end

function BodyState:MouseRelease(wx, wy)
  
end

function BodyState:KeyPress(key, ctr, shift)
  local owner = self.owner
  if key == KEY_DEL or key == KEY_NUMDEL then
    self:DeleteBodies(self.selection)
    self:ClearSelection()
    self:ClearAABB()
    owner:set_tool(SelectTool)
  end

  if ctr == true then
    if key == KEY_C or key == KEY_X then
      self:CopySelection()
      if key == KEY_X then
        self:DeleteBodies(self.selection)
        self:ClearSelection()
        self:ClearAABB()
      end
    elseif key == KEY_V then
      local s = self:Paste()
      self:ClearSelection()
      self:AddToSelection(s)
      self:AABBFromSelection()
    elseif key == KEY_A then
      self:ClearSelection()
      self:ClearAABB()
      local s = {}
      local world = self.owner:get_world()
      for i, v in pairs(world.bodies) do
        table.insert(s, v)
      end
      self:AddToSelection(s)
    elseif key == KEY_G then
      self:GroupBodies(self.selection)
    end
  end

  local selection = self.selection
  if ctr == true and #selection > 0 then
    if key == KEY_R then
      if self.pivotx == nil or self.pivoty == nil then
        self:AABBFromSelection()
      end
      local wpx, wpy = self:GetWorldPivot()
      -- todo pivot is nil when selected body has no aabb
      owner:set_tool(RotateTool, selection, wpx, wpy)
    elseif key == KEY_T then
      if self.pivotx == nil or self.pivoty == nil then
        self:AABBFromSelection()
      end
      owner:set_tool(TranslateTool, selection)
    elseif key == KEY_Y then
      --local sx, sy = gui.get_mouse_position()
      --owner:set_tool(ScaleTool, selection, sx, sy)
    elseif key == KEY_F then
      if self.pivotx == nil or self.pivoty == nil then
        self:AABBFromSelection()
      end
      local wpx, wpy = self:GetWorldPivot()
      local selection = self.selection
      for i, body in ipairs(selection) do
        local p = body.def.position
        p.x = -(p.x - wpx) + wpx
        body.def.angle = -body.def.angle
        for j, shape in ipairs(body.shapes) do
          if shape.def.vertices then
            for k, vertex in ipairs(shape.def.vertices) do
              vertex.x = -vertex.x
            end
          elseif shape.def.localPosition then
            shape.def.localPosition.x = -shape.def.localPosition.x
          end
          shape:Sync()
        end
      end
      scene:RedrawBodiesList(selection)
    end
  end
end

function BodyState:Redraw()
  self:RedrawControls()
  self:RedrawJoints()
  self:RedrawSelectedBodies()

  if self.properties.is_focused ~= true then
    self:UpdateProperties()
  end
end

function BodyState:RedrawControls()
  local owner = self.owner
  local scene = owner:get_scene()
  local camera = owner:get_camera()

  -- pivot
  local wpx, wpy = self.pivotx, self.pivoty
  if wpx and wpy then
    scene:DrawImage("center", wpx, wpy)
  end

  local l, t = self.left, self.top
  local r, b = self.right, self.bottom
  if l and t and r and b then
    -- rotator
    local scale = camera.scalex
    local lcx = (r + l)/2
    local lry = self.top - 15*scale
    local wrx, wry = self:AABBToWorld(lcx, lry)
    scene:DrawImage("rotate", wrx, wry)

    -- aabb
    local x1, y1 = self:AABBToWorld(l, t)
    local x2, y2 = self:AABBToWorld(r, t)
    local x3, y3 = self:AABBToWorld(r, b)
    local x4, y4 = self:AABBToWorld(l, b)

    scene:DrawLine(x1, y1, x2, y2, 1, WHITE, 1)
    scene:DrawLine(x2, y2, x3, y3, 1, WHITE, 1)
    scene:DrawLine(x3, y3, x4, y4, 1, WHITE, 1)
    scene:DrawLine(x4, y4, x1, y1, 1, WHITE, 1)

    -- resizers
    scene:DrawImage("resizer", x1, y1)
    scene:DrawImage("resizer", x2, y2)
    scene:DrawImage("resizer", x3, y3)
    scene:DrawImage("resizer", x4, y4)

    local cx, cy = (l + r)/2,(t + b)/2
    local x5, y5 = self:AABBToWorld(cx, t)
    local x6, y6 = self:AABBToWorld(r, cy)
    local x7, y7 = self:AABBToWorld(cx, b)
    local x8, y8 = self:AABBToWorld(l, cy)

    scene:DrawImage("resizer", x5, y5)
    scene:DrawImage("resizer", x6, y6)
    scene:DrawImage("resizer", x7, y7)
    scene:DrawImage("resizer", x8, y8)
  end
end

-- Redraws all selected bodies and joints
function BodyState:RedrawSelectedBodies()
  -- un-highlight all bodies
  local world = self.owner:get_world()
  local scene = self.owner:get_scene()
  
  scene:ClearBodyHighlights()
  -- highlight selected bodies
  local selection = self.selection
  for i, v in ipairs(selection) do
    scene:HighlightBody(v)
  end
  -- highlight joint if both bodies are selected
  for i, j in pairs(world.joints) do
    local b1 = table.find(selection, j.def.body1)
    local b2 = table.find(selection, j.def.body2)
    if b1 and b2 then
    --[[
      local t = j:GetType()
      local wvx, wvy = j:GetAnchor1()
      if wvx and wvy then
        scene:DrawImageS(t, wvx, wvy)
      end
      local wvx, wvy = j:GetAnchor2()
      if wvx and wvy then
        scene:DrawImageS(t, wvx, wvy)
      end
      ]]
      self:RedrawJoint(j, ORANGE, 1)
    end
  end
end

function BodyState:TestResizers(wx, wy, sq)
  local l, t = self.left, self.top
  local r, b = self.right, self.bottom
  if l == nil or t == nil or r == nil or b == nil then
    return
  end
  local cx, cy = (l + r)/2, (t + b)/2
  -- clicked on the border of the selection box?
  local lx, ly = self:WorldToAABB(wx, wy)
  local sx, sy
  if lx > cx - sq and lx < cx + sq then
    sx = 0
  elseif lx > l - sq and lx < l + sq then
    sx = -1
  elseif lx > r - sq and lx < r + sq then
    sx = 1
  end
  if ly > cy - sq and ly < cy + sq then
    sy = 0
  elseif ly > t - sq and ly < t + sq then
    sy = -1
  elseif ly > b - sq and ly < b + sq then
    sy = 1
  end
  if (sx and sy) and (sx ~= 0 or sy ~= 0) then
    return sx, sy
  end
end

function BodyState:GetResizer(sx, sy)
  local l, t = self.left, self.top
  local r, b = self.right, self.bottom
  local px, py = (r + l)/2,(b + t)/2
  if sx == -1 then
    px = l
  elseif sx == 1 then
    px = r
  end
  if sy == -1 then
    py = t
  elseif sy == 1 then
    py = b
  end
  return px, py
end

function BodyState:TestRotator(wx, wy, sq)
  if self.rotation == nil then
    return
  end
  local camera = self.owner:get_camera()
  local scale = camera.scalex
  local lrx = (self.right + self.left)/2
  local lry = self.top - scale*15
  local rx, ry = self:AABBToWorld(lrx, lry) 
  local rl, rt = rx - sq*2, ry - sq*2
  local rr, rb = rx + sq*2, ry + sq*2
  return wx > rl and wx < rr and wy > rt and wy < rb
end

function BodyState:TestPivot(wx, wy, sq)
  local wpx, wpy = self.pivotx, self.pivoty
  if wpx == nil or wpy == nil then
    return
  end
  local pl, pt = wpx - sq, wpy - sq
  local pr, pb = wpx + sq, wpy + sq
  return wx > pl and wx < pr and wy > pt and wy < pb
end

function BodyState:SetWorldPivot(wpx, wpy, custom)
  if self.pivotx and self.pivoty then
    if self.left and self.right and self.top and self.bottom then
      local lpx, lpy = self:WorldToAABB(wpx, wpy)
      local olpx, olpy = self:WorldToAABB(self.pivotx, self.pivoty)
      local dx, dy = olpx - lpx, olpy - lpy
      self.left = self.left + dx
      self.right = self.right + dx
      self.top = self.top + dy
      self.bottom = self.bottom + dy
    end
  end
  self.pivotx = wpx
  self.pivoty = wpy
  self.custompivot = (custom == true)
end

function BodyState:SetAABBPivot(lx, ly, custom)
  local wpx, wpy = self:AABBToWorld(lx, ly)
  self:SetWorldPivot(wpx, wpy, custom)
end

function BodyState:GetWorldPivot()
  return self.pivotx, self.pivoty, self.custompivot
end

function BodyState:AABBFromSelection()
  local selection = self.selection
  -- todo check for bodies with no shapes/aabb
--[[
  if #self.selection == 1 then
    local s = self.selection[1]
    local p = s.def.position
    local l, t, r, b = s:GetAABB()
    self.left, self.top = l, t
    self.right, self.bottom = r, b
    self.rotation = s.def.angle
    self.pivotx, self.pivoty = p.x, p.y
  else
]]
  local wl, wt, wr, wb = selection[1]:GetWorldAABB()
  for i = 2, #selection do
    local v = selection[i]
    local l, t, r, b = v:GetWorldAABB()
    if l and t and r and b then
      wl = math.min(l, wl or l)
      wr = math.max(r, wr or r)
      wt = math.min(t, wt or t)
      wb = math.max(b, wb or b)
    end
  end
  if wl == nil or wt == nil or wr == nil or wb == nil then
    self:ClearAABB()
    return
  end
  local wpx, wpy = (wr + wl)/2, (wt + wb)/2
  self.pivotx, self.pivoty = wpx, wpy
  self:SetAABB(wl, wt, wr, wb)

  self.custompivot = false
end

function BodyState:SetAABB(sx, sy, ex, ey)
  if sx > ex then
    sx, ex = ex, sx
  end
  if sy > ey then
    sy, ey = ey, sy
  end
  local wpx, wpy = self.pivotx, self.pivoty
  self.left, self.top = sx - wpx, sy - wpy
  self.right, self.bottom = ex - wpx, ey - wpy
  self.rotation = 0
end

function BodyState:ClearAABB()
  self.pivotx = nil
  self.pivoty = nil
  self.rotation = nil
  self.left, self.top = nil, nil
  self.right, self.bottom = nil, nil
  self.custompivot = nil
end

function BodyState:WorldToAABB(wx, wy)
  local a = -self.rotation
  local ox, oy = wx - self.pivotx, wy - self.pivoty
  local cd = math.cos(a)
  local sd = math.sin(a)
  local tx = cd*ox - sd*oy
  local ty = cd*oy + sd*ox
  return tx, ty
end

function BodyState:AABBToWorld(lx, ly)
  local a = self.rotation
  local cd = math.cos(a)
  local sd = math.sin(a)
  local tx = cd*lx - sd*ly
  local ty = cd*ly + sd*lx
  return tx + self.pivotx, ty + self.pivoty
end

function BodyState:TranslateSelection(wdx, wdy)
  -- todo: bodies with shapes have no aabb
  if self.pivotx and self.pivoty then
    self.pivotx = self.pivotx + wdx
    self.pivoty = self.pivoty + wdy
  end
  
  -- todo:make sure the AABB remains inside the world bounds
  local selection = self.selection
  if #selection > 0 then
    local world = self.owner:get_world()
    self:TranslateBodies(selection, wdx, wdy)
    local scene = self.owner:get_scene()
    scene:RedrawBodiesList(selection)
  end
end

function BodyState:RotateSelection(da)
  if self.rotation == nil then
    return
  end
  self.rotation = self.rotation + da

  -- todo:make sure the AABB remains inside the world bounds
  local selection = self.selection
  local wpx, wpy = self.pivotx, self.pivoty
  --local world = self.owner:get_world()
  self:RotateBodies(selection, wpx, wpy, da)
  local scene = self.owner:get_scene()
  scene:RedrawBodiesList(selection)
end

function BodyState:ScaleSelection(s)
  if self.left == nil or self.right == nil or self.top == nil or self.bottom == nil then
    return
  end
  local l, t = self.left*s, self.top*s
  local r, b = self.right*s, self.bottom*s
  self.left = math.min(l, r)
  self.right = math.max(l, r)
  self.top = math.min(t, b)
  self.bottom = math.max(t, b)

  -- todo:make sure the AABB remains inside the world bounds
  local selection = self.selection
  local wpx, wpy = self.pivotx, self.pivoty
  --local world = self.owner:get_world()
  self:ScaleBodies(selection, wpx, wpy, s)
  local scene = self.owner:get_scene()
  scene:RedrawBodiesList(selection)
end

-- Tests a world point vs the selection AABB
function BodyState:TestAABB(wx, wy)
  local l, t = self.left, self.top
  local r, b = self.right, self.bottom
  if l == nil or t == nil or r == nil or b == nil then
    return false
  end
  local lx, ly = self:WorldToAABB(wx, wy)
  return lx > l and lx < r and ly > t and ly < b
end



function BodyState:GetProperties(body)
  local def = body.def
  local t = def.type
  local ti = 0
  if t == "staticBody" then
    ti = 1
  elseif t == "dynamicBody" then
    ti = 2
  elseif t == "kinematicBody" then
    ti = 3
  end
  self.props[1][4].index = ti
  self.props[2][4].text = def.position.x
  self.props[3][4].text = def.position.y
  self.props[4][4].text = def.angle
  self.props[5][4].text = def.scale
  self.props[6][4].text = def.linearDamping
  self.props[7][4].text = def.angularDamping
  self.props[8][4].is_checked = def.fixedRotation
  self.props[9][4].is_checked = def.isBullet
  self.props[10][4].is_checked = def.allowSleep
  self.props[11][4].is_checked = def.isSleeping
  self.props[12][4].is_checked = def.active
  self.props[13][4].text = def.gravityScale
  local lvx, lvy = body:GetLinearVelocity()
  self.props[14][4].text = lvx
  self.props[15][4].text = lvy
  local av = body:GetAngularVelocity()
  self.props[16][4].text = av
  self.props[17][4].text = body.id
  
  self.props[2][4]:clear_selection()
  self.props[3][4]:clear_selection()
  self.props[4][4]:clear_selection()
  self.props[5][4]:clear_selection()
end

function BodyState:ChangeProperty(l, i, value)
  local body = self.selection[1]
  if body == nil then
    return
  end
  if self.pivotx == nil or self.pivoty == nil then
    self:AABBFromSelection()
  end
  local x, y = body:GetPosition()
  local a = body:GetAngle()
  if l == "type" then
    for _, v in ipairs(self.selection) do
      v.def.type = value
    end
    local scene = self.owner:get_scene()
    scene:RedrawBodiesList(self.selection)
  elseif l == "x" then
    self:TranslateSelection(value - x, 0)
  elseif l == "y" then
    self:TranslateSelection(0, value - y)
  elseif l == "angle" then
    self:RotateSelection(value - a)
  elseif l == "scale" then
    local s = body:GetScale()
    self:ScaleSelection(value/s)
  elseif l == "linearVel-X" then
    for _, v in ipairs(self.selection) do
      local lvx, lvy = v:GetLinearVelocity()
      v:SetLinearVelocity(value, lvy)
    end
  elseif l == "linearVel-Y" then
    for _, v in ipairs(self.selection) do
      local lvx, lvy = v:GetLinearVelocity()
      v:SetLinearVelocity(lvx, value)
    end
  elseif l == "angularVel" then
    for _, v in ipairs(self.selection) do
      v:SetAngularVelocity(value)
    end
  elseif l == "id" then
    value = tonumber(value) or value
    local exist = false
    local world = self.owner:get_world()
    for i, b2 in pairs(world.bodies) do
      if b2.id == value then
        exist = true
        break
      end
    end
    if not exist and #self.selection == 1 then
      body.id = value
    end
  else
    local scene = self.owner:get_scene()
    for _, v in ipairs(self.selection) do
      v.def[l] = value
      scene:RedrawBody(v)
    end
  end
end

function BodyState:UpdateProperties()
  self:ClearProperties()
  if #self.selection == 0 then
    self:HideProperties()
    return
  end
  self:ShowProperties()
  
  self:GetProperties(self.selection[1])
  local props = self.props
  for i = #self.selection, 2, -1 do
    local body = self.selection[i]
    local def = body.def
    if props[1][4].items[props[1][4].index] ~= def.type then
      props[1][4].index = 0
    end
    --[[
    if props[2][4].text ~= def.position.x then
      props[2][4].text = ""
    end
    if props[3][4].text ~= def.position.y then
      props[3][4].text = ""
    end
    if props[4][4].text ~= def.angle then
      props[4][4].text = ""
    end
    if props[5][4].text ~= body.scale then
      props[5][4].text = ""
    end
    ]]
    if props[6][4].text ~= def.linearDamping then
      props[6][4].text = ""
    end
    if props[7][4].text ~= def.angularDamping then
      props[7][4].text = ""
    end
    if props[8][4].is_checked ~= def.fixedRotation then
      props[8][4].is_checked = nil
    end
    if props[9][4].is_checked ~= def.isBullet then
      props[9][4].is_checked = nil
    end
    if props[10][4].is_checked ~= def.allowSleep then
      props[10][4].is_checked = nil
    end
    if props[11][4].is_checked ~= def.isSleeping then
      props[11][4].is_checked = nil
    end
    if props[12][4].is_checked ~= def.active then
      props[12][4].is_checked = nil
    end
    if props[13][4].text ~= def.gravityScale then
      props[13][4].text = ""
    end
    local lvx, lvy = body:GetLinearVelocity()
    if props[14][4].text ~= lvx then
      props[14][4].text = ""
    end
    if props[15][4].text ~= lvy then
      props[15][4].text = ""
    end
    local av = body:GetAngularVelocity()
    if props[16][4].text ~= av then
      props[16][4].text = ""
    end
    props[17][4].text = ""
  end
end