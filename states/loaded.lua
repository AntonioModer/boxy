LoadedState = {}
LoadedStateMT = { __index = LoadedState }

setmetatable(LoadedState, { __index = GameState })

function LoadedState:Create(owner, mt)
  local self = GameState:Create(owner, mt or LoadedStateMT)
  assert(false)
  return self
end

function LoadedState:Destroy()
  GameState.Destroy(self)
end

