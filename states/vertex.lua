--
-- Hovers over vertices
--
VertexState = {}
VertexStateMT = { __index = VertexState }

setmetatable(VertexState, { __index = EditorState })

function VertexState:Create(owner)
  local self = EditorState:Create(owner, VertexStateMT)

  local p =
  {
    { "density", "number", "Density of the shape in Kg/m2" },
    { "friction", "number", "Friction coefficient for collisions. Must be between 0 and 1" },
    { "restitution", "number", "Affects elasticity of collisions. Must be between 0 and 1" },
    { "isSensor", "boolean", "Makes the shape a sensor allowing other shapes to pass through it" },
    { "categoryBits", "number", "Filter category" },
    { "maskBits", "number", "Filter mask" },
    { "groupIndex", "number", "Filter group" }
  }
  self:CreateProperties("Shape properties", p)

  return self
end

function VertexState:Destroy()
  self:DestroyProperties()
  EditorState.Destroy(self)
end

function VertexState:Select()
  local scene = self.owner:get_scene()
  scene:SetStyle(true, true)
  self:ShowProperties()
end

function VertexState:Deselect()
  self:HideProperties()
  self:ClearSelection()
  self:RedrawSelectedVertices()
end

function VertexState:MousePress(wx, wy, alt, shift)

end

function VertexState:MouseMove(wx, wy, owx, owy)
end

function VertexState:MouseRelease(wx, wy)
end

function VertexState:KeyPress(key, ctr, shift)
  local owner = self.owner
  local scene = owner:get_scene()
  if key == KEY_W then
    owner:set_tool(PolygonTool, "concave")
  elseif key == KEY_Q then
    owner:set_tool(AddVertexTool)
  elseif key == KEY_DEL or key == KEY_NUMDEL then
    self:DeleteVertices(self.selection, self.selection2)
    -- todo
    for i, v in ipairs(self.selection2) do
      if v and v.body then
        scene:RedrawBody(v.body)
      end
    end
    self:ClearSelection()
    owner:set_tool(VSelectTool)
  end
  if ctr == true then
    if key == KEY_A then
      self:SelectAll()
    end
  end
  --[[
  if keyboard:is_down(KEY_LCTRL) or keyboard:is_down(KEY_RCTRL) then
    if key == KEY_Y then
      self:GroupShapes(self.selection2)
    elseif key == KEY_U then
      self:UnGroupShapes(self.selection2)
    end
  end
  ]]
end

function VertexState:GroupShapes(shapes)
  self:ClearSelection()
  --[[
  -- get unique shapes
  local unique = {}
  for i, v in ipairs(shapes) do
    local exists = false
    for i2, v2 in ipairs(unique) do
      if v2 == v then
        exists = true
      end
    end
    if exists == false then
      table.insert(unique, v)
    end
  end
  if #unique == 0 then
    return
  end
  -- remove shapes from the first body
  local first = table.remove(unique)
  local firstb = first:GetBody()
  for i = #unique, 1, -1 do
    if v:GetBody() == firstb then
      table.remove(unique, i)
    end
  end
  -- transfer shapes
  for i, v in ipairs(unique) do
    local b = v:GetBody()
    for i2, v2 in ipairs(v.shapes) do
      if v2 == v then
        table.remove(v.shapes, i2)
        break
      end
    end
    local shape = {}
    table.insert(firstb.shapes, shape)
  end
  ]]
end
function VertexState:UnGroupShapes(shapes)
  
end

function VertexState:SelectAll()
  self:ClearSelection()
  local s = {}
  local s2 = {}
  local world = self.owner:get_world()
  for i, v in pairs(world.bodies) do
    for j, shape in pairs(v.shapes) do
      local t = shape:GetType()
      if t == "circle" then
        table.insert(s, shape.def.localPosition)
        table.insert(s2, shape)
      elseif t == "polygon" or t == "concave" or t == 'chain' or t == 'loop' then
        for k, vx in ipairs(shape.def.vertices) do
          table.insert(s, vx)
          table.insert(s2, shape)
        end
      end
    end
  end
  self:AddToSelection(s, s2)
end


function VertexState:Redraw(dt)
  self:RedrawSelectedVertices()
  
  if self.properties.is_focused ~= true then
    self:UpdateProperties()
  end
end

function VertexState:RedrawSelectedVertices()
  local owner = self.owner
  local scene = owner:get_scene()
  for i, vx in ipairs(self.selection) do
    local body = self.selection2[i].body
    local wvx, wvy = body:GetWorldPoint(vx.x, vx.y)
    local z = scene:GetZoom()
    scene:DrawFRectangle(wvx, wvy, z*4, z*4, DARKORANGE, 1)
  end
  --[[
  for i, shape in ipairs(self.selection2) do
    local t = shape:GetType()
    if t == 'circle' then
    elseif t == 'polygon' or t == 'concave' then
      local vertices = shape.vertices
      local body = shape.body
      for i = 2, #vertices, 1 do
        local vx1 = vertices[i - 1]
        local vx2 = vertices[i]
        local sx, sy = body:GetWorldPoint(vx1.x, vx1.y)
        local ex, ey = body:GetWorldPoint(vx2.x, vx2.y)
        owner:DrawLine(sx, sy, ex, ey, 1, ORANGE, 1)
      end
    end
  end
  ]]
end



function VertexState:GetProperties(shape)
  local def = shape.def
  self.props[1][4].text = def.density
  self.props[2][4].text = def.friction
  self.props[3][4].text = def.restitution
  self.props[4][4].is_checked = def.isSensor
  
  self.props[5][4].text = string.format("0x%x", tonumber(def.categoryBits) or 1)
  self.props[6][4].text = string.format("0x%x", tonumber(def.maskBits) or 65535)
  self.props[7][4].text = def.groupIndex or 0
end

function VertexState:ChangeProperty(l, i, value)
  local shape = self.selection2[1]
  if shape == nil then
    return
  end
  local scene = self.owner:get_scene()
  for j, v in ipairs(self.selection2) do
    if i == 5 or i == 6 then
      value = string.match(value, "0x(.+)") or value
    end
    v.def[l] = value
    scene:RedrawBody(v.body)
  end
end

function VertexState:UpdateProperties()
  self:ClearProperties()
  if #self.selection2 == 0 then
    self:HideProperties()
    return
  end
  self:ShowProperties()
  self:GetProperties(self.selection2[1])
  local props = self.props
  for i = #self.selection, 2, -1 do
    local shape = self.selection2[i]
    local def = shape.def
    if props[1][4].text ~= def.density then
      props[1][4].text = ""
    end
    if props[2][4].text ~= def.friction then
      props[2][4].text = ""
    end
    if props[3][4].text ~= def.restitution then
      props[3][4].text = ""
    end
    if props[4][4].is_checked ~= def.isSensor then
      props[4][4].is_checked = nil
    end

    --local sz = string.match(props[5][4].text, "0x(.+)") or props[5][4].text
    if tonumber(props[5][4].text) ~= tonumber(def.categoryBits) then
      props[5][4].text = ""
    end
    --local sz2 = string.match(props[6][4].text, "0x(.+)") or props[6][4].text
    if tonumber(props[6][4].text) ~= tonumber(def.maskBits) then
      props[6][4].text = ""
    end
    if tonumber(props[7][4].text) ~= tonumber(def.groupIndex) then
      props[7][4].text = ""
    end
  end
end


