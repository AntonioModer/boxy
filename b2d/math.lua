-- Returns the dot product of two points (a,b)
function dot(ax, ay, bx, by)
  return ax*bx + ay*by
end

-- Returns the cross product of two points (a,b)
function cross(ax, ay, bx, by)
  return ay*bx - ax*by
end

-- Returns the signed area of a triangle (abc)
function signed_triangle_area(ax, ay, bx, by, cx, cy)
  return (ax - cx)*(by - cy) - (ay - cy)*(bx - cx)
end


--       (b)
--      /   \
--     /     \  (p)
--    /      (q)
--   /         \
-- (a)---------(c)

-- Projects a point (p) on a triangle (a,b,c)
-- Returns the nearest point (q) from the position (p) on the triangle
function point_on_triangle(px, py, ax, ay, bx, by, cx, cy)
  local abx, aby = bx - ax, by - ay
  local acx, acy = cx - ax, cy - ay
  local apx, apy = px - ax, py - ay
  local d1 = dot(abx, aby, apx, apy)
  local d2 = dot(acx, acy, apx, apy)
  if d1 <= 0 and d2 <= 0 then
    return ax, ay
  end
  local bpx, bpy = px - bx, py - by
  local d3 = dot(abx, aby, bpx, bpy)
  local d4 = dot(acx, acy, bpx, bpy)
  if d3 >= 0 and d4 <= d3 then
    return bx, by
  end
  local vc = d1*d4 - d3*d2
  if vc <= 0 and d1 >= 0 and d3 <= 0 then
    local v = d1 /(d1 - d3)
    local qx = ax + abx*v
    local qy = ay + aby*v
    return qx, qy
  end
  local cpx, cpy = px - cx, py - cy
  local d5 = dot(abx, aby, cpx, cpy)
  local d6 = dot(acx, acy, cpx, cpy)
  if d6 >= 0 and d5 <= d6 then
    return cx, cy
  end
  local vb = d5*d2 - d1*d6
  if vb <= 0 and d2 >= 0 and d6 <= 0 then
    local w = d2 /(d2 - d6)
    local qx = ax + acx*w
    local qy = ay + acy*w
    return qx, qy
  end
  local va = d3*d6 - d5*d4
  if va <= 0 and (d4 - d3) >= 0 and (d5 - d6) >= 0 then
    local w = (d4 - d3)/((d4 - d3) + (d5 - d6))
    local qx = bx + (cx - bx)* w
    local qy = by + (cy - by)* w
    return qx, qy
  end
  local denom = 1 /(va + vb + vc)
  local v = vb*denom
  local w = vc*denom
  local qx = ax + abx*v + acx*w
  local qy = ay + aby*v + acy*w
  return qx, qy
end

-- ----------
-- |       (q)  (p)
-- |   (r)  |
-- |        |
-- ----------

-- Projects a point (p) on a rect (r)
-- Returns the nearest point (q) from the position (p) on the rect
function point_on_rect(px, py, l, t, r, b)
  local qx, qy = px, py
  if qx < l then
    qx = l
  end
  if qx > r then
    qx = r
  end
  if qy < t then
    qy = t
  end
  if qy > b then
    qy = b
  end
  return qx, qy
end

--  (a1)
--    \  (b2)
--     \ / 
--     (q)
--     / \
--   (a2) \
--        (b1)

-- Tests segment (a1-b1) against segment (a2-b2)
-- Returns the point of intersection (q)
function line_vs_line(ax1, ay1, bx1, by1, ax2, ay2, bx2, by2)
  local sa1 = signed_triangle_area(ax1, ay1, bx1, by1, bx2, by2)
  local sa2 = signed_triangle_area(ax1, ay1, bx1, by1, ax2, ay2)

  if sa1*sa2 < 0 then
    local sa3 = signed_triangle_area(ax2, ay2, bx2, by2, ax1, ay1)
    local sa4 = sa3 + sa2 - sa1
    if sa3*sa4 < 0 then
      local t = sa3 /(sa3 - sa4)
      local qx = ax1 + t*(bx1 - ax1)
      local qy = ay1 + t*(by1 - ay1)
      return qx, qy
    end
  end
end



--       (b)
--      /   \
--     /     \  (p)
--    /       \
--   /         \
-- (a)---------(c)

-- Tests point (p) against triangle (a,b,c)
-- Returns true if they intersect
function point_in_triangle(px, py, ax, ay, bx, by, cx, cy)
  local pab = cross(px - ax, py - ay, bx - ax, by - ay)
  local pbc = cross(px - bx, py - by, cx - bx, cy - by)
  if (pab < 0 and pbc >= 0) or (pab >= 0 and pbc < 0) then
    return false
  end
  local pca = cross(px - cx, py - cy, ax - cx, ay - cy)
  if (pab < 0 and pca >= 0) or (pab >= 0 and pca < 0) then
    return false
  end
  return true
end

--     (b).-'''-.
--      /\       \
--     /| \ (s)   |
--    /  \ \     /
--   /    `-\..-'
-- (a)------(c)

-- Tests triangle (a,b,c) against circle (s)
-- Returns true if they intersect
function triangle_vs_circle(ax, ay, bx, by, cx, cy, sx, sy, r)
  local px, py = point_on_triangle(sx, sy, ax, ay, bx, by, cx, cy)

  local vx, vy = px - sx, py - sy
  local d = dot(vx, vy, vx, vy)
  if d > r*r then
    return false
  end
  return true
end

--         -----------
--   .-'''-.         |
--  /      |\  (r)   |
-- |   (c) | |       |
--  \      -/---------
--   `-...-'

-- Tests circle (c) against rect (r)
-- Returns true if they intersect
function circle_vs_rect(cx, cy, radius, l, t, r, b)
  local px, py = point_on_rect(cx, cy, l, t, r, b)
  local vx, vy = px - cx, py - cy
  local d = dot(vx, vy, vx, vy)
  if d > radius*radius then
    return false
  end
  return true
end

--         (d)
--     (b) / \
--      /\/   \
--     / /\    \
--    /(e)-\---(f)
--   /      \
-- (a)------(c)
function triangle_vs_triangle(a1x, a1y, b1x, b1y, c1x, c1y, a2x, a2y, b2x, b2y, c2x, c2y)
  if line_vs_line(a1x, a1y, b1x, b1y, a2x, a2y, b2x, b2y) then
    return true
  end
  if line_vs_line(a1x, a1y, b1x, b1y, b2x, b2y, c2x, c2y) then
    return true
  end
  if line_vs_line(a1x, a1y, b1x, b1y, c2x, c2y, a2x, a2y) then
    return true
  end
  if line_vs_line(b1x, b1y, c1x, c1y, a2x, a2y, b2x, b2y) then
    return true
  end
  if line_vs_line(b1x, b1y, c1x, c1y, b2x, b2y, c2x, c2y) then
    return true
  end
  if line_vs_line(b1x, b1y, c1x, c1y, c2x, c2y, a2x, a2y) then
    return true
  end
  if line_vs_line(c1x, c1y, a1x, a1y, a2x, a2y, b2x, b2y) then
    return true
  end
  if line_vs_line(c1x, c1y, a1x, a1y, b2x, b2y, c2x, c2y) then
    return true
  end
  if line_vs_line(c1x, c1y, a1x, a1y, c2x, c2y, a2x, a2y) then
    return true
  end
  if point_in_triangle(a1x, a1y, a2x, a2y, b2x, b2y, c2x, c2y) then
    return true
  end
  if point_in_triangle(b1x, b1y, a2x, a2y, b2x, b2y, c2x, c2y) then
    return true
  end
  if point_in_triangle(c1x, c1y, a2x, a2y, b2x, b2y, c2x, c2y) then
    return true
  end
  if point_in_triangle(a2x, a2y, a1x, a1y, b1x, b1y, c1x, c1y) then
    return true
  end
  if point_in_triangle(b2x, b2y, a1x, a1y, b1x, b1y, c1x, c1y) then
    return true
  end
  if point_in_triangle(c2x, c2y, a1x, a1y, b1x, b1y, c1x, c1y) then
    return true
  end
  return false
end

--[[
local function triangle_vs_axis(ax, ay, bx, by, cx, cy, sx, sy, ex, ey)
  -- normalize segment
  local xa, ya = ex - sx, ey - sy
  local dsq = xa*xa + ya*ya
  local d = math.sqrt(dsq)
  local nxa, nya = xa/d, ya/d
  -- project segment origin point to axis
  local o = dot(nxa, nya, sx, sy)
  -- project triangle vertices to the axis
  local a = dot(nxa, nya, ax, ay)
  local b = dot(nxa, nya, bx, by)
  local c = dot(nxa, nya, cx, cy)
  -- find projected extents
  local s = math.min(a, b, c)
  local e = math.max(a, b, c)
  -- check for separation
  return e < o or s > o + d
end

function triangle_vs_triangle(ax, ay, bx, by, cx, cy, a2x, a2y, b2x, b2y, c2x, c2y)
  -- project first triangle
  if triangle_vs_axis(ax, ay, bx, by, cx, cy, a2x, a2y, b2x, b2y) then return false end
  if triangle_vs_axis(ax, ay, bx, by, cx, cy, b2x, b2y, c2x, c2y) then return false end
  if triangle_vs_axis(ax, ay, bx, by, cx, cy, c2x, c2y, a2x, a2y) then return false end
  -- project second triangle
  if triangle_vs_axis(a2x, a2y, b2x, b2y, c2x, c2y, ax, ay, bx, by) then return false end
  if triangle_vs_axis(a2x, a2y, b2x, b2y, c2x, c2y, bx, by, cx, cy) then return false end
  if triangle_vs_axis(a2x, a2y, b2x, b2y, c2x, c2y, cx, cy, ax, ay) then return false end
  return true
end
]]

-- only works with CCW polygons
function is_convex(vertices)
  if #vertices < 3 then
    return
  end

	local cx = vertices[2].x
	local cy = vertices[2].y
	local bx = vertices[1].x
	local by = vertices[1].y

	for i = #vertices, 1, -1 do
		local ax = vertices[i].x
		local ay = vertices[i].y
		-- signed area of a triangle
		-- positive if abc is counterclockwise
		-- negative if abc is clockwise
		-- zero if abc is a degenerate
    --if signed_triangle_area(ax, ay, bx, by, cx, cy) > 0 then
    if (ax - cx)*(by - cy) - (ay - cy)*(bx - cx) <= 0 then
      return false
    end
		cx = bx
		cy = by
		bx = ax
		by = ay
	end
  return true
end

-- should work with non-convex polygons (and self-intersecting polygons)
function is_clockwise(vertices)
  if #vertices < 3 then
    return
  end
	local ax = vertices[#vertices].x
	local ay = vertices[#vertices].y
  local sum = 0
	for i = 1, #vertices do
		local bx = vertices[i].x
		local by = vertices[i].y
		-- sum over the edges
    sum = sum + (bx - ax)*(by + ay)

		ax = bx
		ay = by
	end
  -- positive if the curve is clockwise
  -- negative if the curve is counter-clockwise
  return sum > 0
end

function is_self_intersecting(vertices, loop)
  for i = 1, #vertices do
    local a1 = vertices[i]
    local a2 = vertices[i + 1]
    if a2 == nil then
      if loop ~= true then
        break
      end
      a2 = vertices[1]
    end
    for j = i + 1, #vertices do
      local b1 = vertices[j]
      local b2 = vertices[j + 1]
      if b2 == nil then
        if loop ~= true then
          break
        end
        b2 = vertices[1]
      end
      if line_vs_line(a1.x, a1.y, a2.x, a2.y, b1.x, b1.y, b2.x, b2.y) then
        return true
      end
    end
  end
  return false
end