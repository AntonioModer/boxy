Joint = {}
JointMT = { __index = Joint }

function Joint:Create(world, def, mt)
  local self = {}
  setmetatable(self, mt or JointMT)

  self.world = world
  self.def = def
  
  return self
end

function Joint:Destroy()
  self.world = nil
  self.def = nil
end

function Joint:GetType()
  return self.def.type
end

function Joint:GetAnchor1()
  local def = self.def
  local b1 = def.body1
  local a = def.localAnchor1
  if a then
    return b1:GetWorldPoint(a.x, a.y)
  end
end

function Joint:GetAnchor2()
  local def = self.def
  local b2 = def.body2
  local a = def.localAnchor2
  if a then
    return b2:GetWorldPoint(a.x, a.y)
  end
end




RevoluteJoint = {}
RevoluteJointMT = { __index = RevoluteJoint }

setmetatable(RevoluteJoint, { __index = Joint })

function RevoluteJoint:Create(world, def)
  return Joint:Create(world, def, RevoluteJointMT)
end

function RevoluteJoint:GetLimits()
  local def = self.def
  return def.enableLimit, def.lowerAngle, def.upperAngle
end

function RevoluteJoint:EnableMotor(b)
  self.def.enableMotor = b
end

function RevoluteJoint:SetMotorSpeed(s)
  self.def.motorSpeed = s
end

function RevoluteJoint:SetMaxMotorTorque(t)
  self.def.maxMotorTorque = t
end

function RevoluteJoint:IsLimitEnabled()
  return self.def.enableLimit
end

function RevoluteJoint:EnableLimit(b)
  self.def.enableLimit = b
end

function RevoluteJoint:SetLimits(l, u)
  self.def.lowerAngle = l
  self.def.upperAngle = u
end

function RevoluteJoint:GetLowerLimit()
  return self.def.lowerAngle
end

function RevoluteJoint:GetUpperLimit()
  return self.def.upperAngle
end




WeldJoint = {}
WeldJointMT = { __index = WeldJoint }

setmetatable(WeldJoint, { __index = Joint })

function WeldJoint:Create(world, def)
  return Joint:Create(world, def, WeldJointMT)
end



PrismaticJoint = {}
PrismaticJointMT = { __index = PrismaticJoint }

setmetatable(PrismaticJoint, { __index = Joint })

function PrismaticJoint:Create(world, def)
  return Joint:Create(world, def, PrismaticJointMT)
end

function PrismaticJoint:GetWorldAxis()
  local a = self.def.localAxis1
  local b1 = self.def.body1
  return b1:GetWorldVector(a.x, a.y)
end

function PrismaticJoint:EnableLimit(b)
  self.def.enableLimit = b
end

function PrismaticJoint:SetLimits(l, u)
  if l > u then
    l, u = u, l
  end
  local def = self.def
  def.lowerTranslation = l
  def.upperTranslation = u
end

function PrismaticJoint:EnableMotor(b)
  self.def.enableMotor = b
end

function PrismaticJoint:SetMotorSpeed(s)
  self.def.motorSpeed = s
end

function PrismaticJoint:SetMaxMotorForce(f)
  self.def.maxMotorForce = f
end

function PrismaticJoint:IsLimitEnabled()
  return self.def.enableLimit
end

function PrismaticJoint:GetLowerLimit()
  return self.def.lowerTranslation
end

function PrismaticJoint:GetUpperLimit()
  return self.def.upperTranslation
end




DistanceJoint = {}
DistanceJointMT = { __index = DistanceJoint }

setmetatable(DistanceJoint, { __index = Joint })

function DistanceJoint:Create(world, def)
  return Joint:Create(world, def, DistanceJointMT)
end



RopeJoint = {}
RopeJointMT = { __index = RopeJoint }

setmetatable(RopeJoint, { __index = Joint })

function RopeJoint:Create(world, def)
  return Joint:Create(world, def, RopeJointMT)
end



PulleyJoint = {}
PulleyJointMT = { __index = PulleyJoint }

setmetatable(PulleyJoint, { __index = Joint })

function PulleyJoint:Create(world, def)
  return Joint:Create(world, def, PulleyJointMT)
end
function PulleyJoint:GetGroundAnchor1()
  local ga = self.def.groundAnchor1
  return ga.x, ga.y
end
function PulleyJoint:GetGroundAnchor2()
  local ga = self.def.groundAnchor2
  return ga.x, ga.y
end


GearJoint = {}
GearJointMT = { __index = GearJoint }

setmetatable(GearJoint, { __index = Joint })

function GearJoint:Create(world, def)
  return Joint:Create(world, def, GearJointMT)
end

function GearJoint:GetJoint1()
  return self.def.joint1
end

function GearJoint:GetJoint2()
  return self.def.joint2
end
--[[
function GearJoint:GetAnchor1()
  local def = self.def
  local a1x, a1y = def.joint1:GetAnchor1()
  local a2x, a2y = def.joint2:GetAnchor1()
  local b1 = def.body1
  if a1x and a1y and a2x and a2y then
    return (a1x + a2x)/2, (a1y + a2y)/2
  end
end

function GearJoint:GetAnchor2()
  return self:GetAnchor1()
end
]]

function Joint:GetXML()
  return self:GetXML_v2()
end

-- xml v2 -----------------------------------------------

function Joint:GetXML_v2()
  local type = self:GetType()
  local typesz = string.gsub(type, "%a", string.upper, 1)
  local j = xml.new(typesz .. "Joint")
  j.id = self.id
  j.body1 = self.def.body1.id
  j.body2 = self.def.body2.id
  
  local def = self.def
  if def.localAnchor1 then
    local la = j:append("LocalAnchorA")
    la:append('x')[1] = def.localAnchor1.x
    la:append('y')[1] = def.localAnchor1.y
  end
  if def.localAnchor2 then
    local la = j:append("LocalAnchorB")
    la:append('x')[1] = def.localAnchor2.x
    la:append('y')[1] = def.localAnchor2.y
  end
  if def.collideConnected ~= nil then
    j:append("CollideConnected")[1] = def.collideConnected
  end
  return j
end


function RevoluteJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)
  
  local def = self.def
  j:append("ReferenceAngle")[1] = def.referenceAngle

  if def.enableLimit == true or def.lowerAngle ~= 0 or def.upperAngle ~= 0 then
    j:append("EnableLimit")[1] = def.enableLimit
    j:append("LowerAngle")[1] = def.lowerAngle
    j:append("UpperAngle")[1] = def.upperAngle
  end
  if def.enableMotor == true or def.motorSpeed ~= 0 or def.maxMotorTorque ~= 0 then
    j:append("EnableMotor")[1] = def.enableMotor
    j:append("MotorSpeed")[1] = def.motorSpeed
    j:append("MaxMotorTorque")[1] = def.maxMotorTorque
  end
  return j
end

function WeldJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)

  local def = self.def
  j:append("ReferenceAngle")[1] = def.referenceAngle
  j:append("FrequencyHz")[1] = def.frequencyHz
  j:append("DampingRatio")[1] = def.dampingRatio
  
  return j
end

function PrismaticJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)

  local def = self.def
  local la = j:append("LocalAxisA")
  la:append('x')[1] = def.localAxis1.x
  la:append('y')[1] = def.localAxis1.y

  j:append("ReferenceAngle")[1] = def.referenceAngle

  if def.enableLimit == true or def.lowerTranslation ~= 0 or def.upperTranslation ~= 0 then
    j:append("EnableLimit")[1] = def.enableLimit
    j:append("LowerTranslation")[1] = def.lowerTranslation
    j:append("UpperTranslation")[1] = def.upperTranslation
  end
  if def.enableMotor == true or def.motorSpeed ~= 0 or def.maxMotorForce ~= 0 then
    j:append("EnableMotor")[1] = def.enableMotor
    j:append("MotorSpeed")[1] = def.motorSpeed
    j:append("MaxMotorForce")[1] = def.maxMotorForce
  end
  return j
end

function DistanceJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)

  local def = self.def
  j:append("Length")[1] = def.length
  j:append("FrequencyHz")[1] = def.frequencyHz
  j:append("DampingRatio")[1] = def.dampingRatio
  
  return j
end

function RopeJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)

  local def = self.def
  j:append("MaxLength")[1] = def.maxLength
  
  return j
end

function PulleyJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)

  local def = self.def
  j:append("LengthA")[1] = def.length1
  j:append("LengthB")[1] = def.length2
  j:append("MaxLengthA")[1] = def.maxLength1 -- wrong?
  j:append("MaxLengthB")[1] = def.maxLength2 -- wrong?
  local ga1 = j:append("GroundAnchorA")
  ga1:append('x')[1] = def.groundAnchor1.x
  ga1:append('y')[1] = def.groundAnchor1.y
  local ga2 = j:append("GroundAnchorB")
  ga2:append('x')[1] = def.groundAnchor2.x
  ga2:append('y')[1] = def.groundAnchor2.y
  --j:append("GroundAnchor1")[1] = xml.b2Vec2Str_v1(def.groundAnchor1)
  --j:append("GroundAnchor2")[1] = xml.b2Vec2Str_v1(def.groundAnchor2)
  j:append("Ratio")[1] = def.ratio
  
  return j
end

function GearJoint:GetXML_v2()
  local j = Joint.GetXML_v2(self)

  local def = self.def
  j:append("JointA")[1] = def.joint1.id
  j:append("JointB")[1] = def.joint2.id
  j:append("Ratio")[1] = def.ratio
  
  return j
end

-- xml v1 ---------------------------------------------------------------

function Joint:GetXML_v1()
  local j = xml.new("Joint")
  j.id = self.id
  j.type = self:GetType()
  j.body1 = self.def.body1.id
  j.body2 = self.def.body2.id
  return j
end

function RevoluteJoint:GetXML_v1()
  local j = Joint.GetXML_v1(self)
  
  local def = self.def
  j.collideConnected = def.collideConnected
  j:append("LocalAnchor1")[1] = xml.b2Vec2Str_v1(def.localAnchor1)
  j:append("LocalAnchor2")[1] = xml.b2Vec2Str_v1(def.localAnchor2)
  j:append("ReferenceAngle")[1] = def.referenceAngle

  if def.enableLimit == true then
    local limit = j:append("Limit")
    limit.enabled = def.enableLimit
    limit.lowerAngle = def.lowerAngle
    limit.upperAngle = def.upperAngle
  end
  if def.enableMotor == true then
    local motor = j:append("Motor")
    motor.enabled = def.enableMotor
    motor.speed = def.motorSpeed
    motor.maxTorque = def.maxMotorTorque
  end
  return j
end

function PrismaticJoint:GetXML_v1()
  local j = Joint.GetXML_v1(self)

  local def = self.def
  j.collideConnected = def.collideConnected
  j:append("LocalAnchor1")[1] = xml.b2Vec2Str_v1(def.localAnchor1)
  j:append("LocalAnchor2")[1] = xml.b2Vec2Str_v1(def.localAnchor2)
  j:append("LocalAxis1")[1] = xml.b2Vec2Str_v1(def.localAxis1)
  j:append("ReferenceAngle")[1] = def.referenceAngle

  if def.enableLimit == true then
    local limit = j:append("Limit")
    limit.enabled = def.enableLimit
    limit.lowerTranslation = def.lowerTranslation
    limit.upperTranslation = def.upperTranslation
  end
  if def.enableMotor == true then
    local motor = j:append("Motor")
    motor.enabled = def.enableMotor
    motor.speed = def.motorSpeed
    motor.maxForce = def.maxMotorForce
  end
  return j
end

function DistanceJoint:GetXML_v1()
  local j = Joint.GetXML_v1(self)

  local def = self.def
  j.collideConnected = def.collideConnected
  j:append("LocalAnchor1")[1] = xml.b2Vec2Str_v1(def.localAnchor1)
  j:append("LocalAnchor2")[1] = xml.b2Vec2Str_v1(def.localAnchor2)
  j:append("Length")[1] = def.length
  j:append("FrequencyHz")[1] = def.frequencyHz
  j:append("DampingRatio")[1] = def.dampingRatio
  
  return j
end

function PulleyJoint:GetXML_v1()
  local j = Joint.GetXML_v1(self)

  local def = self.def
  j.collideConnected = def.collideConnected
  j:append("LocalAnchor1")[1] = xml.b2Vec2Str_v1(def.localAnchor1)
  j:append("LocalAnchor2")[1] = xml.b2Vec2Str_v1(def.localAnchor2)
  j:append("Length1")[1] = def.length1
  j:append("Length2")[1] = def.length2
  j:append("MaxLength1")[1] = def.maxLength1 -- wrong?
  j:append("MaxLength2")[1] = def.maxLength2 -- wrong?
  j:append("GroundAnchor1")[1] = xml.b2Vec2Str_v1(def.groundAnchor1)
  j:append("GroundAnchor2")[1] = xml.b2Vec2Str_v1(def.groundAnchor2)
  j:append("Ratio")[1] = def.ratio
  
  return j
end

-------------------------------------------------------
function Joint:GetLua_v1()
  local def = self.def
  local t = self:GetType()

  local body1, body2 = def.body1, def.body2
  local b1 = string.format("bodies[%d]", body1.id)
  local b2 = string.format("bodies[%d]", body2.id)

  local a1x, a1y
  if def.localAnchor1 then
    local la = def.localAnchor1
    a1x, a1y = body1:GetWorldPoint(la.x, la.y)
  end
  local a2x, a2y
  if def.localAnchor2 then
    local la = def.localAnchor2
    a2x, a2y = body2:GetWorldPoint(la.x, la.y)
  end
  local cc = "false"
  if def.collideConnected == true then
    cc = "true"
  end
  local id = self.id
  
  
  local out = {}
  if t == "revolute" then
    local sz = "joints[%d] = love.physics.newRevoluteJoint(%s, %s, %s, %s, %s)"
    sz = string.format(sz, id, b1, b2, a1x, a1y, cc)
    table.insert(out, sz)
    
    if def.enableLimit == true then
      local sz = string.format("joints[%d]:enableLimit(true)", id)
      table.insert(out, sz)
    end
    if def.lowerAngle ~= 0 or def.upperAngle ~= 0 then
      local sz = string.format("joints[%d]:setLimits(%s, %s)", id, def.lowerAngle, def.upperAngle)
      table.insert(out, sz)
    end
    if def.enableMotor == true then
      local sz = string.format("joints[%d]:enableMotor(true)", id)
      table.insert(out, sz)
    end
    if def.motorSpeed ~= 0 or def.maxMotorTorque ~= 0 then
      local sz = string.format("joints[%d]:setMotorSpeed(%s)", id, def.motorSpeed)
      local sz2 = string.format("joints[%d]:setMaxMotorTorque(%s)", id, def.maxMotorTorque)
      table.insert(out, sz)
      table.insert(out, sz2)
    end
  elseif t == "distance" then
    local sz = "joints[%d] = love.physics.newDistanceJoint(%s, %s, %s, %s, %s, %s, %s)"
    sz = string.format(sz, id, b1, b2, a1x, a1y, a2x, a2y, cc)
    table.insert(out, sz)
  elseif t == "prismatic" then
    local ax, ay = self:GetWorldAxis()
    local sz = "joints[%d] = love.physics.newPrismaticJoint(%s, %s, %s, %s, %s, %s, %s)"
    sz = string.format(sz, id, b1, b2, a1x, a1y, ax, ay, cc)
    
    if def.enableLimit == true then
      local sz = string.format("joints[%d]:enableLimit(true)", id)
      table.insert(out, sz)
    end
    if def.lowerAngle ~= 0 or def.upperAngle ~= 0 then
      local sz = string.format("joints[%d]:setLimits(%s, %s)", id, def.lowerTranslation, def.upperTranslation)
      table.insert(out, sz)
    end
    if def.enableMotor == true then
      local sz = string.format("joints[%d]:enableMotor(true)", id)
      table.insert(out, sz)
    end
    if def.motorSpeed ~= 0 or def.maxMotorTorque ~= 0 then
      local sz = string.format("joints[%d]:setMotorSpeed(%s)", id, def.motorSpeed)
      local sz2 = string.format("joints[%d]:setMaxMotorForce(%s)", id, def.maxMotorForce)
      table.insert(out, sz)
      table.insert(out, sz2)
    end    
  end
  
  return table.concat(out, "\n")
end

