-- converts string to number
xml.tonumber = function(sz)
  return tonumber(sz)
end

-- converts string to boolean
xml.toboolean = function(b)
  if b == "true" then
    return true
  elseif b == "false" then
    return false
  end
end


-- xml v1 ---------------------------------------------------------------------------------

-- converts string to x, y
xml.toPosition_v1 = function(v)
  -- longer but probably faster than pattern matching
  local i = string.find(v, ",", 1, true)
  if i == nil then 
    return
  end
  local sx = string.sub(v, 1, i - 1)
  local sy = string.sub(v, i + 2, -1)
  local x = xml.tonumber(sx)
  local y = xml.tonumber(sy)
  if x == nil or y == nil then
    return
  end
  return x, y
end

xml.PositionStr_v1 = function(x, y)
  return string.format("%g, %g", x, y)
end

xml.tob2Vec2_v1 = function(v)
  local x, y = xml.toPosition_v1(v)
  return { x = x, y = y }
end

xml.b2Vec2Str_v1 = function(v)
  return xml.PositionStr_v1(v.x, v.y)
end

xml.toPath_v1 = function(p)
  local path = {}
  for i = 1, #p, 1 do
    local vertex = p[i]
    assert(xml.tag(vertex) == "Vertex", "Unknown tag in path")
    local v = xml.tob2Vec2_v1(vertex[1])
    assert(v, "Invalid vertex in path")
    table.insert(path, v)
  end
  return path
end

xml.PathStr_v1 = function(path)
  local p = xml.new("Path")
  for i, v in ipairs(path) do
    p:append("Vertex")[1] = xml.b2Vec2Str_v1(v)			
  end
  return p
end

-- xml v2 ---------------------------------------------------------------------------------

xml.cast = {}

-- converts xml tag to number
xml.cast.number = function(n)
  return tonumber(n[1])
end

-- converts xml tag to boolean
xml.cast.boolean = function(b)
  if b[1] == "true" then
    return true
  elseif b[1] == "false" then
    return false
  end
end

-- converts xml tag to b2Vec2
xml.cast.b2Vec2 = function(v)
  local x, y
  for _, value in pairs(v) do
    local t = xml.tag(value)
    if t == "x" then
      x = xml.cast.number(value)
    elseif t == "y" then
      y = xml.cast.number(value)
    end
  end
  if x == nil or y == nil then
    return
  end
  return b2.Vec2(x, y)
end

-- converts xml tag to string
xml.cast.string = function(v)
  return v[1]
end

-- converts xml tag to a table of b2Vec2 vertices
xml.cast.path = function(p)
  local path = {}
  for i = 1, #p, 1 do
    local v = p[i]
    local tag = xml.tag(v)
    assert(tag == "Vertex", "Unknown tag in path")
    local vertex = xml.cast.b2Vec2(v)
    assert(vertex, "Invalid vertex in path")
    table.insert(path, vertex)
  end
  return path
end

-- loads xml into destination using a skeleton
xml.loadSkeleton = function(skeleton, src, dest)
  for i, v in pairs(skeleton) do
    local sz = string.gsub(i, "%a", string.upper, 1)
    local x = xml.find(src, sz)
    local value
    if x then
      -- convert value from the source
      assert(xml.cast[v[1]] ~= nil, "Unsupported conversion")
      value = xml.cast[v[1]](x)
      assert(value ~= nil, "Conversion failed:" .. i)
    else
      -- load default value from the skeleton
      value = v[2]
    end
    dest[i] = value
  end
end




-- Returns b2Vec2 path from list of coords
function NewPath(...)
  local path = {}
  for i = 1, select('#', ...), 2 do
    local x = select(i, ...)
    local y = select(i + 1, ...)
    table.insert(path, { x = x, y = y })
  end
  return path
end

-- Returns b2Vec2 path from width and height
function NewBoxPath(w, h, cx, cy, a)
  cx, cy = cx or 0, cy or 0
  a = a or 0
	local cosa = math.cos(a)
	local sina = math.sin(a)

  local path = {}
  path[1] = { x = cx + cosa*-w - sina*-h, y = cy + cosa*-h + sina*-w }
  path[2] = { x = cx + cosa*w - sina*-h, y = cy + cosa*-h + sina*w }
  path[3] = { x = cx + cosa*w - sina*h, y = cy + cosa*h + sina*w }
  path[4] = { x = cx + cosa*-w - sina*h, y = cy + cosa*h + sina*-w }
  return path
end