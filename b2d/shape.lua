Shape = {}
ShapeMT = { __index = Shape }

function Shape:Create(body, def, mt)
  local self = {}
  setmetatable(self, mt or ShapeMT)

  self.body = body
  self.def = def
  
  return self
end

function Shape:Destroy()
  self.body = nil
  self.def = nil
end

function Shape:GetType()
  return self.def.type
end

function Shape:GetBody()
  return self.body
end

function Shape:Sync()
end


CircleShape = {}
CircleShapeMT = { __index = CircleShape }

setmetatable(CircleShape, { __index = Shape })

function CircleShape:Create(body, def)
  return Shape:Create(body, def, CircleShapeMT)
end

function CircleShape:IsValid()
  local def = self.def
  if def.radius <= b2.linearSlop then
    return false
  end
  return true
end

function CircleShape:GetMass()
  local radius = self.def.radius
  local density = self.def.density
  return radius*radius*math.pi*density
end

function CircleShape:GetAABB()
  local lp = self.def.localPosition
  local radius = self.def.radius
  local l, r = lp.x - radius, lp.x + radius
  local t, b = lp.y - radius, lp.y + radius
  return l, t, r, b
end

function CircleShape:GetWorldAABB()
  local lp = self.def.localPosition
  local radius = self.def.radius
  local x, y = self.body:GetWorldPoint(lp.x, lp.y)
  local l, r = x - radius, x + radius
  local t, b = y - radius, y + radius
  return l, t, r, b
end

function CircleShape:TestWorldAABB(l, t, r, b)
  local lp = self.def.localPosition
  local cx, cy = self.body:GetWorldPoint(lp.x, lp.y)
  local radius = self.def.radius
  return circle_vs_rect(cx, cy, radius, l, t, r, b)
end

function CircleShape:TestLocalPoint(ptx, pty)
  local lp = self.def.localPosition
  local radius = self.def.radius
  local dx, dy = lp.x - ptx, lp.y - pty
  local d = math.sqrt(dx*dx + dy*dy)
  return d <= radius
end

-- projects point on the outline of a shape, accepts local point
function CircleShape:VertexOnShape(ptx, pty)
  local lp = self.def.localPosition
  local radius = self.def.radius
  local dx, dy = ptx - lp.x, pty - lp.y
  local d = math.sqrt(dx*dx + dy*dy)
  if d == 0 then
    return lp.x, lp.y + radius, 0
  end
  local nx, ny = dx/d, dy/d
  local px, py = nx*radius + lp.x, ny*radius + lp.y
  return px, py, math.abs(d - radius)
end

PolygonShape = {}
PolygonShapeMT = { __index = PolygonShape }

setmetatable(PolygonShape, { __index = Shape })

function PolygonShape:Create(body, def)
  return Shape:Create(body, def, PolygonShapeMT)
end

function PolygonShape:FindVertex(vertex)
  for i, v in ipairs(self.def.vertices) do
    if vertex == v then
      return i
    end
  end
end

function PolygonShape:IsValid()
  local t = self.def.type
  if t == "polygon" then
    return is_convex(self.def.vertices)
  else
    return not is_self_intersecting(self.def.vertices, t ~= 'chain')
  end
end

-- Inserts a vertex at a given index
-- Returns the new vertex
function PolygonShape:AddVertex(lx, ly, i)
  local v = { x = lx, y = ly }
  table.insert(self.def.vertices, i, v)
  self:Sync()
  return v
end

-- Removes vertex at a given index
-- Returns the new vertex count
function PolygonShape:RemoveVertex(i)
  table.remove(self.def.vertices, i)
  self:Sync()
  return #self.def.vertices
end

function PolygonShape:GetAABB()
  local l, t, r, b
  for i, v in ipairs(self.def.vertices) do
    l = math.min(v.x, l or v.x)
    r = math.max(v.x, r or v.x)
    t = math.min(v.y, t or v.y)
    b = math.max(v.y, b or v.y)
  end
  return l, t, r, b
end

function PolygonShape:GetWorldAABB()
  local body = self.body
  local l, t, r, b
  for i, v in ipairs(self.def.vertices) do
    local x, y = body:GetWorldPoint(v.x, v.y)
    l = math.min(x, l or x)
    r = math.max(x, r or x)
    t = math.min(y, t or y)
    b = math.max(y, b or y)
  end
  return l, t, r, b
end

function PolygonShape:Sync()
  self.triangles = nil
  self.trianglesv = nil

  local t = self:GetType()
  local vertices = self.def.vertices

  if t == "concave" or t == "polygon" then
    self:SetOrientation(false)
    if #vertices <= b2.maxPolygonVertices then
      if is_convex(vertices) == true then
        t = "polygon"
      else
        t = "concave"
      end
    else
      t = "concave"
    end
  end
  self.def.type = t
  
  if t == "polygon" then
    -- ensure edges are non-zero
    local ps = {}
    for _, v in ipairs(vertices) do
      local unique = true
      for _, j in ipairs(ps) do
        local dx = v.x - j.x
        local dy = v.y - j.y
        local dsq = dx*dx + dy*dy
        if dsq < 0.5*b2.linearSlop then
          unique = false
          break
        end
      end
      if unique == true then
        table.insert(ps, v)
      end
    end
    self.valid = (#ps == #vertices)
    --self.valid = b2.ValidatePolygon(vertices)
  elseif t == "concave" and #vertices >= 3 then
    local out = {}
    local ret = b2.Fill(vertices, out)
    self.triangles = out

    -- ensure edges are non-zero
    local valid = {}
    for z = 1, #out, 3 do
      local ps = {}
      for i = 0, 2 do
        local v = out[i + z]
        local unique = true
        for _, j in ipairs(ps) do
          local dx = v.x - j.x
          local dy = v.y - j.y
          local dsq = dx*dx + dy*dy
          if dsq < 0.5*b2.linearSlop then
            unique = false
            break
          end
        end
        if unique == true then
          table.insert(ps, v)
        end
      end
      valid[(z - 1)/3] = (#ps == 3)
    end

    --[[
    -- ensure consecutive edges are not parallel
    for z = 1, #out, 3 do
      local a = out[z]
      local b = out[z + 1]
      local c = out[z + 2]
      local s1x, s1y = a.x - b.x, a.y - b.y
      local s1d = math.sqrt(s1x*s1x + s1y*s1y)
      local s2x, s2y = b.x - c.x, b.y - c.y
      local s2d = math.sqrt(s2x*s2x + s2y*s2y)
      local n1x, n1y = s1x/s1d, s1y/s1d
      local n2x, n2y = s2x/s2d, s2y/s2d
      local cross = n1x*n2y - n1y*n2x
      cross = math.max(cross, -1.0)
      cross = math.min(cross, 1.0)
      local angle = math.asin(cross)
      if angle < b2.angularSlop then
        valid[(z - 1)/3] = false
        assert(false)
      end
    end
    ]]
    --[[
    local valid = {}
    for z = 1, #out, 3 do
      local a = out[z]
      local b = out[z + 1]
      local c = out[z + 2]
      assert(a and b and c)
      local t = { a, b, c }
      if b2.ValidatePolygon(t, 3) == false then
        valid[(z - 1)/3 + 1] = false
      end
    end
    ]]
    self.trianglesv = valid
  end
end

function PolygonShape:GetMass()
  local out = self.triangles
  if out == nil and #self.def.vertices >= 3 then
    out = {}
    b2.Fill(self.def.vertices, out)
  end
  local area = 0
  for i = 1, #out, 3 do
    local ax, ay = out[i].x, out[i].y
    local bx, by = out[i + 1].x, out[i + 1].y
    local cx, cy = out[i + 2].x, out[i + 2].y
    local sta = signed_triangle_area(ax, ay, bx, by, cx, cy)
    area = area + math.abs(sta)
  end
  return area*self.def.density
end

function PolygonShape:TestWorldAABB(l, t, r, b)
  local vertices = self.def.vertices
  if #vertices < 3 then
    return false
  end
  local out = self.triangles
  if out == nil then
    out = {}
    b2.Fill(self.def.vertices, out)
  end
  local body = self.body
  for i = 1, #out, 3 do
    local ax, ay = body:GetWorldPoint(out[i].x, out[i].y)
    local bx, by = body:GetWorldPoint(out[i + 1].x, out[i + 1].y)
    local cx, cy = body:GetWorldPoint(out[i + 2].x, out[i + 2].y)
    if triangle_vs_triangle(l, t, r, t, l, b, ax, ay, bx, by, cx, cy) then
      return true
    end
    if triangle_vs_triangle(r, t, r, b, l, b, ax, ay, bx, by, cx, cy) then
      return true
    end
  end
  return false
end

function PolygonShape:TestLocalPoint(px, py)
  local out = self.triangles
  if out == nil then
    out = {}
    b2.Fill(self.def.vertices, out)
  end
  for i = 1, #out, 3 do
    local ax, ay = out[i].x, out[i].y
    local bx, by = out[i + 1].x, out[i + 1].y
    local cx, cy = out[i + 2].x, out[i + 2].y
    if point_in_triangle(px, py, ax, ay, bx, by, cx, cy) then
      return true
    end
  end
  return false
end

function PolygonShape:SetOrientation(cw)
  local verts = self.def.vertices
  if is_clockwise(verts) == not cw then
    -- make sure the vertices are CCW
    local iverts = {}
    for i = #verts, 1, -1 do
      table.insert(iverts, verts[i])
    end
    verts = iverts
    self.def.vertices = iverts
  end
  --self:Sync()
end

function PolygonShape:VertexOnSegment(ptx, pty, ax, ay, bx, by)
  local nabx, naby = bx - ax, by - ay
  local nptx, npty = ptx - ax, pty - ay
  local d1 = nptx*nabx + npty*naby
  local d2 = nabx*nabx + naby*naby
  assert(d2 ~= 0)
  local u = d1/d2
  if u < 0.0 then
    u = 0
  else
    if u > 1.0 then
      u = 1.0
    end
  end
  return ax + u*nabx, ay + u*naby
end

-- projects point on the outline of a shape, accepts local point
function PolygonShape:VertexOnShape(ptx, pty)
  local path = self.def.vertices
  local resx, resy = nil, nil
  local resi = nil
  local resd = nil
  for i = 1, #path, 1 do
    local a = path[i]
    local b
    if i == #path then
      b = path[1]
    else
      b = path[i + 1]
    end
    if a.x ~= b.x or a.y ~= b.y then
      local rx, ry = self:VertexOnSegment(ptx, pty, a.x, a.y, b.x, b.y)
      local dx, dy = rx - ptx, ry - pty
      local d = math.sqrt(dx*dx + dy*dy)
      if resx == nil or resd > d then
        resx, resy = rx, ry
        resd = d
        resi = i
      end
    end
  end
  -- returns point, distance and index
  return resx, resy, resd, resi
end





-- xml v2 -----------------------------------------------------------------------

function Shape:GetXML_v2()
  local t = self:GetType()
  
  local typesz = string.gsub(t, "%a", string.upper, 1)
  if t == "loop" then
    typesz = "chain"
  end
  local shape = xml.new(typesz .. "Shape")

  local def = self.def
  if def.density ~= 0 then
    shape:append('Density')[1] = def.density
  end
  if def.friction ~= 0 then
    shape:append('Friction')[1] = def.friction
  end
  if def.restitution ~= 0 then
    shape:append('Restitution')[1] = def.restitution
  end
  if def.isSensor ~= false then
    shape:append('IsSensor')[1] = def.isSensor
  end
  if t == "loop" or t == "chain" then
    shape.loop = (t == "loop")
  end
  
  local f = def
  if f then
    if (f.maskBits ~= nil and f.maskBits ~= 65535) or 
      (f.categoryBits ~= nil and f.categoryBits ~= 1) or
      (f.groupIndex ~= nil and f.groupIndex ~= 0) then
      
      local filter = shape:append("Filter")
      filter.GroupIndex = f.groupIndex
      filter.CategoryBits = f.categoryBits
      filter.MaskBits = f.maskBits
    end
  end

  return shape
end


function CircleShape:GetXML_v2()
  local shape = Shape.GetXML_v2(self)
  local def = self.def
  shape:append("Radius")[1] = def.radius
  if def.localPosition.x ~= 0 or def.localPosition.y ~= 0 then
    local lp = shape:append("LocalPosition")
    lp:append('x')[1] = def.localPosition.x
    lp:append('y')[1] = def.localPosition.y
  end
  return shape
end


function PolygonShape:GetXML_v2()
  local shape = Shape.GetXML_v2(self)
  if #self.def.vertices <= 2 then
    return
  end
  local p = shape:append("Path")
  for i, vertex in ipairs(self.def.vertices) do
    local v = p:append("Vertex")
    v:append('x')[1] = vertex.x
    v:append('y')[1] = vertex.y
  end

  return shape
end

-- xml v1 -----------------------------------------------------------------------

function Shape:GetXML_v1()
  local shape = xml.new("Shape")
  local type = self:GetType()
  shape.type = type

  local def = self.def
  shape.density = def.density
  shape.friction = def.friction
  shape.restitution = def.restitution
  shape.isSensor = def.isSensor
  
  local f = def.filter
  if f then
    if (f.maskBits ~= nil and f.maskBits ~= 65535) or 
      (f.categoryBits ~= nil and f.categoryBits ~= 1) or
      (f.groupIndex ~= nil and f.groupIndex ~= 0) then
      
      local filter = shape:append("Filter")
      filter.GroupIndex = f.groupIndex
      filter.CategoryBits = f.categoryBits
      filter.MaskBits = f.maskBits
    end
  end

  return shape
end


function CircleShape:GetXML_v1()
  local shape = Shape.GetXML_v1(self)
  local def = self.def
  shape:append("Radius")[1] = def.radius
  local lp = def.localPosition
  shape:append("LocalPosition")[1] = xml.PositionStr_v1(lp.x, lp.y)
  return shape
end


function PolygonShape:GetXML_v1()
  local shape = Shape.GetXML_v1(self)
  shape.path = xml.PathStr_v1(self.def.vertices)
  return shape
end

------------------------------------------------------------


function Shape:GetLua_v1()
  local t = self:GetType()
  local def = self.def

  if t == "concave" or t == "polygon" then
    local verts = self.def.vertices
    self:SetOrientation(false)
    if #verts <= b2.maxPolygonVertices then
      if is_convex(verts) == true then
        t = "polygon"
      else
        t = "concave"
      end
    else
      t = "concave"
    end
  end
  
  local body = self:GetBody()
  local out = {}
  
  if t == "circle" or t == "polygon" then
    local sz = string.format("    {\n      type = '%s'", t)
    table.insert(out, sz)

    if def.density ~= 0 then
      sz2 = string.format("density = %s", def.density)
      table.insert(out, sz2)
    end
    if def.friction ~= 0 then
      local sz3 = string.format("friction = %s", def.friction)
      table.insert(out, sz3)
    end
    if def.restitution ~= 0 then
      local sz3 = string.format("restitution = %s", def.restitution)
      table.insert(out, sz3)
    end
    if t == "circle" then
      local lp = def.localPosition
      local sz = string.format("localX = %s", lp.x)
      table.insert(out, sz)
      local sz = string.format("localY = %s", lp.y)
      table.insert(out, sz)
      local sz = string.format("radius = %s", def.radius)
      table.insert(out, sz)
    else
      local v = def.vertices
      local sz = "vertices = { "
      sz = sz .. string.format("%s, %s", v[1].x, v[1].y)
      for i = 2, #v do
        local vx = v[i]
        sz = sz .. string.format(", %s, %s", vx.x, vx.y)
      end
      sz = sz .. " }"
      table.insert(out, sz)
    end
    
    table.insert(out, "    }")
  elseif t == "concave" then
    local o = {}
    local ret = b2.Fill(def.vertices, o)
    for i = 1, #o, 3 do
      local ax, ay = o[i].x, o[i].y
      local bx, by = o[i + 1].x, o[i + 1].y
      local cx, cy = o[i + 2].x, o[i + 2].y
      local sz = "local shape = love.physics.newPolygonShape(%s, %s, %s, %s, %s, %s)"
      sz = string.format(sz, ax, ay, bx, by, cx, cy)
      table.insert(out, sz)
      
      local sz2 = "local fixture = love.physics.newFixture(bodies[%s], shape, %s)"
      sz2 = string.format(sz2, body.id, def.density)
      table.insert(out, sz2)
      if def.friction ~= 0 then
        local sz3 = string.format("fixture:setFriction(%s)", def.friction)
        table.insert(out, sz3)
      end
      if def.restitution ~= 0 then
        local sz3 = string.format("fixture:setRestitution(%s)", def.restitution)
        table.insert(out, sz3)
      end
    end
  end

  return table.concat(out, ",\n     ")
end