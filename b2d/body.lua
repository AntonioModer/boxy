Body = {}
BodyMT = { __index = Body }

function Body:Create(world, def)
  local self = {}
  setmetatable(self, BodyMT)
  
  def.scale = def.scale or 1

  self.world = world
  self.def = def
  self.shapes = {}
  def.linearVelocity = { x = 0, y = 0 }
  def.angularVelocity = 0
  
  return self
end

function Body:Destroy()
  self.world = nil
  self.def = nil
  self.shapes = nil
end

function Body:GetPosition()
  local p = self.def.position
  return p.x, p.y
end

function Body:SetPosition(x, y)
  local p = self.def.position
  p.x, p.y = x, y
end

function Body:ChangePosition(dx, dy)
  local p = self.def.position
  p.x, p.y = p.x + dx, p.y + dy
end

function Body:GetLinearVelocity()
  local lv = self.def.linearVelocity
  return lv.x, lv.y
end

function Body:SetLinearVelocity(x, y)
  local lv = self.def.linearVelocity
  lv.x = x
  lv.y = y
end

function Body:GetAngularVelocity()
  return self.def.angularVelocity or 0
end

function Body:SetAngularVelocity(av)
  self.def.angularVelocity = av
end

function Body:GetScale()
  return self.def.scale
end

function Body:GetMass()
  local m = 0
  for i, v in ipairs(self.shapes) do
    m = m + v:GetMass()
  end
  return m
end

function Body:SetScale(s)
  if s == 0 then
    return
  end
  local os = self:GetScale()
  for i, v in ipairs(self.shapes) do
    local t = v:GetType()
    if t == 'circle' then
      v.def.radius = math.abs(v.def.radius/os*s)
      local lp = v.def.localPosition
      lp.x = lp.x/os*s
      lp.y = lp.y/os*s
    elseif t == 'polygon' or t == 'concave' or t == 'chain' then
      for i, v in ipairs(v.def.vertices) do
        v.x = v.x/os*s
        v.y = v.y/os*s
      end
    end
    v:Sync()
  end
  self.def.scale = s
end

function Body:GetAngle()
  return self.def.angle
end

function Body:SetAngle(a)
  self.def.angle = a
end

function Body:ChangeAngle(a)
  self.def.angle = self.def.angle + a
end

function Body:CreateShape(def)
  local s
  local t = def.type
  if t == "circle" then
    s = CircleShape:Create(self, def)
  elseif t == "polygon" or t == "concave" or t == "chain" or t =="loop" then
    s = PolygonShape:Create(self, def)
  else
    assert(false)
  end
  s:Sync()
  table.insert(self.shapes, s)
  return s
end

function Body:CreateShapeEx(t, p1, p2, p3, p4, p5, p6, p7)
  local def = {}
  def.type = t
  def.density = p2 or 0
  def.friction = p3 or 0
  def.restitution = p4 or 0
  def.isSensor = p5 or false
  if t == "polygon" or t == "concave" or t == "chain" or t =="loop" then
    def.vertices = p1
    if t == "loop" then
      def.loop = true
    end
  elseif t == "circle" then
    def.radius = p1
    def.localPosition = { x = p6 or 0, y = p7 or 0 }
  else
    assert(false)
  end
  def.categoryBits = 1
  def.maskBits = 65535
  def.groupIndex = 0
  return self:CreateShape(def)
end

function Body:DestroyShape(s)
  local i = table.find(self.shapes, s)
  if i then
    s:Destroy()
    table.remove(self.shapes, i)
  end
end

-- Returns AABB from the body's shapes in local coords
function Body:GetAABB()
  local l, t, r, b
  for i, v in ipairs(self.shapes) do
    local l2, t2, r2, b2 = v:GetAABB()
    l = math.min(l2, l or l2)
    r = math.max(r2, r or r2)
    t = math.min(t2, t or t2)
    b = math.max(b2, b or b2)
  end
  return l, t, r, b
end

-- Returns AABB from the body's shapes in world coords
function Body:GetWorldAABB()
  local l, t, r, b
  for i, v in ipairs(self.shapes) do
    local l2, t2, r2, b2 = v:GetWorldAABB()
    l = math.min(l2, l or l2)
    r = math.max(r2, r or r2)
    t = math.min(t2, t or t2)
    b = math.max(b2, b or b2)
  end
  return l, t, r, b
end

function Body:GetLocalCenter()
  local l, t, r, b = self:GetAABB()
  if l == nil or t == nil then
    return 0, 0
  end
  return (l + r)/2, (t + b)/2
end

-- Returns AABB from the body's shapes in local coords
function Body:GetWorldCenter()
  local lx, ly = self:GetLocalCenter()
  return self:GetWorldPoint(lx, ly)
end

function Body:QueryAABB(l, t, r, b, q)
  -- quick aabb elimination
  local l2, t2, r2, b2 = self:GetWorldAABB()
  if l2 == nil or t2 == nil or r2 == nil or b2 == nil then
    return
  end
  if l > r2 or r < l2 then
    return
  end
  if t > b2 or b < t2 then
    return
  end

  -- test each shape
  for i, v in ipairs(self.shapes) do
    if v:TestWorldAABB(l, t, r, b) then
      table.insert(q, v)
    end
  end
end

function Body:TestPoint(x, y)
  local lx, ly = self:GetLocalPoint(x, y)
  -- test each shape
  for i, v in ipairs(self.shapes) do
    if v:TestLocalPoint(lx, ly) then
      return true
    end
  end
  return false
end

function Body:NearestVertex(x, y, ignore_circles)
  local lx, ly = self:GetLocalPoint(x, y)
  local resx, resy, resd, ress, resi
  -- test each shape
  for _, shape in ipairs(self.shapes) do
    if ignore_circles ~= true or shape:GetType() ~= "circle" then
      local x, y, d, i = shape:VertexOnShape(lx, ly)
      if resd == nil or d < resd then
        resx, resy = x, y
        resd = d
        ress, resi = shape, i
      end
    end
  end
  if resx and resy then
    resx, resy = self:GetWorldPoint(resx, resy)
  end
  return resx, resy, resd, ress, resi
end

function Body:GetLocalPoint(wx, wy)
  local p = self.def.position
  local lx, ly = wx - p.x, wy - p.y
  return self:GetLocalVector(lx, ly)
end

function Body:GetWorldPoint(lx, ly)
  local r = self.def.angle
	local tx, ty = self:GetLocalVector(lx, ly, r)
  local p = self.def.position
  return tx + p.x, ty + p.y
end

function Body:GetLocalVector(wvx, wvy, a)
  local r = a or -self.def.angle
  local cd = math.cos(r)
	local sd = math.sin(r)
	local tx = cd*wvx - sd*wvy
	local ty = cd*wvy + sd*wvx
  return tx, ty
end

function Body:GetWorldVector(lx, ly)
  local r = self.def.angle
  return self:GetLocalVector(lx, ly, r)
end

function Body:GetDistanceTo(x, y)
  if self:TestPoint(x, y) then
    return 0
  end
  local lx, ly = self:GetLocalPoint(x, y)
  local dist
  for i, s in ipairs(self.shapes) do
    local _, _, d, _ = s:VertexOnShape(lx, ly)
    if dist == nil or d < dist then
      dist = d
    end
  end
  if dist == nil then
    dist = math.sqrt(lx*lx + ly*ly)
  end
  return dist
end



-- xml v2 -----------------------------------------------------------

-- skeleton includes type and default value
local fixtureSkeleton =
{
  density = { "number", 0 },
  friction = { "number", 0 },
  restitution = { "number", 0 },
  isSensor = { "boolean", false }
}

local circleDefSkeleton =
{
  radius = { "number", 0 },
  localPosition = { "b2Vec2", b2.Vec2() }
}

function Body:CreateShapeXML_v2(s)
  local def = {}

  local t = xml.tag(s)
  if t == "CircleShape" then
    def.type = "circle"
    xml.loadSkeleton(circleDefSkeleton, s, def)
  elseif t == "PolygonShape" or t == "ConcaveShape" or t == "ChainShape" then
    def.type = "polygon"
    if t == "ConcaveShape" then
      def.type = "concave"
    elseif t == "ChainShape" then
      def.type = "chain"
      if s.loop == "true" then
        def.type = "loop"
      end
    end
    local path = xml.find(s, "Path")
    local vertices = xml.cast.path(path)
    assert(path, "Polygon without path")
    def.vertexCount = #vertices
    def.vertices = vertices
  else
    assert(false, "Unknown shape type")
  end
  local f = xml.find(s, "Filter")
  if f then
    def.groupIndex = f.GroupIndex
    def.categoryBits = f.CategoryBits
    def.maskBits = f.MaskBits
  end
  def.groupIndex = def.groupIndex or 0
  def.categoryBits = def.categoryBits or 1
  def.maskBits = def.maskBits or 65535

  -- assign values
  xml.loadSkeleton(fixtureSkeleton, s, def)

  return self:CreateShape(def)
end

function Body:GetXML_v2()
  local body = xml.new("Body")

  body.id = self.id
  local def = self.def
  body:append('Type')[1] = def.type
  local p = body:append("Position")
	p:append('x')[1] = def.position.x
	p:append('y')[1] = def.position.y
  if def.angle ~= 0 then
    body:append('Angle')[1] = def.angle
  end
  if def.linearDamping ~= 0 then
    body:append('LinearDamping')[1] = def.linearDamping
  end
  if def.angularDamping ~= 0 then
    body:append('AngularDamping')[1] = def.angularDamping
  end
  if def.fixedRotation ~= false then
    body:append('FixedRotation')[1] = def.fixedRotation
  end
  if def.isBullet ~= false then
    body:append('Bullet')[1] = def.isBullet
  end
  if def.allowSleep ~= false then
    body:append('AllowSleep')[1] = def.allowSleep
  end
  if def.isSleeping ~= false and def.isSleeping ~= nil then
    body:append('Awake')[1] = not def.isSleeping
  end
  if def.gravityScale ~= 1 then
    body:append('GravityScale')[1] = def.gravityScale
  end
  if def.active ~= true then
    body:append('Active')[1] = def.active
  end
  
  if def.scale ~= 1 then
    body:append("Scale")[1] = def.scale
  end
  local lvx, lvy = self:GetLinearVelocity()
  if lvx ~= 0 or lvy ~= 0 then
    local lv = body:append("LinearVelocity")
    lv:append('x')[1] = lvx
    lv:append('y')[1] = lvy
  end
  local av = self:GetAngularVelocity()
  if av ~= 0 then
    body:append("AngularVelocity")[1] = av
  end

  for i, v in ipairs(self.shapes) do
    local shape = v:GetXML_v2()
    body:append(shape)
  end
 
  return body
end




-- xml v1 -----------------------------------------------------------

function Body:CreateShapeXML_v1(s)
  local def = {}

  local t = s.type
  if t == "circle" then
    local r = xml.find(s, "Radius")
    local lp = xml.find(s, "LocalPosition")
    -- assign values
    def.radius = xml.tonumber(r[1])
    -- default values
    def.localPosition = { x = 0, y = 0 }
    -- optional
    if lp then
      local lpx, lpy = xml.toPosition_v1(lp[1])
      def.localPosition.x = lpx
      def.localPosition.y = lpy
    end
  elseif t == "polygon" or t == "concave" then
    local path = xml.find(s, "Path")
    assert(path, "Polygon without path")
    def.vertices = xml.toPath_v1(path)
    assert(#def.vertices > 1, "Polygon vertices less than two")
    --assert(#vertices <= b2_maxPolygonVertices, "Polygon exceeds b2_maxPolygonVertices")
    -- assign values
    def.vertexCount = #def.vertices
  else
    assert(false, "Unknown shape type")
  end

  -- assign values
  def.type = t
  def.density = xml.tonumber(s.density)
  def.friction = xml.tonumber(s.friction)
  def.restitution = xml.tonumber(s.restitution)
  def.isSensor = xml.toboolean(s.isSensor)

  return self:CreateShape(def)
end

function Body:GetXML_v1()
  local body = xml.new("Body")

  body.id = self.id
  local def = self.def
  local p = def.position
  body.type = def.type
  body.position = xml.PositionStr_v1(p.x, p.y)
  body.angle = def.angle
  body.linearDamping = def.linearDamping
  body.angularDamping = def.angularDamping
  body.fixedRotation = def.fixedRotation
  body.isBullet = def.isBullet
  body.allowSleep = def.allowSleep
  body.isSleeping = def.isSleeping

  if def.scale ~= 1 then
    body:append("Scale")[1] = def.scale
  end
  local lvx, lvy = self:GetLinearVelocity()
  if lvx ~= 0 or lvy ~= 0 then
    body:append("LinearVelocity")[1] = xml.PositionStr_v1(lvx, lvy)
  end
  local av = self:GetAngularVelocity()
  if av ~= 0 then
    body:append("AngularVelocity")[1] = av
  end

  for i, v in ipairs(self.shapes) do
    local shape = v:GetXML_v1()
    body:append(shape)
  end
  return body
end

----------------------------------------------------


function Body:GetLua_v1()
  local def = self.def

  local x, y = def.position.x, def.position.y
  local t = "static"
  if def.type == "dynamicBody" then
    t = "dynamic"
  elseif def.type == "kinematicBody" then
    t = "kinematic"
  end
  local id = self.id

  local sz = "bodies[%d] =\n{\n  type = '%s',\n  x = %s,\n  y = %s"
  sz = string.format(sz, id, def.type, x, y)
  local out = { sz }
  if def.linearDamping ~= 0 then
    local sz = string.format("linearDamping = %s", def.linearDamping)
    table.insert(out, sz)
  end
  if def.angularDamping ~= 0 then
    local sz = string.format("angularDamping = %s", def.angularDamping)
    table.insert(out, sz)
  end
  if def.fixedRotation == true then
    local sz = "fixedRotation = true"
    table.insert(out, sz)
  end
  if def.isBullet == true then
    local sz = "bullet = true"
    table.insert(out, sz)
  end
  if def.allowSleep == true then
    local sz = "allowSleep = true"
    table.insert(out, sz)
  end
  if def.isSleeping == true then
    local sz = "isSleeping = true"
    table.insert(out, sz)
  end
  if def.gravityScale ~= 1 then
    local sz = string.format("gravityScale = %s", def.gravityScale)
    table.insert(out, sz)
  end
  if def.active == false then
    local sz = ",\n active = false"
    table.insert(out, sz)
  end

  local lvx, lvy = self:GetLinearVelocity()
  if lvx ~= 0 or lvy ~= 0 then
    local sz = string.format("linearVelocityX = %s", lvx)
    table.insert(out, sz)
    local sz = string.format("linearVelocityY = %s", lvy)
    table.insert(out, sz)
  end
  local av = self:GetAngularVelocity()
  if av ~= 0 then
    local sz = string.format("angularVelocity = %s", av)
    table.insert(out, sz)
  end

  if #self.shapes > 0 then
    table.insert(out, "fixtures =\n  {  \n")
    local fix = {}
    for i, v in ipairs(self.shapes) do
      local shape = v:GetLua_v1()
      table.insert(fix, shape)
    end
    local sz = table.concat(fix, ",\n    ")
    table.insert(out, string.format("fixtures =\n  {\n%s\n  }", sz))
  end
  
  table.insert(out, "}\n")
 
  return table.concat(out, ",\n  ")
end

----------------------------------------------------

function Body:GetXML()
  return self:GetXML_v2()
end