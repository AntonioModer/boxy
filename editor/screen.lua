local gstates =
{
  BodyState,
  VertexState,
  JointState,
  ModelState
}
local tools =
{
  AddVertexTool,
  AddJointTool,
  AnchorTool,
  CircleTool,
  JSelectTool,
  JTranslateTool,
  PivotTool,
  PolygonTool,
  RotateTool,
  ScaleTool,
  SelectTool,
  TranslateTool,
  VSelectTool,
  VTranslateTool,
  AddModelTool,
  BoxTool
}

gui.Screen = function()
  local screen = gui.VContainer(0, 0, conf.resx, conf.resy)
  
  screen.tooltip = Sprite()
  screen.tooltip.depth = -10000
  screen.node:add_child(screen.tooltip)

  screen.gstates = {}
  for i, v in ipairs(gstates) do
    screen.gstates[v] = v:Create(screen)
  end
  screen.tools = {}
  for i, v in ipairs(tools) do
    screen.tools[v] = v:Create(screen)
  end
  
  screen.reset = function(self, w, h)
    screen:set_size(w, h)
    for i, v in pairs(screen.gstates) do
      v:ResetProperties()
    end
  end

  screen.get_world = function(self)
    return self.scene:GetWorld()
  end

  screen.get_scene = function(self)
    return self.scene
  end
  screen.set_scene = function(self, scene)
    self.scene = scene
    local c = scene:GetCamera()
    self:set_camera(c)
  end

  screen.wheel_move = function(self, n)
    gui.VContainerG.wheel_move(self, n)
    if self.focus then
      return
    end
    self.scene:ChangeZoom(n)
  end

  screen.mouse_press = function(self, button, x, y)
    gui.VContainerG.mouse_press(self, button, x, y)
    if self.focus then
      return
    end
    if button ~= MBUTTON_LEFT then
      return
    end
    local keyboard = keyboard
    local shift = keyboard:is_down(KEY_LSHIFT)
    shift = shift or keyboard:is_down(KEY_RSHIFT)
    local alt = keyboard:is_down(KEY_LALT)
    alt = alt or keyboard:is_down(KEY_RALT)
    local ctrl = keyboard:is_down(KEY_LCTRL)
    ctrl = ctrl or keyboard:is_down(KEY_RCTRL)

    x, y = self:local_to_camera(x, y)
    local camera = self:get_camera()
    local wx, wy = camera:get_world_point(x, y)
    self.tool:MousePress(wx, wy, alt, shift, ctrl)
  end

  screen.mouse_release = function(self, button, x, y)
    gui.VContainerG.mouse_release(self, button, x, y)
    if self.focus then
      return
    end
    if button ~= MBUTTON_LEFT then
      return
    end

    x, y = self:local_to_camera(x, y)
    local camera = self:get_camera()
    local wx, wy = camera:get_world_point(x, y)
    self.tool:MouseRelease(wx, wy)
  end

  screen.double_click = function(self, button, x, y)
    gui.VContainerG.double_click(self, button, x, y)
    if self.focus then
      return
    end
    if button ~= MBUTTON_LEFT then
      return
    end
    self.tool:DoubleClick(wx, wy, owx, owy)
  end

  screen.mouse_move = function(self, x, y, x2, y2)
    gui.VContainerG.mouse_move(self, x, y, x2, y2)
    if self.focus then
      --return
    end
    local camera = self:get_camera()
    x, y = self:local_to_camera(x, y)
    x2, y2 = self:local_to_camera(x2, y2)
    local wx, wy = camera:get_world_point(x, y)
    local owx, owy = camera:get_world_point(x2, y2)
    
    if mouse:is_down(MBUTTON_RIGHT) then
      local wdx, wdy = (wx - owx), (wy - owy)
      self.scene:MoveCamera(-wdx, -wdy)
      return
    end

    self.tool:MouseMove(wx, wy, owx, owy)
  end

  screen.set_state = function(self, state, ...)
    if self.gstates[state] == self.gstate then
      return
    end
    if self.gstate then
      self.gstate:Deselect() --Destroy()
    end
    self.gstate = self.gstates[state]
    self.gstate:Select(...)
  end

  screen.set_tool = function(self, state, ...)
    if self.tool then
      self.tool:Deselect() --Destroy()
    end
    self.tool = self.tools[state]
    self.tool:Select(...)
  end
  
  screen.load = function(self)
    local win = gui.FileDialog(200, 100, "Load file", conf.curpath)
    self:add_child(win)
    self:set_focus(win, true)
    win.on_ok = function(win, p)
      conf.curpath = win.path
      local world = LoadWorld(p)
      if world then
        self:remove_child(win)
        win:destroy()
        return
      end
      local sz = string.format("Could not load %s", p)
      local diag = gui.Warning(50, 50, "Load file error", sz)
      win:add_child(diag)
      win:set_focus(diag, true)
      diag.on_ok = function(diag)
        win:remove_child(diag)
        diag:destroy()
      end
    end
    win.on_cancel = function(win)
      conf.curpath = win.path
      self:remove_child(win)
      win:destroy()
    end
  end
  
  screen.save_file = function(self, file, func)
    local world = self:get_world()
--[[
-- optimize ids
local bid = 1
local bodies = {}
for _, b in pairs(world.bodies) do
  b.id = bid
  bodies[bid] = b
  bid = bid + 1
end
world.bodies = bodies

local joints = {}
local jid = 1
for _, j in pairs(world.joints) do
  j.id = jid
  joints[jid] = j
  jid = jid + 1
end
world.joints = joints
]]
    conf.lastfile = io.sanitize(file)
    world[func](world, file)
  end

  screen.save = function(self, f)
    local funcs =
    {
      ["XML"] = "SaveXML",
      ["Lua"] = "SaveLua"
    }
    local func = funcs[f]
    assert(func)
    local sz = string.format("Save %s file", f)
    local win = gui.FileDialog(200, 100, sz, conf.curpath)
    self:add_child(win)
    self:set_focus(win, true)
    
    win.on_ok = function(win, p)
      conf.curpath = win.path
      local attr = lfs.attributes(p)
      if attr ~= nil then
        local title = "File already exists"
        local desc = string.format("Overwrite %s?", p)
        local c = gui.OkCancel(50, 50, title, desc)
        c.on_ok = function(c)
          self:save_file(p, func)
          self:remove_child(win)
          win:destroy()
        end
        c.on_cancel = function(c)
          win:remove_child(c)
          c:destroy()
        end
        win:add_child(c)
        win:set_focus(c, true)
        return
      end
      self:save_file(p, func)
      self:remove_child(win)
      win:destroy()
    end
    win.on_cancel = function(win)
      conf.curpath = win.path
      self:remove_child(win)
      win:destroy()
    end
  end
  
  screen.show_help = function(self)
    local f = io.open("boxy/readme.txt")
    if f == nil then
      return
    end
    local sz = f:read("*all")
    f:close()
    local win = gui.Window(200, 100, 500, 310, "Help")
    win.node.depth = -4
    local tb = gui.Textbox(10, 10, 480, 260)
    tb.readonly = true
    local ok = gui.Button(10, 280, "OK", 100)
    ok:set_style("align", "center")
    win:add_child(tb)
    win:add_child(ok)
    self:add_child(win)
    self:set_focus(win, true)
    tb.text = sz
    
    ok.on_click = function(ok)
      self:remove_child(win)
    end
  end
  screen.show_xml = function(self)
    local w = scene:GetWorld()
    local sz = xml.str(w:GetXML())
    local win = gui.Window(200, 100, 500, 410, "Preview XML")
    win.node.depth = -4
    local tb = gui.Textbox(10, 10, 480, 360)
    tb.readonly = false
    local ok = gui.Button(10, 380, "OK", 100)
    ok:set_style("align", "center")
    win:add_child(tb)
    win:add_child(ok)
    self:add_child(win)
    self:set_focus(win, true)
    tb.text = sz
    
    ok.on_click = function(ok)
      self:remove_child(win)
    end
  end

  screen.key_command = function(self, key, ctr, shift)
    if key == KEY_S then
      if ctr == true then
        screen:save("XML")
      end
    elseif key == KEY_O then
      if ctr == true then
        screen:load()
      end
    elseif key == KEY_E then
      if ctr == true then
        screen:save("Lua")
      end
    end
    if self.focus == nil then
      if key == KEY_G then
        self.scene:ToggleGrid()
      elseif key == KEY_F1 then
        screen:show_help()
      elseif key == KEY_F2 then
        self:set_state(BodyState)
        self:set_tool(SelectTool)
      elseif key == KEY_F3 then
        self:set_state(VertexState)
        self:set_tool(VSelectTool)
      elseif key == KEY_F4 then
        self:set_state(JointState)
        self:set_tool(JSelectTool)
      elseif key == KEY_F5 then
        self:set_state(ModelState)
        self:set_tool(AddModelTool)
      elseif key == KEY_F12 then
        self:show_xml()
      end
      self.tool:KeyPress(key, ctr, shift)
      self.gstate:KeyPress(key, ctr, shift)
    end
    gui.VContainerG.key_command(self, key, ctr, shift)
  end

  screen.update = function(self, dt)
    gui.VContainerG.update(self, dt)
    self.sprite.canvas:clear()
    
    -- must be updated before states 
    self.scene:Redraw()
    
    self.gstate:Redraw(dt)
    self.tool:Redraw(dt)
    
    local mx, my = gui.get_mouse_position()
    local w, h = self:get_size()
    local t = self:query_local(mx, my)
    if t then
      local lx, ly = mx, my
      while t do
        if t.query_local then
          lx, ly = t:parent_to_local(lx, ly)
          local t2 = t:query_local(lx, ly)
          if t2 == nil then
            break
          end
          t = t2
        else
          break
        end
      end
    end
    local c = self.tooltip.canvas
    c:clear()
    mx, my = mx - w/2, -my + h/2
    if t and t.tooltip then
      local sz = t.tooltip
      local h = font:get_size()
      local w = font:get_width(sz)
      mx = math.min(mx, conf.resx/2 - w - 10)
      mx = math.max(mx, -conf.resx/2 + 10)
      c:move_to(mx, my - 30)
      c:rel_move_to(-5, -5)
      c:rel_line_to(0, h + 10)
      c:rel_line_to(w + 10, 0)
      c:rel_line_to(0, -h - 10)
      c:close_path()
      c:set_fill_style(WHITE, 1)
      c:set_line_style(1, BLACK, 1)
      c:fill_preserve()
      c:stroke()
      c:set_font(font, BLACK, 1)
      c:move_to(mx, my - 30)
      c:write(sz)
    end
  end
  
  return screen
end