Scene = {}
SceneMT = { __index = Scene }

function Scene:Create()
  local self = {}
  setmetatable(self, SceneMT)
  
  self.bodies = {}
  
  self.font = font

  self.layer = Layer()
  self.layer.depth = 1
  self.background = Sprite()
  self.background.depth = 1
  self.foreground = Sprite()
  self.foreground.depth = -1
  self.overlay = Sprite()
  self.overlay.depth = 1
  self.camera = Camera()
  self.layer:add_child(self.background)
  self.layer:add_child(self.foreground)
  --self.layer:add_child(self.overlay)
  self.layer:add_child(self.camera)

  self.outlines = true
  self.centers = true
  self.vertices = true
  self.grid = true
  self.gridres = 0.001
  
  return self
end

function Scene:Destroy()
  self.bodies = nil

  self.layer:remove_children()
  self.layer = nil
  self.background = nil
  self.foreground = nil
  self.overlay = nil
  self.camera = nil

  self.outlines = nil
  self.centers = nil
  self.vertices = nil
  self.grid = nil
  self.gridres = nil
end

function Scene:GetWorld()
  return self.world
end

function Scene:SetWorld(world)
  self:RemoveBodies()
  self.world = world
  for _, b in pairs(world.bodies) do
    self:AddBody(b)
  end
  self:ResetZoom()
end

function Scene:GetCamera()
  return self.camera
end

function Scene:SetStyle(c, v)
  self.centers = c
  self.vertices = v
  self:RedrawBodies()
end


function Scene:GetZoom()
  return self.camera.scalex
end

function Scene:ResetZoom()
  local l, t, r, b
  for body, _ in pairs(self.bodies) do
    local l2, t2, r2, b2 = body:GetWorldAABB()
    if l2 and t2 and r2 and b2 then
      if l and t and r and b then
        l = math.min(l, l2)
        t = math.min(t, t2)
        r = math.max(r, r2)
        b = math.max(b, b2)
      else
        l, t, r, b = l2, t2, r2, b2
      end
    end
  end
  if l == nil or t == nil or r == nil or b == nil then
    local d = self.world.def
    --local l, t = d.left, d.top
    --local r, b = d.right, d.bottom
    local l, t = -10, -10
    local r, b = 10, 10
    local w, h = r - l, b - t
    local side = math.max(w, h)
    local s = side/600
    self.camera:set_scale(s, s)
    local x, y = (r + l)/2, (t + b)/2
    self.camera:set_position(x, y)
    return
  end
  local w, h = r - l, b - t
  local side = 10--math.max(w, h)
  local s = side/600
  s = math.max(s, 0.001)
  self.camera:set_scale(s, s)
  local x, y = (r + l)/2, (t + b)/2
  self.camera:set_position(x, y)
  self:RedrawBodies()
end

function Scene:ChangeZoom(notches)
  local d = 1
  if notches > 0 then
    d = 1.1
  elseif notches < 0 then
    d = 0.9
  end
  local s = self:GetZoom()*d
  s = math.max(s, 0.001)
  self.camera:set_scale(s, s)
  self:RedrawBodies()
end

function Scene:MoveCamera(dx, dy)
  self.camera:change_position(dx, dy)
end


-- Snaps world point to grid
function Scene:ToGrid(x, y)
  if self.gridres == nil then
    return x, y
  end
  local igrid = 1/self.gridres
  local gx = math.floor(x*igrid + 0.5)/igrid
  local gy = math.floor(y*igrid + 0.5)/igrid
  return gx, gy
end

-- Changes grid resolution
function Scene:ToggleGrid()
  if self.gridres == nil then
    self.gridres = 0.001
  end
  -- increment grid resolution
  self.gridres = self.gridres*10
  if self.gridres >= 10 then
    self.gridres = nil
  end
end

function Scene:RedrawGridRes(step, color, alpha)
  assert(step > 0)
  local camera = self.camera
  local istep = 1/step
  assert(istep > 0)
  local ow, oh = conf.resx, conf.resy
  local hw, hh = ow/2, oh/2
  local l2, t2 = camera:get_world_point(-hw, -hh)
  local r2, b2 = camera:get_world_point(hw, hh)
  l2 = math.floor(l2*istep)/istep
  t2 = math.floor(t2*istep)/istep
  r2 = math.ceil(r2*istep)/istep
  b2 = math.ceil(b2*istep)/istep

  local d = self.world.def
  --local l, t = d.left, d.top
  --local r, b = d.right, d.bottom
  local l, t = -1000, -1000
  local r, b = 1000, 1000
  local c = self.background.canvas
  -- vertical lines
  for x = l2, r2, step do
    if x >= l and x <= r then
      self:DrawLineC(c, x, t, x, b, 1, color, alpha)
    end
  end
  -- horizontal lines
  for y = t2, b2, step do
    if y >= t and y <= b then
      self:DrawLineC(c, l, y, r, y, 1, color, alpha)
    end
  end
end

function Scene:RedrawGrid()
  if self.grid == false or self.gridres == nil then
    return
  end
  -- clamp grid resolution depending on the camera zoom
  local step = self:GetZoom()*30
  step = 10^math.floor(math.log10(step) + 0.5)
  step = math.max(step, self.gridres)
  self:RedrawGridRes(step, WHITE, 0.1, false)
  self:RedrawGridRes(step*10, WHITE, 0.2)
end

local controls =
{
  anchor = 0, center = 1, rotate = 2, resizer = 3,
  distance = 4, prismatic = 5, revolute = 6, circle = 7,
  gear = 8, anchor1 = 9, anchor2 = 10, rope = 11
}

local img = Image()
if img:load("boxy/icons/controls.png") == false then
  img = nil
end

function Scene:DrawImage(icon, wx, wy)
  local x, y = self.camera:get_local_point(wx, wy)
  local frame = controls[icon]
  --local img = display.gfx.load(img)
  if img and frame then
    local c = self.overlay.canvas

    -- frame dimensions from cols and rows
    local w = img.width/4
    local h = img.height/4
    -- position in image
    local sx = frame%4*w
    local sy = math.floor(frame/4)*h
    
    --local sx, sy, w, h = img:get_subimage(frame, 4, 4)
    c:set_source_subimage(img, sx, sy, w, h, x, y)
    c:paint()
  end
end

function Scene:DrawImageS(icon, wx, wy)
  self:DrawImage(icon, wx, wy, "boxy/icons/controlsS.png")
end

function Scene:DrawLineC(c, x1, y1, x2, y2, line, color, alpha)
  line = line*self:GetZoom()

  c:move_to(x1, y1)
  c:line_to(x2, y2)
  c:set_line_style(line, color, alpha)
  c:stroke()
end

function Scene:DrawLine(x1, y1, x2, y2, line, color, alpha)
  local c = self.foreground.canvas
  self:DrawLineC(c, x1, y1, x2, y2, line, color, alpha)
end

function Scene:DrawCurve(x1, y1, x2, y2, x3, y3, line, color, alpha)
  line = line*self:GetZoom()
  local c = self.foreground.canvas
  c:move_to(x1, y1)
  
  c:curve_to(x2, y2, x3, y3)
  c:set_line_style(line, color, alpha)
  c:stroke()
end

function Scene:DrawArc(x, y, s, e, r, line, color, alpha)
  local z = self:GetZoom()
  line = line*z
  r = r*z
  local c = self.foreground.canvas
  c:move_to(x, y)

  local range = e - s
  local seg = 32--math.ceil(range/360*32)
  for i = 0, seg do
    local a = range/seg*i + s
    local ar = math.rad(a)
    local nx, ny = math.cos(ar), math.sin(ar)
    nx, ny = nx*r, ny*r
    if i == 0 then
      c:move_to(nx + x, ny + y)
    else
      c:line_to(nx + x, ny + y)
    end
  end
  --c:arc(s, e, r, r)
  c:set_line_style(line, color, alpha)
  c:stroke()
end
--[[
function Scene:DrawArcW(x, y, s, e, r, line, color, alpha)
  local lx, ly = self.camera:get_local_point(x, y)
  --r = self.camera:get_local_point(r, 0)
  s, e = math.deg(s), math.deg(e)
  self:DrawArc(lx, ly, s, e, r, line, color, alpha)
end
]]
function Scene:DrawFRectangleC(c, x, y, w, h, color, alpha)
  c:move_to(x, y)
  c:rectangle(w, h)
  c:set_fill_style(color, alpha)
  c:fill()
end

function Scene:DrawFRectangle(x, y, w, h, color, alpha)
  local c = self.foreground.canvas
  self:DrawFRectangleC(c, x, y, w, h, color, alpha)
end

function Scene:DrawRectangleC(c, x, y, w, h, line, color, alpha)
  c:move_to(x, y)
  c:rectangle(w, h)
  c:set_line_style(line, color, alpha)
  c:stroke()
end

function Scene:DrawRectangle(wx, wy, w, h, line, color, alpha)
  local z = self:GetZoom()
  line = line*z
  local c = self.foreground.canvas
  self:DrawRectangleC(c, wx, wy, w, h, line, color, alpha)
end

function Scene:DrawRRectangle(wx, wy, w, h, a, line, color, alpha)
  local z = self:GetZoom()
  line = line*z
  local c = self.foreground.canvas
  c:rrectangle(wx, wy, w, h, a)
  c:set_line_style(line, color, alpha)
  c:stroke()
end

function Scene:DrawCircleC(c, x, y, r, line, color, alpha)
  c:move_to(x, y)
  c:circle(r)
  c:set_line_style(line, color, alpha)
  c:stroke()
end

-- accepts world coords, draws on overlay
function Scene:DrawCircle(wx, wy, r, line, color, alpha)
  local z = self:GetZoom()
  line = line*z
  local c = self.foreground.canvas
  self:DrawCircleC(c, wx, wy, r, line, color, alpha)
end

function Scene:DrawBackground()
  local c = self.background.canvas
  local d = self.world.def
  --local l, t = d.left, d.top
  --local r, b = d.right, d.bottom
  local l, t = -1000, -1000
  local r, b = 1000, 1000
  local x, y = (l + r)/2, (t + b)/2
  self:DrawFRectangleC(c, x, y, r - l, b - t, GRAY, 1)
end

function Scene:Redraw()
  self.overlay.canvas:clear()
  
  self.background.canvas:clear()
  self.foreground.canvas:clear()
  
  self:DrawBackground()
  self:RedrawGrid()
  
  --self:DrawRectangle(0, 0, 5, 5/1.7791, 1, RED, 1)
  self:DrawRectangle(0, 0, conf.areaw, conf.areah, 1, RED, 1)
end

function Scene:AddBody(body)
  local s = Sprite()
  self.layer:add_child(s)
  assert(self.bodies[body] == nil, "Duplicate body")
  self.bodies[body] = s
  self:RedrawBody(body)
end

function Scene:RemoveBody(body)
  local s = self.bodies[body]
  self.bodies[body] = nil
  self.layer:remove_child(s)
end

function Scene:RemoveBodies()
  for i, _ in pairs(self.bodies) do
    self:RemoveBody(i)
  end
end

function Scene:SyncBody(body)
  local s = self.bodies[body]
  local x, y = body:GetPosition()
  local a = body:GetAngle()
  s:set_position(x, y)
  s:set_rotation_r(-a)
end

function Scene:RedrawBodies()
  for body, _ in pairs(self.bodies) do
    self:RedrawBody(body)
  end
end

function Scene:RedrawBodiesList(bodies)
  for _, body in pairs(bodies) do
    self:RedrawBody(body)
  end
end

function Scene:ClearBodyHighlights()
  for i, v in pairs(self.bodies) do
    v.color = WHITE
  end
end

function Scene:HighlightBody(body)
  self.bodies[body].color = ORANGE
end

local hue = Color(255, 255, 255)
function Scene:RedrawBody(body)
  self:SyncBody(body)
  
  local line = self:GetZoom()

  local sprite = self.bodies[body]
  local c = sprite.canvas
  c:clear()
  local s = 1/sprite.scalex
  local line = s*line

  local color = BLACK
  if body.def.type == "dynamicBody" then
    color = GREEN
  elseif body.def.type == "kinematicBody" then
    color = PURPLE
  end
  -- collision shapes
  for i, v in ipairs(body.shapes) do
    local t = v:GetType()
    if t == "circle" then
      local def = v.def
      local lp = def.localPosition
      c:move_to(lp.x*s, lp.y*s)
      c:circle(def.radius*s)
      c:rel_line_to(0, def.radius*s)
    elseif t == "polygon" or t == "concave" or t == "chain" or t == "loop" then
      local vertices = v.def.vertices
      c:move_to(vertices[1].x*s, vertices[1].y*s)
      for i = 2, #vertices do
        local v = vertices[i]
        c:line_to(v.x*s, v.y*s)
      end
      if t ~= "chain" then
        c:close_path()
      end
    end
    --v:Plot(c, scale)
    
    local alpha = 0.75
    if self.outlines == true then
      local valid = v:IsValid()
      local lcolor = hue
      lcolor.red = 0
      lcolor.green = 255*(v.def.friction)
      lcolor.blue = 255*(1 - v.def.restitution)
      if valid == false then
        lcolor = RED
      end
      c:set_line_style(line*2, lcolor, 1)
      if valid == true and t ~= "chain" and t ~= "loop" then
        c:stroke_preserve()
      else
        c:stroke()
      end
    else
      alpha = 1
    end
    if v.def.isSensor == true then
      alpha = alpha/2
    end
    --[[
    if v.def.density > 0 then
      color = Color(64, 128, 64)
      local b = math.min(v.def.density, 1)
      color.green = 128 + 127*b
    end
    ]]
    c:set_fill_style(color, alpha)
    if t == "polygon" then
      if v.valid == false then
        c:set_fill_style(RED, alpha)
      end
    end
    c:fill()
    

    if t == "concave" then
      local out = v.triangles
      if out then
        for i = 1, #out, 3 do
          local ax, ay = out[i].x, out[i].y
          local bx, by = out[i + 1].x, out[i + 1].y
          local cx, cy = out[i + 2].x, out[i + 2].y
          c:move_to(ax, ay)
          c:line_to(bx, by)
          c:line_to(cx, cy)
          c:close_path()
          c:set_line_style(line, WHITE, 0.5)
          if v.trianglesv[(i - 1)/3] == false then
            c:set_fill_style(RED, 0.5)
            c:fill_preserve()
          end
          c:stroke()
        end
      end
    end
  end

  -- center of body
  if self.centers == true then
    local r = line*4
    c:move_to(0, 0)
    c:line_to(0, r)
    c:move_to(0, 0)
    c:line_to(-r, -r)
    c:move_to(0, 0)
    c:line_to(r, -r)
    c:set_line_style(line, WHITE, 1)
    c:stroke()
  end

--c:set_font(self.font, WHITE, 1)
--c:write("hdsafas")
--[[
  -- linear velocity
  local lvx, lvy = body:GetLinearVelocity()
  lvx, lvy = body:GetWorldVector(lvx, lvy)
  c:move_to(0, 0)
  c:line_to(lvx, lvy)
  c:set_line_style(line, RED, 1)
  c:stroke()
]]
  -- vertices
  if self.vertices == true then
    local sq = line*2.5
    local a = -body:GetAngle()
    for _, shape in ipairs(body.shapes) do
      local t = shape:GetType()
      if t == 'circle' then
        local lc = shape.def.localPosition
        c:rrectangle(lc.x*s, lc.y*s, sq, sq, a)
      elseif t == 'polygon' or t == 'concave' or t == 'chain' or t == 'loop' then
        for _, vx in ipairs(shape.def.vertices) do
          c:rrectangle(vx.x*s, vx.y*s, sq, sq, a)
        end
      end
    end
    c:set_line_style(line, WHITE, 1)
    c:stroke()
  end
end

-- sprite coords
function Scene:DrawText(sz, x, y, color, alpha)
  local c = self.overlay.canvas
  c:move_to(x, y)
	c:set_font(self.font, color, alpha)
  c:write(sz)
end

function Scene:DrawTextW(sz, wx, wy, color, alpha)
  local lx, ly = self.camera:get_local_point(wx, wy)
  self:DrawText(sz, lx, ly, color, alpha)
end

function Scene:DrawStatus(sz)
  local x, y = -conf.resx/2 + 5, -conf.resy/2 + 5
	self:DrawText(sz, x, y, ORANGE, 1)
end

function Scene:DrawTooltip(sz, wx, wy)
  -- convert from world coords to camera coords
  local camera = self.camera
  local x, y = camera:get_local_point(wx, wy)
  -- convert from camera coords to sprite coords
  --local lx, ly = self:camera_to_local(x, y)
  y = y - 20

  local w, h = self.font:get_dimensions(sz)
  local pad = 5
  w = w + pad*2
  h = h + pad*2
  local lh = self.font:get_height()

	local lx, ly = x + w/2, y - h/2
  local c = self.overlay.canvas
	self:DrawFRectangleC(c, lx, ly, w, h, WHITE, 1)
	x, y = x + pad, y - lh
	self:DrawText(sz, x, y, BLACK, 1)
end

Canvas.rrectangle = function(self, x, y, w, h, r)
  local cr = math.cos(r)
  local sr = math.sin(r)
  local crw = cr*w
  local crh = cr*h
  local srw = sr*w
  local srh = sr*h
  local x1 = x + crw - srh 
  local y1 = y + crh + srw
  local x2 = x - crw - srh
  local y2 = y + crh - srw
  local x3 = x - crw + srh
  local y3 = y - crh - srw
  local x4 = x + crw + srh
  local y4 = y - crh + srw
  self:move_to(x1, y1)
  self:line_to(x2, y2)
  self:line_to(x3, y3)
  self:line_to(x4, y4)
  self:close_path()
end
-- Finds the max width and total \height in pixels of a given string
Font.get_dimensions = function(self, sz)
  local lh = self:get_height()
  local w, h = self:get_width(sz), lh
  local i = 0
  local len = string.len(sz)
  for j = 1, len do
    local b = string.byte(sz, j)
    i = i + 1
    if string.char(b) == '\n' then
      local line = string.sub(sz, j - i, j)
      local lw = self:get_width(line)
      w = math.max(w, lw)
      h = h + lh
    end
  end
	return w, h
end