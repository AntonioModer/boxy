gui.Warning = function(x, y, title, desc)
  local win = gui.Window(x, y, 300, 85, title)
  win.node.depth = -10
  local label = gui.Label(10, 10, desc, 280)
  label:set_style("align", "left")
  
  win.on_ok = function(win, path)
    
  end

  local cx = 300/2 - 100/2
  local ok = gui.Button(cx, 50, "OK", 100)
  ok.on_click = function(ok)
    win:on_ok()
  end

  win.ok = ok

  win:add_child(label)
  win:add_child(ok)
  
  return win
end