
-- removes "./" and "../" from path string
local strip_path = function(path)
  return io.sanitize(path)
end

gui.FileDialog = function(x, y, title, path)
  local win = gui.Window(x, y, 400, 305, title)
  win.node.depth = -5
  
  win.on_ok = function(win, path)
  
  end
  win.on_cancel = function(win)
  
  end

  win.set_path = function(win, p)
    local attr = lfs.attributes(p)
    if attr == nil or attr.mode ~= 'directory' then
      return false
    end
    p = './' .. strip_path(p)
    win.path = p
    win.list.items = {}
    -- calculate the total number of bytes to be loaded
    for file in lfs.dir(p) do
      if file ~= "." then
        win.list:add_items(file)
      end
    end
    win.list:set_index(1)
    win.title = title .. " (" .. p .. ")"
    return true
  end
  
  gui.set_style("line_spacing", 210/14)
  local list = gui.List(10, 10, 380, 14)
  list.node.depth = -1
  local input = gui.Input(10, 240, 380)
  local ok = gui.Button(10, 270, "OK", 100)
  local cancel = gui.Button(120, 270, "Cancel", 100)
  ok:set_style("align", "center")
  cancel:set_style("align", "center")

  list.on_change = function(list, i)
    win.input.text = list.items[i]
    win.input:select_all()
  end
  list.on_select = function(list, i)
    win.input.text = list.items[i]
    win.ok:on_click()
  end
  
  ok.on_click = function(ok)
    local sz = win.input.text
    if sz == "" then
      return
    end
    local p = string.format("%s/%s", win.path, sz)
    if win:set_path(p) == true then
      return
    end
    win:on_ok(p)
  end
  cancel.on_click = function(cancel)
    win:on_cancel()
  end
  
  win.list = list
  win.input = input
  win.ok = ok
  win.cancel = cancel
  
  win:add_child(list)
  win:add_child(input)
  win:add_child(ok)
  win:add_child(cancel)
  
  if win:set_path(path) == false then
    win:set_path(".")
  end
  
  return win
end