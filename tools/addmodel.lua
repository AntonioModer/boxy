AddModelTool = {}
AddModelToolMT = { __index = AddModelTool }

setmetatable(AddModelTool, { __index = Tool })

function AddModelTool:Create(owner)
  local self = Tool:Create(owner, AddModelToolMT)
  return self
end

function AddModelTool:Destroy()
  Tool.Destroy(self)
end

function AddModelTool:Select(mode)
  if mode == nil then
    for i, v in pairs(models) do
      mode = i
      break
    end
    assert(mode)
  end
  mode = mode
  local world = self.owner:get_world()
  self.mode = mode
  self.model = models[mode]:Create(world)
end

function AddModelTool:Deselect()
  self.mode = nil
  self.model:Destroy()
  self.model = nil
end

function AddModelTool:MousePress(wx, wy, alt, shift)
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  self.sx, self.sy = wx, wy
end

function AddModelTool:MouseMove(wx, wy, owx, owy)
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  self.ex, self.ey = wx, wy
end

function AddModelTool:MouseRelease(wx, wy)
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  local gstate = self.owner.gstate
  local scene = self.owner:get_scene()
  wx, wy = gstate:ToGrid(wx, wy)
  local bodies = self.model:Init(sx, sy, wx, wy)
  for i, v in pairs(bodies) do
    --v:UpdateSprite(0)
    scene:AddBody(v)
    scene:RedrawBody(v)
  end

  self.sx, self.sy = nil, nil
  self.ex, self.ey = nil, nil
end

function AddModelTool:Redraw(dt)
  local owner = self.owner
  local camera = owner:get_camera()
  local scene = owner:get_scene()
  --[[
  local level = owner:get_world()
  level.sprite.canvas:clear()
  ]]
  local ex, ey = self.ex, self.ey
  if ex == nil or ey == nil then
    return
  end
  local sx, sy = self.sx or ex, self.sy or ey
  local s = camera.scalex
  self.model:Preview(sx, sy, ex, ey, s)

  scene:DrawStatus(self.mode)
end