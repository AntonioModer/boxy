--
-- Translates a list of vertices
--
VTranslateTool = {}
VTranslateToolMT = { __index = VTranslateTool }

setmetatable(VTranslateTool, { __index = Tool })

function VTranslateTool:Create(owner)
  local self = Tool:Create(owner, VTranslateToolMT)

  return self
end

function VTranslateTool:Destroy()
  Tool.Destroy(self)
end


function VTranslateTool:Select(selection, selection2)
  self.selection = selection
  self.selection2 = selection2
end

function VTranslateTool:Deselect()
  self.selection = nil
  self.selection2 = nil
end

function VTranslateTool:MousePress(wx, wy)
end

function VTranslateTool:MouseMove(wx, wy, owx, owy)
  local owner = self.owner
  local selection = self.selection
  local selection2 = self.selection2
  local gstate = owner.gstate
  local scene = owner:get_scene()
  
  -- todo prevent from moving outside world bounds
  local vertex = selection[1]
  local shape = selection2[1]
  local wvx, wvy = shape.body:GetWorldPoint(vertex.x, vertex.y)
  -- snap to grid
  wx, wy = gstate:ToGrid(wx, wy)
  owx, owy = wx - (wx - wvx), wy - (wy - wvy)
  local wdx, wdy = wx - owx, wy - owy
  if wdx == 0 and wdy == 0 then
    return
  end

  -- disallow moving 2 vertices (from the same shape) on top of each other
  -- todo
  local r = b2.linearSlop/2
  local sq = 4*scene:GetZoom()
  local q, qs = gstate:QueryVertices(wx, wy, wx, wy, sq)
  for i, v2 in ipairs(q) do
    if qs[i] == shape and v2 ~= vertex then
      local w2vx, w2vy = qs[i].body:GetWorldPoint(v2.x, v2.y)
      local dx = math.abs(w2vx - wx)
      local dy = math.abs(w2vy - wy)
      if dx < r and dy < r then
        return
      end
    end
  end

  gstate:TranslateVertices(selection, selection2, wdx, wdy)

  local bodies = {}
  for i, v in ipairs(selection2) do
    bodies[v.body.id] = v.body
  end
  -- mark invalid shapes red
  scene:RedrawBodiesList(bodies)
end

function VTranslateTool:MouseRelease(wx, wy)
  self.owner:set_tool(VSelectTool)
end

function VTranslateTool:Redraw(dt)
  local scene = self.owner:get_scene()
  local sz = "Moving selection (%d vertices)"
  sz = string.format(sz, #self.owner.gstate.selection)
  scene:DrawStatus(sz)
end