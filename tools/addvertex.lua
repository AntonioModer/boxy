--
-- Inserts a vertex
--
AddVertexTool = {}
AddVertexToolMT = { __index = AddVertexTool }

setmetatable(AddVertexTool, { __index = Tool })

function AddVertexTool:Create(owner)
  local self = Tool:Create(owner, AddVertexToolMT)
  
  return self
end

function AddVertexTool:Destroy()
  Tool.Destroy(self)
end


function AddVertexTool:MousePress(wx, wy)
  local owner = self.owner
  local gstate = owner.gstate
  local scene = owner:get_scene()

  local sq = 4*scene:GetZoom()
  local q, s = gstate:QueryVertices(wx, wy, wx, wy, sq)
  if #q > 0 then
    owner:set_tool(VSelectTool)
    owner.tool:MousePress(wx, wy)
  end
end

function AddVertexTool:MouseRelease(wx, wy)
  local owner = self.owner
  local gstate = owner.gstate
  local scene = owner:get_scene()
  local world = owner:get_world()
  local sx, sy = self.sx, self.sy
  local z = scene:GetZoom()
  if sx == nil or sy == nil then
    return
  end

  local wx, wy, _, shape, i = self:NearestVertex(world, wx, wy, z*15)

  -- clicked on an existing vertex?
  local q, s = gstate:QueryVertices(wx, wy, wx, wy, 4*z)
  if #q > 0 then
    gstate:ClearSelection()
    gstate:AddToSelection({ q[1] }, { s[1] })
    return
  end
  
  local body = shape.body
  local t = shape:GetType()
  assert(t == 'polygon' or t == 'concave' or t == 'chain' or t == 'loop', t)
  local lx, ly = body:GetLocalPoint(wx, wy)
  local vertex = shape:AddVertex(lx, ly, i + 1)
  scene:RedrawBody(body)
  
  self.sx = nil
  self.sy = nil
  if vertex and shape then
    gstate:ClearSelection()
    gstate:AddToSelection({ vertex }, { shape })
    --owner:set_tool(VSelectTool)
  end
end

function AddVertexTool:MouseMove(wx, wy, owx, owy)
  local scene = self.owner:get_scene()
  local world = self.owner:get_world()
  local z = scene:GetZoom()*15
  local px, py, ptd = self:NearestVertex(world, wx, wy, z)
  if ptd and ptd > z then
    px, py = nil, nil
  end
  self.sx, self.sy = px, py
end

function AddVertexTool:Redraw(dt)
  local scene = self.owner:get_scene()
  scene:DrawStatus("Insert vertex")
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  local sq = scene:GetZoom()*5
  scene:DrawRectangle(sx, sy, sq, sq, 1, ORANGE, 1)
end

function AddVertexTool:NearestVertex(world, wx, wy)
  --local gstate = self.owner.gstate
  local world = self.owner:get_world()
  --local shapes = gstate:QueryAABB(wx - r, wy - r, wx + r, wy + r)

  local px, py, ptd
  local ps, pi
  for _, b in pairs(world.bodies) do
    local x, y, d, s, i = b:NearestVertex(wx, wy, true)
    if d and (ptd == nil or d < ptd) then
      px, py = x, y
      ptd = d
      ps, pi = s, i
    end
  end
  return px, py, ptd, ps, pi
end

