CircleTool = {}
CircleToolMT = { __index = CircleTool }

setmetatable(CircleTool, { __index = Tool })

function CircleTool:Create(owner)
  local self = Tool:Create(owner, CircleToolMT)
  self.radius = 1
  return self
end

function CircleTool:Destroy()
  self.radius = nil
  Tool.Destroy(self)
end

function CircleTool:Select()
end

function CircleTool:Deselect()
  self.sx = nil
  self.sy = nil
  self.ex = nil
  self.ey = nil
end

function CircleTool:MousePress(wx, wy)
  -- closing the polygon?
  -- snap to grid
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  self.sx = wx
  self.sy = wy
end

function CircleTool:MouseMove(wx, wy, owx, owy)
  -- snap to grid
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  self.ex = wx
  self.ey = wy
end

function CircleTool:MouseRelease(wx, wy)
  local owner = self.owner
  local world = owner:get_world()
  local gstate = owner.gstate
  local scene = owner:get_scene()
  
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  wx, wy = gstate:ToGrid(wx, wy)
  local dx, dy = wx - sx, wy - sy
  local radius = math.sqrt(dx*dx + dy*dy)
  if radius == 0 then
    radius = self.radius
  end
  if radius > b2.linearSlop then
    self.radius = radius
    local b = world:CreateBodyEx("staticBody", sx, sy)
    local def = {}
    def.type = "circle"
    def.radius = radius
    def.localPosition = { x = 0, y = 0 }
    def.density = 0
    def.friction = 0
    def.restitution = 0
    def.isSensor = false
    local s = b:CreateShape(def)
    scene:AddBody(b)
    --gstate:ClearSelection()
    --gstate:AddToSelection({def.localPosition}, {s})
  end
  self.sx = nil
  self.sy = nil
  self.ex = nil
  self.ey = nil
end

function CircleTool:Redraw(dt)
  local owner = self.owner
  local scene = owner:get_scene()
  scene:DrawStatus("Create circle")
  local sx, sy = self.sx, self.sy
  local ex, ey = self.ex, self.ey
  if sx == nil or sy == nil then
    sx, sy = ex, ey
  end
  if ex == nil or ey == nil then
    return
  end
  
  local z = scene:GetZoom()
  local dx, dy = ex - sx, ey - sy
  local radius = math.sqrt(dx*dx + dy*dy)
  if radius == 0 then
    radius = self.radius
  end

  if radius > b2.linearSlop then
    scene:DrawRectangle(sx, sy, 4*z, 4*z, 1, ORANGE, 1)
    scene:DrawCircle(sx, sy, radius, 1, ORANGE, 1)
  end
end

