BoxTool = {}
BoxToolMT = { __index = BoxTool }

setmetatable(BoxTool, { __index = Tool })

function BoxTool:Create(owner)
  local self = Tool:Create(owner, BoxToolMT)
  self.width = 2
  self.height = 1
  return self
end

function BoxTool:Destroy()
  self.width = nil
  self.height = nil
  Tool.Destroy(self)
end

function BoxTool:Select()
end

function BoxTool:Deselect()
  self.sx = nil
  self.sy = nil
  self.ex = nil
  self.ey = nil
end

function BoxTool:MousePress(wx, wy)
  -- closing the polygon?
  -- snap to grid
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  self.sx = wx
  self.sy = wy
end

function BoxTool:MouseMove(wx, wy, owx, owy)
  -- snap to grid
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  self.ex = wx
  self.ey = wy
end

function BoxTool:MouseRelease(wx, wy)
  local owner = self.owner
  local world = owner:get_world()
  local gstate = owner.gstate
  local scene = owner:get_scene()
  
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  local ex, ey = gstate:ToGrid(wx, wy)
  local x, y = (sx + ex)/2,(sy + ey)/2
  local dx, dy = ex - sx, ey - sy
  dx, dy = math.abs(dx), math.abs(dy)
  
  if dx == 0 and dy == 0 then
    dx, dy = self.width, self.height
    x, y = sx + dx/2, sy - dy/2
  end
  if dx > b2.linearSlop and dy > b2.linearSlop then
    self.width = dx
    self.height = dy
    
    local b = world:CreateBodyEx("staticBody", x, y)
    local def = {}
    def.type = "polygon"
    local verts = NewBoxPath(dx/2, dy/2)
    def.vertices = verts
    def.density = 0
    def.friction = 0
    def.restitution = 0
    def.isSensor = false
    local s = b:CreateShape(def)
    scene:AddBody(b)
    --gstate:ClearSelection()
    --gstate:AddToSelection({verts[1], verts[2], verts[3], verts[4]}, {s, s, s, s})
  end
  self.sx = nil
  self.sy = nil
  self.ex = nil
  self.ey = nil
end

function BoxTool:Redraw(dt)
  local owner = self.owner
  local scene = owner:get_scene()
  scene:DrawStatus("Create box")
  local sx, sy = self.sx, self.sy
  local ex, ey = self.ex, self.ey
  if sx == nil or sy == nil then
    sx, sy = ex, ey
  end
  if ex == nil or ey == nil then
    return
  end
  
  local z = scene:GetZoom()
  local x, y = (sx + ex)/2,(sy + ey)/2
  local dx, dy = ex - sx, ey - sy
  dx, dy = math.abs(dx), math.abs(dy)
  if dx == 0 and dy == 0 then
    dx, dy = self.width, self.height
    x, y = sx + dx/2, sy - dy/2
  end
  if dx > b2.linearSlop and dy > b2.linearSlop then
    scene:DrawRectangle(x, y, dx, dy, 1, ORANGE, 1)
  end
end

