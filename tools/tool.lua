Tool = {}
ToolMT = { __index = Tool }

function Tool:Create(owner, mt)
  local self = {}
  setmetatable(self, mt or ToolMT)
  
  self.owner = owner

  return self
end

function Tool:Destroy()
  self.owner = nil
end

function Tool:MouseMove(wx, wy, owx, owy)
end

function Tool:MousePress(wx, wy)
end

function Tool:MouseRelease(wx, wy)
end

function Tool:DoubleClick(wx, wy)
end

function Tool:Select()
end

function Tool:Deselect()
end

function Tool:KeyPress(key, ctr, shift)
end

function Tool:Redraw()
end