ScaleTool = {}
ScaleToolMT = { __index = ScaleTool }

setmetatable(ScaleTool, { __index = Tool })

function ScaleTool:Create(owner)
  local self = Tool:Create(owner, ScaleToolMT)

  return self
end

function ScaleTool:Destroy()
  Tool.Destroy(self)
end

function ScaleTool:Select(selection, sx, sy)
  -- selected resizer
  local gstate = self.owner.gstate
  local rx, ry = gstate:GetResizer(sx, sy)
  -- prevent scaling along the pivot axis
  local wpx, wpy = gstate:GetWorldPivot()
  local lpx, lpy = gstate:WorldToAABB(wpx, wpy)
  if lpx == rx and sx ~= 0 then
    sx = 0
  end
  if lpy == ry and sy ~= 0 then
    sy = 0
  end
  -- store the scaling direction and selected pivot
  self.sx, self.sy = sx, sy
  self.rx, self.ry = rx, ry
  self.selection = selection
end

function ScaleTool:Deselect()
  self.sx, self.sy = nil, nil
  self.rx, self.ry = nil, nil
  self.selection = nil
end

function ScaleTool:MousePress(wx, wy, alt, shift)

end

function ScaleTool:MouseMove(wx, wy, owx, owy)
  local gstate = self.owner.gstate
  -- change in scale
  local owx, owy = gstate:AABBToWorld(self.rx, self.ry)
  local ospx, ospy = gstate:WorldToAABB(owx, owy)
  local spx, spy = gstate:WorldToAABB(wx, wy)

  -- avoid division by zero
  if ospx == 0 then
    ospx = spx
  end
  if ospy == 0 then
    ospy = spy
  end
  local ssx, ssy = spx/ospx, spy/ospy
  -- enforce aspect ratio
  local s = ssx
  if self.sx == 0 then
    if self.sy == 0 then
      return
    end
    s = ssy
  end
  if s == 0 then
    return
  end
  -- move the resizer
  self.rx, self.ry = self.rx*s, self.ry*s

  -- scale AABB
  gstate:ScaleSelection(s)
end

function ScaleTool:MouseRelease(wx, wy)
  self.owner:set_tool(SelectTool)
end

function ScaleTool:Redraw(dt)
  local f = "Scaling selection (%d bodies)"
  local sz = string.format(f, #self.selection)
  local scene = self.owner:get_scene()
  scene:DrawStatus(sz)
end

