--
-- Defines a new polygon
--
PolygonTool = {}
PolygonToolMT = { __index = PolygonTool }

setmetatable(PolygonTool, { __index = Tool })

function PolygonTool:Create(owner)
  local self = Tool:Create(owner, PolygonToolMT)

  return self
end

function PolygonTool:Destroy()
  Tool.Destroy(self)
end

function PolygonTool:Select(type)
  self.type = type
  self.polygon = {}
end

function PolygonTool:Deselect()
  self.type = nil
  self.polygon = nil
  self.sx = nil
  self.sy = nil
end

function PolygonTool:MousePress(wx, wy)
  local polygon = self.polygon
  -- closing the polygon?
  local owner = self.owner
  local scene = owner:get_scene()
  local sq = scene:GetZoom()*5
  local l, t = wx - sq, wy - sq
  local r, b = wx + sq, wy + sq
  if #polygon > 1 then
    local p = polygon[1]
    if l < p.x and r > p.x and t < p.y and b > p.y then
      self:ClosePolygon()
      owner:set_tool(VSelectTool)
      return
    end
  end
  -- snap to grid
  local gstate = owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  -- don't allow duplicate vertices
  for i, v in ipairs(polygon) do
    if v.x == wx and v.y == wy then
      return
    end
  end
  table.insert(polygon, { x = wx, y = wy })
  self.sx = wx
  self.sy = wy
end

function PolygonTool:MouseMove(wx, wy, owx, owy)
  -- snap to grid
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  
  self.sx = wx
  self.sy = wy
end

function PolygonTool:MouseRelease(wx, wy)

end

function PolygonTool:DoubleClick(wx, wy)
  self:ClosePolygon()
  self.owner:set_tool(VSelectTool)
end

function PolygonTool:Redraw(dt)
  local owner = self.owner
  local scene = owner:get_scene()
  scene:DrawStatus("Create " .. self.type)
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  
  local z = scene:GetZoom()
  scene:DrawRectangle(sx, sy, z*5, z*5, 1, ORANGE, 1)
  
  local p = self.polygon
  if #p == 0 then
    return
  end
  
  -- open polygon path
  scene:DrawLine(sx, sy, p[#p].x, p[#p].y, 1, ORANGE, 1)
  scene:DrawRectangle(p[1].x, p[1].y, z*5, z*5, 1, ORANGE, 1)

  for i = #p, 2, -1 do
    local s = p[i]
    local e = p[i - 1]
    scene:DrawLine(s.x, s.y, e.x, e.y, 1, ORANGE, 1)
    scene:DrawRectangle(s.x, s.y, z*5, z*5, 1, ORANGE, 1)
  end
end

function PolygonTool:ClosePolygon()
  local p = self.polygon
  if p == nil or #p < 2 then
    return
  end
  local shape = self:InsertPolygon(p)
  if shape then
    -- select the newly created polygon
    local s = {}
    for i = 1, #p do
      table.insert(s, shape)
    end
    local gstate = self.owner.gstate
    gstate:ClearSelection()
    gstate:AddToSelection(shape.def.vertices, s)
  end
  return shape
end

function PolygonTool:InsertPolygon(path)
  local gstate = self.owner.gstate
  local world = self.owner:get_world()
  local scene = self.owner:get_scene()
  -- find the center
  local cx, cy = path[1].x, path[1].y
  for i = 2, #path do
    cx = cx + path[i].x
    cy = cy + path[i].y
  end
  cx = cx/#path
  cy = cy/#path
  cx, cy = gstate:ToGrid(cx, cy)
  -- convert to local coords
  for i, v in ipairs(path) do
    v.x = v.x - cx
    v.y = v.y - cy
  end
  local b = world:CreateBodyEx("staticBody", cx, cy)
  local def = {}
  def.type = self.type
  def.vertices = path
  def.vertexCount = #path
  def.density = 0
  def.friction = 0
  def.restitution = 0
  def.isSensor = false
  local s = b:CreateShape(def)
  if s == nil then
    world:DestroyBody(b)
    return
  end
  scene:AddBody(b)
  return s
end

