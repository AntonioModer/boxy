--
-- Sets the associated body/bodies of a joint
--
AnchorTool = {}
AnchorToolMT = { __index = AnchorTool }

setmetatable(AnchorTool, { __index = Tool })

function AnchorTool:Create(owner)
  local self = Tool:Create(owner, AnchorToolMT)
  
  return self
end

function AnchorTool:Destroy()
  Tool.Destroy(self)
end

function AnchorTool:Select(wx, wy, joint, body)
  self.anchor = { x = wx, y = wy }
  self.joint = joint
  self.body = body
end

function AnchorTool:Deselect()
  self.anchor = nil
  self.body = nil
  self.joint = nil
end

function AnchorTool:MousePress(wx, wy)

end

function AnchorTool:MouseMove(wx, wy, owx, owy)
  local a = self.anchor
  a.x, a.y = wx, wy
end

function AnchorTool:MouseRelease(wx, wy)
  local gstate = self.owner.gstate
  local scene = self.owner:get_scene()

  local z = scene:GetZoom()
  local a = self.anchor
  local q = gstate:QueryBodies(a.x, a.y, a.x, a.y, 4*z)
  --[[
  if #q > 1 then
    local s = gui.Selection(50, 50, "Select body", "Which body would you like to join?", {"a", "b", "c"})
    screen:add_child(s)
    screen:set_focus(s, true)
  end
  ]]
  if #q > 0 then
    -- todo: overlapping bodies
    local joint = self.joint
    local b1, b2 = "body1", "body2"
    local a1, a2 = joint.def.localAnchor1, joint.def.localAnchor2
    if self.body == joint.def[b2] then
      b1, b2 = b2, b1
      a1, a2 = a2, a1
    end
    if q[1] ~= joint.def[b2] then
      -- link joint to selected body
      local wax, way = joint.def[b1]:GetWorldPoint(a1.x, a1.y)
      local lax, lay = q[1]:GetLocalPoint(wax, way)
      joint.def[b1] = q[1]
      a1.x = lax
      a1.y = lay
    end
  end
  self.owner:set_tool(JSelectTool)
end

function AnchorTool:Redraw(dt)
  local gstate = self.owner.gstate
  local scene = self.owner:get_scene()

  scene:DrawStatus("Moving anchor")
  
  local a = self.anchor
  local joint = self.joint
  local body = self.body
  local b1, b2 = joint.def.body1, joint.def.body2
  local a1, a2 = joint.def.localAnchor1, joint.def.localAnchor2
  if body == b2 then
    b1, b2 = b2, b1
    a1, a2 = a2, a1
  end
  local wvx, wvy = b1:GetWorldPoint(a1.x, a1.y)

  local cm1x, cm1y = a.x, a.y
  local z = scene:GetZoom()
  scene:DrawLine(wvx, wvy, cm1x, cm1y, 1, ORANGE, 1)
  scene:DrawImageS("circle", cm1x, cm1y)
  local ox, oy = -9*z, -9*z
  if b1 == joint.def.body2 then
    ox = -ox
  end
  scene:DrawImageS("anchor", cm1x + ox, cm1y + oy)

  local t = joint:GetType()
  if t ~= 'distance' and t ~= 'prismatic' then
    local cm2x, cm2y = b2:GetWorldCenter()
    --local cm2x, cm2y = b2:GetPosition()
    scene:DrawLine(wvx, wvy, cm2x, cm2y, 1, ORANGE, 1)
    scene:DrawImage("circle", cm2x, cm2y)
    scene:DrawImage("anchor", cm2x - ox, cm2y + oy)
  end

  -- highlight target body
  local q = gstate:QueryBodies(a.x, a.y, a.x, a.y, 4*z)
  if #q == 0 then
    return
  end
  if q[1] == body then
    return
  end
  scene:HighlightBody(q[1])
  --q[1].object.sprite.color = DARKORANGE
end