PivotTool = {}
PivotToolMT = { __index = PivotTool }

setmetatable(PivotTool, { __index = Tool })

function PivotTool:Create(owner)
  local self = Tool:Create(owner, PivotToolMT)

  return self
end

function PivotTool:Destroy()
  Tool.Destroy(self)
end

function PivotTool:MousePress(wx, wy, alt, shift)
  self.sx, self.sy = wx, wy
end

function PivotTool:MouseMove(wx, wy, owx, owy)
  self.owner.gstate:SetWorldPivot(wx, wy, true)
end

function PivotTool:MouseRelease(wx, wy)
  self.sx, self.sy = nil, nil
  self.owner:set_tool(SelectTool)
end

function PivotTool:Redraw(dt)
  local wpx, wpy = self.owner.gstate:GetWorldPivot()
  local f = "Pivot (%f %f)"
  local sz = string.format(f, wpx, wpy)
  local scene = self.owner:get_scene()
  scene:DrawStatus(sz)
end