--
-- Inserts a joint
--
AddJointTool = {}
AddJointToolMT = { __index = AddJointTool }

setmetatable(AddJointTool, { __index = Tool })

function AddJointTool:Create(owner)
  local self = Tool:Create(owner, AddJointToolMT)
  
  return self
end

function AddJointTool:Destroy()
  Tool.Destroy(self)
end

function AddJointTool:Select(mode)
  self.mode = mode
end

function AddJointTool:Deselect()
  self.sx = nil
  self.sy = nil
  self.ex = nil
  self.ey = nil
  self.b1 = nil
  self.b2 = nil
end

function AddJointTool:MousePress(wx, wy)

end

function AddJointTool:MouseRelease(wx, wy)
  local owner = self.owner
  local gstate = owner.gstate
  local scene = owner:get_scene()
  local world = owner:get_world()
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    self.sx, self.sy = wx, wy
    return
  end
  local ex, ey = self.ex, self.ey
  if ex == nil or ey == nil then
    self.ex, self.ey = wx, wy
    return
  end

  local b1, b2 = self.b1, self.b2
  if b1 == nil or b2 == nil then
    return
  end
  
  local j
  local m = self.mode
  if m == "revolute" then
    j = world:CreateJointEx('revolute', b1, b2, false, ex, ey)
  elseif m == "weld" then
    j = world:CreateJointEx('weld', b1, b2, false, ex, ey)
  elseif m == "distance" then
    j = world:CreateJointEx('distance', b1, b2, false, sx, sy, ex, ey)
  elseif m == "rope" then
    j = world:CreateJointEx('rope', b1, b2, false, sx, sy, ex, ey)
  elseif m == "prismatic" then
    local ax, ay = ex - sx, ey - sy
    local ad = math.sqrt(ax*ax + ay*ay)
    if ad == 0 then
      ax, ay = 0, 1
    else
      ax, ay = ax/ad, ay/ad
    end
    j = world:CreateJointEx('prismatic', b1, b2, false, sx, sy, ax, ay)
    --j:SetLimits(0, ad)
    --j:EnableLimit(true)
  end
  
  self.sx = nil
  self.sy = nil
  self.ex = nil
  self.ey = nil
  self.b1 = nil
  self.b2 = nil
  
  if j then
    owner:set_tool(JSelectTool)
    gstate:ClearSelection()
    gstate:AddToSelection({ j.def.localAnchor1 }, { j })
    if m ~= "revolute" and m ~= "weld" then
      gstate:AddToSelection({ j.def.localAnchor2 }, { j })
    end
  end
end

function AddJointTool:MouseMove(wx, wy, owx, owy)
  local world = self.owner:get_world()
  local b1, b2 = self:NearestBodies(world, wx, wy)
  local gstate = self.owner.gstate
  wx, wy = gstate:ToGrid(wx, wy)
  if self.ex == nil or self.ey == nil then
    self.sx, self.sy = wx, wy
    self.b1 = b1
  else
    self.ex, self.ey = wx, wy
    self.b2 = b1
    if self.b1 == b1 then
      self.b2 = b2
    end
    if self.mode == "revolute" or self.mode == "weld" then
      self.ex, self.ey = self.sx, self.sy --wx, wy
    end
  end
end

function AddJointTool:Redraw(dt)
  local scene = self.owner:get_scene()
  local sz = string.format("Create %s joint", self.mode)
  scene:DrawStatus(sz)
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  local z = scene:GetZoom()
  local ox, oy = 9*z, 9*z
  local b1, b2 = self.b1, self.b2
  if b1 then
    local b1x, b1y = b1:GetWorldCenter()
    scene:DrawLine(sx, sy, b1x, b1y, 1, DARKORANGE, 1)
    scene:DrawImageS("circle", b1x, b1y)
    scene:DrawImageS("anchor1", b1x - ox, b1y - oy)
    scene:DrawImageS(self.mode, sx, sy)
    scene:HighlightBody(b1)
  end
  local ex, ey = self.ex, self.ey
  if b2 then
    local b2x, b2y = b2:GetWorldCenter()
    scene:DrawLine(ex, ey, b2x, b2y, 1, DARKORANGE, 1)
    scene:DrawImageS("circle", b2x, b2y)
    scene:DrawImageS("anchor2", b2x + ox, b2y - oy)
    scene:DrawImageS(self.mode, ex, ey)
    scene:HighlightBody(b2)
    scene:DrawLine(sx, sy, ex, ey, 1, DARKORANGE, 1)
  end
end

function AddJointTool:NearestBodies(world, wx, wy)
  local dist1, b1, b1static
  for _, b in pairs(world.bodies) do
    local d = b:GetDistanceTo(wx, wy)
    if dist1 == nil or d < dist1 then
      dist1 = d
      b1 = b
      b1static = b.def.type == "staticBody"
    end
  end
  local dist2, b2
  for _, b in pairs(world.bodies) do
    if b ~= b1 then
      local b2static = b.def.type == "staticBody"
      if b1static == false or b2static == false then
        local d = b:GetDistanceTo(wx, wy)
        if dist2 == nil or d < dist2 then
          dist2 = d
          b2 = b
        end
      end
    end
  end
  return b1, b2
end

