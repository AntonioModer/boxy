--
-- Translates a list of joints
--
JTranslateTool = {}
JTranslateToolMT = { __index = JTranslateTool }

setmetatable(JTranslateTool, { __index = Tool })

function JTranslateTool:Create(owner)
  local self = Tool:Create(owner, JTranslateToolMT)

  return self
end

function JTranslateTool:Destroy()
  Tool.Destroy(self)
end

function JTranslateTool:Select(selection, selection2)
  self.selection = selection
  self.selection2 = selection2
end

function JTranslateTool:Deselect()
  self.selection = nil
  self.selection2 = nil
end

function JTranslateTool:MousePress(wx, wy)

end

function JTranslateTool:MouseMove(wx, wy, owx, owy)
  local gstate = self.owner.gstate
  -- todo snap to grid
  -- todo prevent from moving outside of world bounds
  --local dx, dy = wx - owx, wy - owy
  local s, s2 = gstate.selection, gstate.selection2

  -- snap to grid
  local b = s2[1].def.body1
  if s[1] == s2[1].def.localAnchor2 then
    b = s2[1].def.body2
  end
  local wvx, wvy = s[1].x, s[1].y
  if s[1] == s2[1].def.localAnchor1 or s[1] == s2[1].def.localAnchor2 then
    wvx, wvy = b:GetWorldPoint(s[1].x, s[1].y)
  end
  wx, wy = gstate:ToGrid(wx, wy)
  owx, owy = wx - (wx - wvx), wy - (wy - wvy)
  local wdx, wdy = wx - owx, wy - owy
  if wdx == 0 and wdy == 0 then
    return
  end
  
  --local level = self.owner:get_level()
  gstate:TranslateAnchors(s, s2, wdx, wdy)
end

function JTranslateTool:MouseRelease(wx, wy)
  self.owner:set_tool(JSelectTool)
end

function JTranslateTool:Redraw(dt)
  local scene = self.owner:get_scene()
  local sz = "Moving selection (%d joints)"
  sz = string.format(sz, #self.owner.gstate.selection)
  scene:DrawStatus(sz)
end