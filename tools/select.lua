--
-- Selects bodies
--
SelectTool = {}
SelectToolMT = { __index = SelectTool }

setmetatable(SelectTool, { __index = Tool })

function SelectTool:Create(owner)
  local self = Tool:Create(owner, SelectToolMT)

  return self
end

function SelectTool:Destroy()
  Tool.Destroy(self)
end

function SelectTool:MousePress(wx, wy, alt, shift, ctrl)
  local owner = self.owner
  local world = owner:get_world()
  local camera = owner:get_camera()
  local scale = camera.scalex*4
  local sq = scale
  local gstate = owner.gstate
  local selection = gstate.selection

  if alt == false and shift == false then
    if gstate:TestPivot(wx, wy, sq) == true then
      owner:set_tool(PivotTool)
      return
    end
    local sx, sy = gstate:TestResizers(wx, wy, sq)
    if sx and sy then
      -- set the pivot opposite to the selected resizer
      if gstate.custompivot ~= true then
        local px, py = gstate:GetResizer(-sx, -sy)
        gstate:SetAABBPivot(px, py, false)
      end
      owner:set_tool(ScaleTool, selection, sx, sy)
      return
    end
    if gstate:TestRotator(wx, wy, sq) == true then
      local wpx, wpy = gstate:GetWorldPivot()
      owner:set_tool(RotateTool, selection, wpx, wpy)
      owner.tool:MousePress(wx, wy, alt, shift, ctrl)
      return
    end
    -- clicked inside the selection AABB?
    if gstate:TestAABB(wx, wy) == true then
      owner:set_tool(TranslateTool, selection)
      owner.tool:MousePress(wx, wy, alt, shift, ctrl)
      return
    end
  end

  local q = gstate:QueryBodies(wx, wy, wx, wy, sq)
  if #q > 0 and alt == false and shift == false then
    local n = q[1]
    -- selection of overlapping bodies/shapes
    if #selection == 1 and ctrl == true then
      local i = table.find(q, selection[1])
      if i then
        i = i - 1
        if i == 0 then
          i = #q
        end
        n = q[i]
      end
    end
    -- select and translate body
    local i = table.find(selection, n)
    if i == nil then
      -- clicked on a non-selected body
      gstate:ClearSelection()
      gstate:ClearAABB()
      gstate:AddToSelection({ n })
    else
      -- clicked on a selected body
      if ctrl == false then
        gstate:AABBFromSelection()
      else
        return
      end
    end
    owner:set_tool(TranslateTool, selection)
    owner.tool:MousePress(wx, wy, alt, shift, ctrl)
  else
    -- begin new selection
    gstate:ClearAABB()
    
    self.alt, self.shift = alt, shift
    self.sx, self.sy = wx, wy
    -- modify or clear selection?
    local gstate = owner.gstate
    if alt == true or shift == true then
      self.oldselect = { unpack(selection) }
    else
      gstate:ClearSelection()
    end
  end
end

function SelectTool:MouseMove(wx, wy, owx, owy)
  local sx, sy = self.sx, self.sy
  if sx == nil or sy == nil then
    return
  end
  local gstate = self.owner.gstate
  -- temporarily modify selection
  local camera = self.owner:get_camera()
  local scale = camera.scalex*4
  local sq = scale
  local q = gstate:QueryBodies(sx, sy, wx, wy, sq)
  gstate:ClearSelection()
  if self.oldselect then
    gstate:AddToSelection(self.oldselect)
  end
  if self.alt == true then
    gstate:RemoveFromSelection(q)
  else
    gstate:AddToSelection(q)
  end
  self.ex, self.ey = wx, wy
end

function SelectTool:MouseRelease(wx, wy)
  -- complete selection
  self:MouseMove(wx, wy, wx, wy)
  self.oldselect = nil
  self.alt, self.shift = nil, nil
  self.sx, self.sy = nil, nil
  self.ex, self.ey = nil, nil
end

function SelectTool:Redraw(dt)
  local owner = self.owner
  local scene = owner:get_scene()
  local gstate = owner.gstate
  local camera = owner:get_camera()
  if self.ex and self.ey then
    local sx, sy = self.sx, self.sy
    local ex, ey = self.ex, self.ey
    scene:DrawLine(sx, sy, sx, ey, 1, WHITE, 1)
    scene:DrawLine(sx, ey, ex, ey, 1, WHITE, 1)
    scene:DrawLine(ex, ey, ex, sy, 1, WHITE, 1)
    scene:DrawLine(ex, sy, sx, sy, 1, WHITE, 1)
    scene:DrawStatus("Selecting bodies (shift/alt to modify)")
    return
  end
  local sq = camera.scalex*4
  local mx, my = mouse.xaxis, mouse.yaxis
  local wx, wy = camera:get_world_point(mx, my)
  if #gstate.selection > 0 then
    local f = "%d bodies selected"
    local sz = string.format(f, #gstate.selection)
    scene:DrawStatus(sz)
    
    if gstate:TestPivot(wx, wy, sq) == true then
      scene:DrawTooltip("Pivot", wx, wy)
      return
    end
    if gstate:TestRotator(wx, wy, sq) == true then
      scene:DrawTooltip("Rotate", wx, wy)
      return
    end
    local sx, sy = gstate:TestResizers(wx, wy, sq)
    if sx and sy then
      scene:DrawTooltip("Resize", wx, wy)
      return
    end
    if gstate:TestAABB(wx, wy, sq) == true then
      scene:DrawTooltip("Move", wx, wy)
      return
    end
  else
    scene:DrawStatus("Edit bodies")
    
    -- show hover query
    local q = gstate:QueryBodies(wx, wy, wx, wy, sq)
    local body = q[1]
    if body then
      local px, py = body:GetPosition()
      local a = body:GetAngle()
      --local mass = body:GetMass()
      local s = body.def.scale or 1
      local t = body.def.type or "?"
      local m = body:GetMass()
      local f = "Body \n id:%d \n type:%s \n x:%g \n y:%g \n angle:%g \n scale:%g \n mass:%g"
      local sz = string.format(f, body.id, t, px, py, a, s, m)
      scene:DrawTooltip(sz, wx, wy)
    end
  end
end
