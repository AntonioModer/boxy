--
-- Rotates selected bodies
--
RotateTool = {}
RotateToolMT = { __index = RotateTool }

setmetatable(RotateTool, { __index = Tool })

function RotateTool:Create(owner)
  local self = Tool:Create(owner, RotateToolMT)

  return self
end

function RotateTool:Destroy()
  Tool.Destroy(self)
end

function RotateTool:Select(selection, wpx, wpy)
  self.selection = selection
  self.wpx, self.wpy = wpx, wpy
end

function RotateTool:Deselect()
  self.selection = nil
  self.wpx, self.wpy = nil, nil
end

function RotateTool:MousePress(wx, wy, alt, shift)
  self.sx, self.sy = wx, wy
end

function RotateTool:MouseMove(wx, wy, owx, owy)
  if self.sx == nil or self.sy == nil then
    return
  end
  -- relative change in rotation
  local wpx, wpy = self.wpx, self.wpy
  local a1 = math.atan2(wpx - wx, wpy - wy)
  local a2 = math.atan2(wpx - owx, wpy - owy)
  local da = a2 - a1

  -- rotate AABB
  self.owner.gstate:RotateSelection(da)
end

function RotateTool:MouseRelease(wx, wy)
  self.sx, self.sy = nil, nil
  self.owner:set_tool(SelectTool)
end

function RotateTool:Redraw(dt)
  local f = "Rotating selection (%d bodies)"
  local sz = string.format(f, #self.selection)
  local scene = self.owner:get_scene()
  scene:DrawStatus(sz)
end
