local Crank = {}
local CrankMT = { __index = Crank }

setmetatable(Crank, { __index = Model })

Crank.params = { rotation = 2, torque = 300 }

function Crank:Create(world)
  local self = {}
  setmetatable(self, CrankMT)

  self.world = world

  return self
end

function Crank:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Crank:Init(sx, sy, ex, ey)
  if ex < sx then
    sx, ex = ex, sx
  end
  if ey > sy then
    sy, ey = ey, sy
  end
  local world = self.world
  local dx, dy = ex - sx, ey - sy
  sx = sx + dx/2
  local w, h = dx/2, -dy

  local d = math.abs(dy)
  local a = 0 --math.atan2(dx, -dy)
  local b = world:CreateBodyEx("dynamicBody", sx, sy)
  local dg = math.min(w/5, h/5)
  dg = math.max(dg, b2.linearSlop*5)
  --local v = { b2Vec2(-w, 0), b2Vec2(w, 0), b2Vec2(w, -h + dg), b2Vec2(w - dg, -h), b2Vec2(-w + dg, -h), b2Vec2(-w, -h + dg) }
  local v = NewPath(-w, 0, w, 0, w, -h + dg, w - dg, -h, -w + dg, -h, -w, -h + dg)
  b:CreateShapeEx('concave', v, 1, 1, 0)
  local base = world:CreateBodyEx("staticBody", sx, sy)

  local axisx, axisy = 0, -dy -- -dx, -dy
  local axisd = math.sqrt(axisx*axisx + axisy*axisy)
  axisx, axisy = axisx/axisd, axisy/axisd
  local j = world:CreateJointEx('prismatic', base, b, false, sx, sy, axisx, axisy)
  j:EnableLimit(true)
  j:SetLimits(-d, 0)

  local ptx, pty = 0, -h
  ptx = ptx + sx
  pty = pty + sy
  local b2 = world:CreateBodyEx("dynamicBody", ptx, pty, a)
  --local v = { b2Vec2(-dg, dg), b2Vec2(-dg, -d), b2Vec2(dg, -d), b2Vec2(dg, dg) }
  local v = NewPath(-dg, dg, -dg, -d, dg, -d, dg, dg)
  b2:CreateShapeEx('concave', v, 1, 1, 0, false)
  world:CreateJointEx('revolute', b, b2, false, ptx, pty)

  local hd = d/2
  local ptx, pty = 0, -h - d
  ptx = ptx + sx
  pty = pty + sy
  local b3 = world:CreateBodyEx("dynamicBody", ptx, pty, a)
  --local v = { b2Vec2(-dg, dg), b2Vec2(-dg, -hd), b2Vec2(dg, -hd), b2Vec2(dg, dg) }
  local v = NewPath(-dg, dg, -dg, -hd, dg, -hd, dg, dg)
  b3:CreateShapeEx('concave', v, 1, 1, 0, false)
  world:CreateJointEx('revolute', b2, b3, false, ptx, pty)
  local ptx, pty = 0, -h - d - hd
  --ptx, pty = RotatePoint(ptx, pty, a)
  ptx = ptx + sx
  pty = pty + sy
  local j = world:CreateJointEx('revolute', base, b3, false, ptx, pty)
  
  local params = self.params
  j:EnableMotor(true)
  j:SetMotorSpeed(params.rotation)
  j:SetMaxMotorTorque(params.torque)
  
  return { base, b, b2, b3 }
end

function Crank:Preview(sx, sy, ex, ey, scale)
  if ex < sx then
    sx, ex = ex, sx
  end
  if ey > sy then
    sy, ey = ey, sy
  end
  local dx, dy = ex - sx, ey - sy
  sx = sx + dx/2
  local w, h = dx/2, -dy
  --local w, h = math.abs(dx)/ 2, math.abs(dy)
  local dg = math.min(w/5, h/5)
  dg = math.max(dg, b2.linearSlop*5)
  local d = math.abs(dy)--math.sqrt(dx*dx + dy*dy)
  local hd = d/2
  
  local x, y = sx, sy
  scene:DrawLine(x - w, y, x + w, y, 1, ORANGE, 1)
  scene:DrawLine(x + w, y, x + w, y - h + dg, 1, ORANGE, 1)
  scene:DrawLine(x + w, y - h + dg, x + w - dg, y - h, 1, ORANGE, 1)
  scene:DrawLine(x + w - dg, y - h, x - w + dg, y - h, 1, ORANGE, 1)
  scene:DrawLine(x - w + dg, y - h, x - w, y - h + dg, 1, ORANGE, 1)
  scene:DrawLine(x - w, y - h + dg, x - w, y, 1, ORANGE, 1)
  local x, y = sx, sy - h
  scene:DrawLine(x - dg, y + dg, x - dg, y - d, 1, ORANGE, 1)
  scene:DrawLine(x - dg, y - d, x + dg, y - d, 1, ORANGE, 1)
  scene:DrawLine(x + dg, y - d, x + dg, y + dg, 1, ORANGE, 1)
  scene:DrawLine(x + dg, y + dg, x - dg, y + dg, 1, ORANGE, 1)
  local x, y = sx, sy - h - d
  scene:DrawLine(x - dg, y + dg, x - dg, y - hd, 1, ORANGE, 1)
  scene:DrawLine(x - dg, y - hd, x + dg, y - hd, 1, ORANGE, 1)
  scene:DrawLine(x + dg, y - hd, x + dg, y + dg, 1, ORANGE, 1)
  scene:DrawLine(x + dg, y + dg, x - dg, y + dg, 1, ORANGE, 1)
end

return Crank