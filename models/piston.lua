local Piston = {}
local PistonMT = { __index = Piston }

setmetatable(Piston, { __index = Model })

Piston.params = { width = 0.25, height = 0.25, speed = 5, force = 10 }

function Piston:Create(world)
  local self = {}
  setmetatable(self, PistonMT)
  
  self.world = world

  return self
end

function Piston:Destroy()
  self.world = nil
end

function Piston:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  local w, h = math.abs(dx)/2, math.abs(dy)/2

  local world = self.world
  local base = world:CreateBodyEx("staticBody", x, y)

  local platform = world:CreateBodyEx("dynamicBody", x, y, 0, 0, 0, true, true, false, false, PistonOMT)
  local box = NewBoxPath(w, h)
  platform:CreateShapeEx('polygon', box, 10, 1, 0, false)

  local s, f = params.speed, params.force*platform:GetMass()
  local p = world:CreateJointEx('prismatic', base, platform, false, x, y, 0, 1)
  p:EnableMotor(true)
  p:SetMotorSpeed(-s)
  p:SetMaxMotorForce(f)
  p:EnableLimit(true)
  p:SetLimits(0, -math.abs(dy))

  return { base, platform }
end

function Piston:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  if sx == ex and sy == ey then
    local params = self.params
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2
  
  scene:DrawRectangle(x, y, dx, dy, 1, ORANGE, 1)
end

return Piston