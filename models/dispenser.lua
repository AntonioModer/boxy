local Dispenser = {}
local DispenserMT = { __index = Dispenser }

setmetatable(Dispenser, { __index = Model })

Dispenser.params = { width = 0.2, height = 0.2 }

function Dispenser:Create(world)
  local self = {}
  setmetatable(self, DispenserMT)
  
  self.world = world

  return self
end

function Dispenser:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Dispenser:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  local w, h = math.abs(dx)/2, math.abs(dy)/2
  local pad = math.min(w/8, h/8)
  local pw, ph = w - pad*2, h - pad

  local world = self.world
  local base = world:CreateBodyEx("staticBody", x, y)
  local box = NewBoxPath(pw, pad, 0, ph)
  local box2 = NewBoxPath(pad, h, pw + pad, 0)
  local box3 = NewBoxPath(pad, h, -pw - pad, 0)
  base:CreateShapeEx('polygon', box, 0, 1, 0, false)
  base:CreateShapeEx('polygon', box2, 0, 1, 0, false)
  base:CreateShapeEx('polygon', box3, 0, 1, 0, false)

  return { base }
end

function Dispenser:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  if sx == ex and sy == ey then
    local params = self.params
    dx, dy = params.width, params.height
  end
  local x, y = sx + dx/2, sy + dy/2

  scene:DrawRectangle(x, y, dx, dy, 1, ORANGE, 1)
end

return Dispenser