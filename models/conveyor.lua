Conveyor = {}
ConveyorMT = { __index = Conveyor }

setmetatable(Conveyor, { __index = Model })

Conveyor.params = { radius = 0.1, rotation = 2, torque = 200 }

function Conveyor:Create(world)
  local self = {}
  setmetatable(self, ConveyorMT)

  self.world = world

  return self
end

function Conveyor:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Conveyor:Init(sx, sy, ex, ey)
  if sx == ex and sy == ey then
    --ex = ex + b2.linearSlop
  end
  
  local params = self.params
  local radius = params.radius
  if radius == 0 then
    return {}
  end
  local r, t = params.rotation, params.torque
  local world = self.world
  
  local bodies = {}

  -- conveyor gears
  local gears, joints = self:CreateGears(bodies, sx, sy, ex, ey, r, t)
  -- conveyor bands
  local ulx, uly = gears[1]:GetWorldPoint(-radius, radius)
  local blx, bly = gears[1]:GetWorldPoint(-radius, -radius)
  local urx, ury = gears[#gears]:GetWorldPoint(radius, radius)
  local brx, bry = gears[#gears]:GetWorldPoint(radius, -radius)
  local b1 = self:CreateBands(bodies, ulx, uly, urx, ury, radius)
  local b2 = self:CreateBands(bodies, urx, ury, brx, bry, radius)
  local b3 = self:CreateBands(bodies, brx, bry, blx , bly, radius)
  local b4 = self:CreateBands(bodies, blx , bly, ulx, uly, radius)
  assert(#b1 > 0 and #b2 > 0 and #b3 > 0 and #b4 > 0)
  -- link the bands in a loop
  local a1x, a1y = b2[1]:GetPosition()
  world:CreateJointEx('revolute', b2[1], b1[#b1], true, a1x, a1y)
  local a2x, a2y = b3[1]:GetPosition()
  world:CreateJointEx('revolute', b3[1], b2[#b2], true, a2x, a2y)
  local a3x, a3y = b4[1]:GetPosition()
  world:CreateJointEx('revolute', b4[1], b3[#b3], true, a3x, a3y)
  local a4x, a4y = b1[1]:GetPosition()
  world:CreateJointEx('revolute', b1[1], b4[#b4], true, a4x, a4y)

  return bodies
end

function Conveyor:CreateGears(bodies, sx, sy, ex, ey, r, t)
  local radius = self.params.radius
  local world = self.world
  local dx, dy = ex - sx, ey - sy
  local w = radius*2
  local d = math.sqrt(dx*dx + dy*dy)
  d = math.max(d, w)
  --local c = d/2/w
  local c = (d - w*0.1)/2/w
  assert(c ~= 0)
  dx, dy = dx/c, dy/c
  c = math.floor(c)
  c = math.abs(c)

  local a = -math.atan2(dx, dy) + math.pi/2

  -- base
  local bx = (c/2)*dx + sx
  local by = (c/2)*dy + sy
  local base = world:CreateBodyEx("staticBody", bx, by, a)
  local bw = c*w + w/2 + w*0.1/2
  local box = NewBoxPath(bw, radius)
  base:CreateShapeEx('polygon', box, 0, 0, 0, true)
  table.insert(bodies, base)
  
  local gears = {}
  local joints = {}
  for i = 0, c do
    local x, y = i*dx + sx, i*dy + sy
    local b = world:CreateBodyEx("dynamicBody", x, y, a)
    b:CreateShapeEx('circle', radius, 1, 1, 0)
    local j = world:CreateJointEx('revolute', b, base, false, x, y)
    j:EnableMotor(true)
    j:SetMotorSpeed(r)
    j:SetMaxMotorTorque(t)
    table.insert(gears, b)
    table.insert(joints, j)
    table.insert(bodies, b)
  end
  return gears, joints
end

function Conveyor:CreateBands(bodies, sx, sy, ex, ey, radius)
  local world = self.world
  local dx, dy = ex - sx, ey - sy
  local w = radius*2
  --local c = math.sqrt(dx*dx + dy*dy - w*0.1)/w
  local d = math.sqrt(dx*dx + dy*dy)
  d = math.max(d, w)
  local c = (d - w*0.1)/w
  assert(c ~= 0)
  dx, dy = dx/c, dy/c
  c = math.floor(c)
  c = math.abs(c)
  
  local a = -math.atan2(dx, dy) + math.pi/2
  local bands = {}
  for i = 0, c do
    local x, y = i*dx + sx, i*dy + sy
    --local v = { b2Vec2(w*0.1, 0), b2Vec2(w*0.9, 0), b2Vec2(w, w/2), b2Vec2(0, w/2) }
    local v = NewPath(w*0.1, 0, w*0.9, 0, w, w/2, 0, w/2)
    local b = world:CreateBodyEx("dynamicBody", x, y, a)
    b:CreateShapeEx('polygon', v, 1, 1, 0)
    table.insert(bands, b)
    table.insert(bodies, b)
  end
  for i = 2, #bands do
    local l, n = bands[i - 1], bands[i]
    local ax, ay = n:GetPosition()
    world:CreateJointEx('revolute', l, n, true, ax, ay)
  end
  return bands
end

function Conveyor:Preview(sx, sy, ex, ey, scale)
  if sx == ex and sy == ey then
    --ex = ex + b2.linearSlop
  end

  local r = self.params.radius
  if r == 0 then
    return
  end

  local dx, dy = ex - sx, ey - sy
  local w = r*2
  local d = math.sqrt(dx*dx + dy*dy)
  d = math.max(d, w)
  local c = (d - w*0.1)/2/w
  assert(c ~= 0)
  dx, dy = dx/c, dy/c
  c = math.floor(c)
  c = math.abs(c)
  local a = -math.atan2(dx, dy) + math.pi/2
  for i = 0, c do
    local x, y = i*dx + sx, i*dy + sy
    scene:DrawCircle(x, y, r, 1, ORANGE, 1)
  end
  local a2 = math.atan2(dx, dy)
  local bx = (c/2)*dx + sx
  local by = (c/2)*dy + sy
  scene:DrawRRectangle(bx, by, r, c*w + w, -a2, 1, ORANGE, 1)
  scene:DrawRRectangle(bx, by, r + w/2, c*w + w/2, -a2, 1, ORANGE, 1)
end

return Conveyor