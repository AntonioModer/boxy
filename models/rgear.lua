local RGear = {}
local RGearMT = { __index = RGear }

setmetatable(RGear, { __index = Model })

RGear.params = { radius = 0.25, circle = 0.5, segments = 10, gap = 1 }

function RGear:Create(world)
  local self = {}
  setmetatable(self, RGearMT)
  
  self.world = world

  return self
end

function RGear:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function RGear:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local r = math.sqrt(dx*dx + dy*dy)

  local world = self.world
  --local base = world:CreateBodyEx("staticBody", sx, sy)

  local platform = world:CreateBodyEx("dynamicBody", sx, sy, 0, 0, 2)

  local bodies = {}
  local s = params.segments
  local g = params.gap
  for i = 0, s - g - 1 do
    local offset = math.rad((360/s/2))*g + math.rad(90)
    local a1 = math.rad(i/s*360) + offset
    local a2 = math.rad((i + 1)/s*360) + offset
    local x1 = math.cos(a2)*r
    local y1 = math.sin(a2)*r
    local x2 = math.cos(a1)*r
    local y2 = math.sin(a1)*r
    local seg = NewPath(x1, y1, x2, y2, 0, 0)
    platform:CreateShapeEx('concave', seg, 1, 1, 0, false)
  end
  local c = r*params.circle
  c = math.min(c, r)
  if c > 0 then
    platform:CreateShapeEx('circle', c, 1, 1, 0, false)
  end
  --table.insert(bodies, base)
  table.insert(bodies, platform)
--[[
  local s, f = params.speed, params.torque*platform:GetMass()
  local p = world:CreateJointEx('revolute', base, platform, false, sx, sy)
  --p:EnableMotor(true)
  p:SetMotorSpeed(-s)
  p:SetMaxMotorTorque(f)
  ]]
  return bodies
end

function RGear:Circle(sx, sy, r, s, g)
  for i = 0, s - g - 1 do
    local offset = math.rad((360/s/2))*g + math.rad(90)
    local j = i/s*(math.pi*2) + offset
    local ax, ay = math.cos(j)*r, math.sin(j)*r
    local k = (i + 1)/s*(math.pi*2) + offset
    local bx, by = math.cos(k)*r, math.sin(k)*r
    scene:DrawLine(sx + ax, sy + ay, sx + bx, sy + by, 1, ORANGE, 1)
    scene:DrawLine(sx + ax, sy + ay, sx, sy, 1, ORANGE, 1)
    scene:DrawLine(sx + bx, sy + by, sx, sy, 1, ORANGE, 1)
  end
end

function RGear:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local r = math.sqrt(dx*dx + dy*dy)
  local s = params.segments
  local g = params.gap
  self:Circle(sx, sy, r, s, g)
  local c = r*params.circle
  c = math.min(c, r)
  if c > 0 then
    scene:DrawCircle(sx, sy, c, 1, ORANGE, 1)
  end
end

return RGear