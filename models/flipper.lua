local Flipper = {}
local FlipperMT = { __index = Flipper }

setmetatable(Flipper, { __index = Model })

Flipper.params = { base = 0.1, tip = 0.05 }

function Flipper:Create(world)
  local self = {}
  setmetatable(self, FlipperMT)
  
  self.world = world

  return self
end

function Flipper:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Flipper:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if dx < b2.linearSlop and dy < b2.linearSlop then
  end
  
  local a = math.atan2(dy, dx)
  local len = math.sqrt(dx*dx + dy*dy)
  local params = self.params

  local world = self.world
  local base = world:CreateBodyEx("staticBody", sx, sy, a)

  --local polygon = { b2Vec2(0, -params.base), b2Vec2(len, -params.tip), b2Vec2(len, params.tip), b2Vec2(0, params.base) }
  local polygon = NewPath(0, -params.base, len, -params.tip, len, params.tip, 0, params.base)
  local platform = world:CreateBodyEx("dynamicBody", sx, sy, a, 0, 0, false, true)
  platform:CreateShapeEx('circle', params.base, 0, 1, 0, false)
  platform:CreateShapeEx('polygon', polygon, 10, 1, 0)
  platform:CreateShapeEx('circle', params.tip, 10, 1, 0, false, len, 0)

  local p = world:CreateJointEx('revolute', base, platform, false, sx, sy)
  p:EnableLimit(true)
  p:SetLimits(0, math.pi/2)
  p:EnableMotor(true)
  p:SetMotorSpeed(-1)
  p:SetMaxMotorTorque(10)

  return { base, base2, platform }
end

function Flipper:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  local a = math.atan2(dy, dx) + math.pi/2
  local ca = math.cos(a)
  local sa = math.sin(a)
  
  local params = self.params
  local base = params.base
  local tip = params.tip

  scene:DrawCircle(sx, sy, base, 1, ORANGE, 1)
  scene:DrawCircle(ex, ey, tip, 1, ORANGE, 1)
  scene:DrawLine(sx + ca*base, sy + sa*base, ex + ca*tip, ey + sa*tip, 1, ORANGE, 1)
  scene:DrawLine(sx - ca*base, sy - sa*base, ex - ca*tip, ey - sa*tip, 1, ORANGE, 1)
end

return Flipper