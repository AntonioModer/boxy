local Ring = {}
local RingMT = { __index = Ring }

setmetatable(Ring, { __index = Model })

Ring.params = { radius = 0.25, thickness = 0.2, segments = 20 }

function Ring:Create(world)
  local self = {}
  setmetatable(self, RingMT)
  
  self.world = world

  return self
end

function Ring:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Ring:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local r = math.sqrt(dx*dx + dy*dy)
  local h = r*params.thickness
  --r = math.max(r, h)
  local circ = r*math.pi*2
  local w = circ/params.segments
  --local extra = (circ - params.segments*w)/params.segments

  local world = self.world
  --local base = world:CreateBodyEx("staticBody", sx, sy)

  local platform = world:CreateBodyEx("dynamicBody", sx, sy, 0, 0, 0)

  local bodies = {}
  local r1, r2 = r - h, r
  for i = 1, params.segments do
    local a1 = math.rad(i/params.segments*360)
    local x1 = math.cos(a1)*r1
    local y1 = math.sin(a1)*r1
    local a2 = math.rad((i + 1)/params.segments*360)
    local x2 = math.cos(a2)*r1
    local y2 = math.sin(a2)*r1
    local x3 = math.cos(a2)*r2
    local y3 = math.sin(a2)*r2
    local x4 = math.cos(a1)*r2
    local y4 = math.sin(a1)*r2
    local seg = NewPath(x1, y1, x2, y2, x3, y3, x4, y4)
    --local seg = NewBoxPath(w/2, h/2, x, y, a - math.pi/2)
    platform:CreateShapeEx('concave', seg, 1, 1, 0, false)
  end
  --table.insert(bodies, base)
  table.insert(bodies, platform)
--[[
  local s, f = params.speed, params.torque*platform:GetMass()
  local p = world:CreateJointEx('revolute', base, platform, false, sx, sy)
  p:EnableMotor(true)
  p:SetMotorSpeed(-s)
  p:SetMaxMotorTorque(f)
  ]]
  return bodies
end

function Ring:Circle(sx, sy, r, s)
  for i = 1, s do
    local j = i/s*(math.pi*2)
    local ax, ay = math.cos(j)*r, math.sin(j)*r
    local k = (i + 1)/s*(math.pi*2)
    local bx, by = math.cos(k)*r, math.sin(k)*r
    scene:DrawLine(sx + ax, sy + ay, sx + bx, sy + by, 1, ORANGE, 1)
  end
end

function Ring:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local r = math.sqrt(dx*dx + dy*dy)
  local t = r*params.thickness
  --r = math.max(r, t)
  local s = params.segments
  self:Circle(sx, sy, r, s)
  self:Circle(sx, sy, r - t, s)
  --scene:DrawCircle(sx, sy, r, 1, ORANGE, 1)
  --scene:DrawCircle(sx, sy, r - params.thickness, 1, ORANGE, 1)
end

return Ring