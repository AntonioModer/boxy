local Gear = {}
local GearMT = { __index = Gear }

setmetatable(Gear, { __index = Model })

Gear.params = { radius = 0.25, gears = 5, spacing = 0.2, circle = 0.8, speed = 5, torque = 10 }

function Gear:Create(world)
  local self = {}
  setmetatable(self, GearMT)
  
  self.world = world

  return self
end

function Gear:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Gear:Init(sx, sy, ex, ey)
  local dx, dy = ex - sx, ey - sy
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local w, h = math.abs(dx), math.abs(dy)
  local r = math.sqrt(w*w + h*h)
  if r == 0 then
    return
  end

  local world = self.world
  local base = world:CreateBodyEx("staticBody", sx, sy)

  local c = params.circle
  local platform = world:CreateBodyEx("dynamicBody", sx, sy, 0, 0, 0)
  platform:CreateShapeEx('circle', r*c, 10, 1, 0, false)

  local s, f = params.speed, params.torque*platform:GetMass()
  local p = world:CreateJointEx('revolute', base, platform, false, sx, sy)
  p:EnableMotor(true)
  p:SetMotorSpeed(-s)
  p:SetMaxMotorTorque(f)
  
  local g = params.gears
  local t = r*math.pi/(g*2)*params.spacing
  t = math.min(t, r*c)
  for i = 1, g do
    local a = math.rad(i/g*180)
    local tooth = NewBoxPath(r, t, 0, 0, a)
    platform:CreateShapeEx('polygon', tooth, 10, 1, 0, false)
  end

  return { base, platform }
end

function Gear:Preview(sx, sy, ex, ey, scale)
  local dx, dy = ex - sx, ey - sy
  
  local params = self.params
  if sx == ex and sy == ey then
    dx, dy = params.radius, params.radius
  end

  local w, h = math.abs(dx), math.abs(dy)
  local r = math.sqrt(w*w + h*h)
  if r == 0 then
    return
  end
  
  local c = params.circle
  local g = params.gears
  local t = r*math.pi/(g*2)*params.spacing
  t = math.min(t, r*c)
  for i = 1, params.gears do
    local a = math.rad(i/g*180)
    scene:DrawRRectangle(sx, sy, r, t, a, 1, ORANGE, 1)
  end
  scene:DrawCircle(sx, sy, r*c, 1, ORANGE, 1)
end

return Gear