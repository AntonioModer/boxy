Chain = {}
ChainMT = { __index = Chain }

setmetatable(Chain, { __index = Model })

Chain.params = { radius = 0.1, rotation = 2, torque = 200, links = 10, lradius = 0.25 }

function Chain:Create(world)
  local self = {}
  setmetatable(self, ChainMT)

  self.world = world

  return self
end

function Chain:Destroy()
  self.world = nil
  Model.Destroy(self)
end

function Chain:Init(sx, sy, ex, ey)
  if sx == ex and sy == ey then
    --ex = ex + b2.linearSlop
  end
  
  local params = self.params
  local radius = params.radius
  if radius == 0 then
    return {}
  end
  local r, t = params.rotation, params.torque
  local world = self.world
  local links = params.links
  
  local bodies = {}

  local dx, dy = ex - sx, ey - sy
  local a = -math.atan2(dx, dy) + math.pi/2
  local radius = self.params.radius
  local lradius = radius*self.params.lradius
  
  local bx = dx/2 + sx
  local by = dy/2 + sy
  local base = world:CreateBodyEx("staticBody", bx, by, a)
  table.insert(bodies, base)
  
  -- Chain gears
  local gear1 = world:CreateBodyEx("dynamicBody", sx, sy, a)
  local gear2 = world:CreateBodyEx("dynamicBody", ex, ey, a)
  gear1:CreateShapeEx('circle', radius, 1, 1, 0)
  gear2:CreateShapeEx('circle', radius, 1, 1, 0)
  table.insert(bodies, gear1)
  table.insert(bodies, gear2)
  local j1 = world:CreateJointEx('revolute', gear1, base, false, sx, sy)
  j1:EnableMotor(true) j1:SetMotorSpeed(r) j1:SetMaxMotorTorque(t)
  local j2 = world:CreateJointEx('revolute', gear2, base, false, ex, ey)
  j2:EnableMotor(true) j2:SetMotorSpeed(r) j2:SetMaxMotorTorque(t)
  
  --local len = dx*2 + (math.pi*radius*2)
  --local linkd = len/links
  
  -- Chain bands
  local radii = radius -- + lradius
  local ulx, uly = gear1:GetWorldPoint(0, radii)
  local blx, bly = gear1:GetWorldPoint(0, -radii)
  local urx, ury = gear2:GetWorldPoint(0, radii)
  local brx, bry = gear2:GetWorldPoint(0, -radii)
  
  local pts = {}
  
  local d = math.sqrt(dx*dx + dy*dy)
  local linkd = d/links
  local nx, ny = dx, dy
  nx, ny = nx/d, ny/d
  local cx, cy = nx*linkd, ny*linkd
  local first, last
  for i = 0, links do
    local x, y = ulx + nx*linkd*i, uly + ny*linkd*i
    pts[#pts + 1] = { x = x, y = y }
  end
  local r2 = math.pi*radius -- half the circumference
  local links2 = math.floor(r2/linkd)
  
  local xradii = linkd/(2*math.sin(math.pi/(links2*2)))

  for i = links2 - 1, 1, -1 do
    local a2 = math.rad(i/links2*180 - 90) -- + a
    local x2, y2 = math.cos(a2)*xradii, math.sin(a2)*xradii
    local c = math.cos(a)
    local s = math.sin(a)
    local x3, y3 = c*x2 - s*y2, s*x2 + c*y2
    x2, y2 = x3, y3
    pts[#pts + 1] = { x = x2 + ex, y = y2 + ey }
  end
  for i = 0, links do
    local x, y = brx + -nx*linkd*i, bry + -ny*linkd*i
    pts[#pts + 1] = { x = x, y = y }
  end
  for i = links2 - 1, 1, -1 do
    local a2 = math.rad(i/links2*180 + 90) -- + a
    local x2, y2 = math.cos(a2)*xradii, math.sin(a2)*xradii
    local c = math.cos(a)
    local s = math.sin(a)
    local x3, y3 = c*x2 - s*y2, s*x2 + c*y2
    x2, y2 = x3, y3
    pts[#pts + 1] = { x = x2 + sx, y = y2 + sy }
  end
  local last = pts[#pts]
  local bds = {}
  for _, c in ipairs(pts) do
    local a2 = math.atan2(c.y - last.y, c.x - last.x)-- - a
    local b = world:CreateBodyEx("dynamicBody", c.x, c.y, a2)
    b:CreateShapeEx('circle', lradius, 1, 1, 0)
    b:CreateShapeEx('circle', lradius, 1, 1, 0, false, -linkd, 0)
    b:CreateShapeEx('polygon', NewPath(lradius,0, -lradius,0, 0,lradius*1.75), 1, 1, 0, false) 
    table.insert(bodies, b)
    table.insert(bds, b)
    last = c
  end
  local b1 = bds[#bds]
  for _, b2 in ipairs(bds) do
    local x, y = b1:GetPosition()
    world:CreateJointEx('revolute', b1, b2, false, x, y)
    --world:CreateJointEx('rope', b1, b2, false, x, y, x, y)
    b1 = b2
  end
  
  return bodies
end

function Chain:Preview(sx, sy, ex, ey, scale)
  if sx == ex and sy == ey then
    --ex = ex + b2.linearSlop
  end

  local r = self.params.radius
  if r == 0 then
    return
  end

  scene:DrawCircle(sx, sy, r, 1, ORANGE, 1)
  scene:DrawCircle(ex, ey, r, 1, ORANGE, 1)
end

return Chain