require('utils.agen.display')
require('utils.agen.images')
require('utils.agen.draw')
require('utils.table.util')
require('utils.table.field')
require('LuaXml')
require('lfs')
--require('utils.io.xml')
require('utils.io.lfs')
require('utils.io.path')

RED = Color(255, 0, 0)
LIME = Color(0, 255, 0)
ORANGE = Color(0, 128, 128)
GREEN = Color(0, 128, 0)
BLUE = Color(0, 0, 255)
BLACK = Color(0, 0, 0)
WHITE = Color(255, 255, 255)
GRAY = Color(64, 64, 64)
DARKORANGE = Color(255, 140, 0)

conf = {}
conf.curpath = "."
conf.lastfile = "boxy/default.xml"
conf.resx = 1000
conf.resy = 600
conf.resd = 32
conf.areaw = 5
conf.areah = 5

-- Overwrites existing values in the destination table (d) with values from the source table (s)
-- Preserves the original value types in the destination table (boolean, number or string)
-- @param s Source xml table
-- @param d Destination table
xml.overwrite = function(s, d, m)
  -- iterate the source table
  for k, v in pairs(s) do
    local kt = type(k)
    -- substatements and text content is represented as array data having numerical keys
    -- attributes and tags use string keys
    assert(type(k) ~= "table")
    --app.log('error', "key '%s' with type '%s'", k, type(k))
    -- tag
    if type(v) == "table" then
      -- s[k] points to a table, k must be a number, s[k] ia a tag
      local tag = xml.tag(v)
      if d[tag] == nil and m == true then
        d[tag] = {}
      end
      if type(d[tag]) == "table" then
        xml.overwrite(v, d[tag])
      end
    -- attribute
    elseif k ~= xml.TAG then
      -- s[k] points to a string
      -- if k is a string s[k] is an attribute of s
      -- if k is a number s[k] is a substatement or text content
      -- convert source value to match destination type
      local dt = type(d[k])
      if dt == 'number' then
        v = tonumber(v)
      elseif dt == 'boolean' then
        if v == 'true' then
          v = true
        elseif v == 'false' then
          v = false
        else
          v = nil
        end
      end
      -- overwrite
      if v ~= nil then
        d[k] = v
      end
    end
  end
end

local private = system.get_private_dir()
private = string.format("%s/boxy", private)
local confpath = string.format("%s/conf.xml", private)
local e, c = pcall(xml.load, confpath) --xml.safe_load(confpath)
if e == true and c then
  xml.overwrite(c, conf)
end

display:create("Boxy", conf.resx, conf.resy, conf.resd, true)
display.on_destroy = function()
  lfs.mkdir(private)
  xml.save(conf, confpath) --safe_save(confpath, conf)
end

display:set_title(c.lastfile)

display.on_resize = function(d, w, h)
--[[
  local dm = DisplayMode()
  display:get_mode(dm)
  dm.width = w
  dm.height = h
  if display:set_mode(dm) == false then
    return
  end
  ]]
  conf.resx = w
  conf.resy = h
  if screen then
    --screen:reset(w, h)
  end
end

font = Font()
--font:load_system("Tahoma", 8)
font:load_file("boxy/fonts/apple.ttf", 6)


--require('lfs')
require('Box2D')--('2.3.0') -- b2.fill, b2.linearSlop and b2.maxPolygonVertices

require('utils.gui.main')
require('utils.gui.obj.label')
require('utils.gui.obj.image')
require('utils.gui.obj.mcontainer')
require('utils.gui.obj.window')
require('utils.gui.obj.input')
require('utils.gui.obj.checkbox')
require('utils.gui.obj.list')
require('utils.gui.obj.slider')
require('utils.gui.obj.scrollbar')
require('utils.gui.obj.arrow')
require('utils.gui.obj.button')
require('utils.gui.obj.textbox')

require('LuaXML')
require('boxy.b2d.math')
require('boxy.b2d.world')
require('boxy.b2d.body')
require('boxy.b2d.shape')
require('boxy.b2d.joint')
require('boxy.b2d.cast')

require('boxy.states.state')
require('boxy.states.editor')
require('boxy.states.body')
require('boxy.states.joint')
require('boxy.states.vertex')
require('boxy.states.model')

require('boxy.tools.tool')
require('boxy.tools.addvertex')
require('boxy.tools.addjoint')
require('boxy.tools.anchor')
require('boxy.tools.circle')
require('boxy.tools.jselect')
require('boxy.tools.jtranslate')
require('boxy.tools.pivot')
require('boxy.tools.polygon')
require('boxy.tools.rotate')
require('boxy.tools.scale')
require('boxy.tools.select')
require('boxy.tools.translate')
require('boxy.tools.vselect')
require('boxy.tools.vtranslate')
require('boxy.tools.addmodel')
require('boxy.tools.box')

require('boxy.editor.scene')
require('boxy.editor.filedialog')
require('boxy.editor.okcancel')
require('boxy.editor.warning')
require('boxy.editor.screen')
--require('boxy.editor.selection')

models = {}
require('boxy.models.model')

local path = "boxy/models"
for file in lfs.dir(path) do
  if file ~= "." and file ~= ".." then
    local full = string.format("%s/%s", path, file)
    local i = string.sub(file, 1, -5)
    local v = dofile(full)
    models[i] = v
  end
end

gui.create()
gui.set_style('font', font)

scene = Scene:Create()
screen = gui.Screen()
gui.container:add_child(screen)
gui.container:set_focus(screen)

function LoadWorld(p)
  local res, file = pcall(xml.load, p)
  if res == false or file == nil then
    return
  end
  local box = xml.find(file, "World")
  if box == nil then
    return
  end
  local world = World:CreateXML(box)
  if screen.gstate then
    screen.gstate:Deselect()
    screen.gstate:Select()
  end
  scene:SetWorld(world)
  conf.lastfile = io.sanitize(p)
  display:set_title(p)
  xml.save(confpath, conf) --safe_save(confpath, conf)
  return world
end

--local box = xml.safe_load(conf.lastfile)
local world = LoadWorld(conf.lastfile)
if world == nil then
  local d = { gravity = { x = 0, y = 0 } }
  world = World:Create(d)
end

scene:SetWorld(world)

--screen:set_style(gui.styles.font)
screen.node:add_child(scene.overlay)
--screen:set_camera(world.camera)
screen:set_scene(scene)
screen:set_state(BodyState)
screen:set_tool(SelectTool)

screen.gstate:ShowMenu()

--[[
for i = 1, 50 do
  local fn = "FTM/lvl/clymright/" .. i
  display:set_title(fn .. ".xml")
  local x = xml.safe_load(fn .. ".xml")
  assert(x)
  local box = xml.find(x, "World")
  assert(box)
  local w = World:CreateXML(box)
  local gx, gy = w:GetGravity()
  -- m/(s*s)
  -- (fv - iv)/t
  w:SetGravity(b2.Vec2(gx*2, gy*2))
  local b = {}
  for _, v in pairs(w.bodies) do
    for _, s in ipairs(v.shapes) do
      s.def.friction = s.def.friction*2
      --s.def.restitution = s.def.restitution --*2
      s.def.density = s.def.density/2
    end

    v.def.linearDamping = v.def.linearDamping/2
    --v.def.angularDamping = v.def.angularDamping/2
    local lvx, lvy = v:GetLinearVelocity()
    v:SetLinearVelocity(lvx*2, lvy*2)
    --local av = v:GetAngularVelocity()
    --v:SetAngularVelocity(av*2)
    table.insert(b, v)
  end
  screen.gstate:ScaleBodies(b, 0, 0, 2)
  for _, v in pairs(w.joints) do
    local t = v:GetType()
    if t == "prismatic" then
      if v.def.motorSpeed ~= nil then
        v.def.motorSpeed = v.def.motorSpeed*2
      end
    end
  end
  w:SaveXML("FTM/lvl/clysm/" .. i .. ".xml")
end
]]